"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const accounts_module_1 = require("./accounts/accounts.module");
const auth_module_1 = require("./auth/auth.module");
const banks_module_1 = require("./banks/banks.module");
const countries_module_1 = require("./countries/countries.module");
const seeder_service_1 = require("./seeder/seeder.service");
const sucursals_module_1 = require("./sucursals/sucursals.module");
const user_entity_1 = require("./users/entities/user.entity");
const users_module_1 = require("./users/users.module");
const presupuesto_module_1 = require("./presupuesto/presupuesto.module");
const apps_module_1 = require("./apps/apps.module");
const clients_module_1 = require("./clients/clients.module");
const client_instruments_module_1 = require("./client-instruments/client-instruments.module");
const rates_module_1 = require("./rates/rates.module");
const transactions_module_1 = require("./transactions/transactions.module");
const wallet_users_module_1 = require("./wallet-users/wallet-users.module");
const platform_express_1 = require("@nestjs/platform-express");
const multer_1 = require("multer");
const transactions_tracking_module_1 = require("./transactions-tracking/transactions-tracking.module");
const trans_wall_module_1 = require("./trans-wall/trans-wall.module");
const wallets_refills_module_1 = require("./wallets-refills/wallets-refills.module");
const config_1 = require("@nestjs/config");
const withdrawallet_module_1 = require("./withdrawallet/withdrawallet.module");
const documents_module_1 = require("./documents/documents.module");
const documents_expedient_module_1 = require("./documents-expedient/documents-expedient.module");
const expedient_module_1 = require("./expedient/expedient.module");
const queues_module_1 = require("./queues/queues.module");
const transactions_temporary_module_1 = require("./transactions-temporary/transactions-temporary.module");
const withdra_wallet_detail_module_1 = require("./withdra-wallet-detail/withdra-wallet-detail.module");
const bull_1 = require("@nestjs/bull");
const notifications_module_1 = require("./notifications/notifications.module");
const backup_module_1 = require("./backup/backup.module");
const dolar_module_1 = require("./dolar/dolar.module");
const bitacora_module_1 = require("./bitacora/bitacora.module");
const publicidad_module_1 = require("./publicidad/publicidad.module");
const recharge_module_1 = require("./recharge/recharge.module");
let AppModule = class AppModule {
    constructor(seederService) {
        this.seederService = seederService;
    }
    async onApplicationBootstrap() {
        await this.seederService.seed();
    }
};
exports.AppModule = AppModule;
exports.AppModule = AppModule = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot({
                envFilePath: [
                    '.prod.env',
                    '.dev.env',
                    '.env'
                ]
            }),
            platform_express_1.MulterModule.register({
                storage: (0, multer_1.diskStorage)({
                    destination: "./uploads",
                }),
            }),
            bull_1.BullModule.forRoot({
                redis: {
                    host: 'localhost',
                    port: 6379,
                },
            }),
            typeorm_1.TypeOrmModule.forRoot({
                type: "mysql",
                host: process.env.DB_HOST || "desarrollo.paneteirl.store",
                port: parseInt(process.env.DB_PORT, 10) || 3306,
                username: process.env.DB_USERNAME || "acampos",
                password: process.env.DB_PASSWORD || "1256Eli*a1",
                database: process.env.DB_DATABASE || "panet",
                autoLoadEntities: process.env.DB_AUTOLOAD_ENTITIES === "true" || true,
                synchronize: process.env.DB_SYNCHRONIZE === "true" || true,
            }),
            users_module_1.UsersModule,
            sucursals_module_1.SucursalsModule,
            accounts_module_1.AccountsModule,
            countries_module_1.CountriesModule,
            banks_module_1.BanksModule,
            auth_module_1.AuthModule,
            typeorm_1.TypeOrmModule.forFeature([user_entity_1.User]),
            presupuesto_module_1.PresupuestoModule,
            apps_module_1.AppsModule,
            clients_module_1.ClientsModule,
            client_instruments_module_1.ClientInstrumentsModule,
            rates_module_1.RatesModule,
            transactions_module_1.TransactionsModule,
            wallet_users_module_1.WalletUsersModule,
            transactions_tracking_module_1.TransactionsTrackingModule,
            trans_wall_module_1.TransWallModule,
            wallets_refills_module_1.WalletsRefillsModule,
            withdrawallet_module_1.WithdrawalletModule,
            documents_module_1.DocumentsModule,
            documents_expedient_module_1.DocumentsExpedientModule,
            expedient_module_1.ExpedientModule,
            queues_module_1.QueuesModule,
            transactions_temporary_module_1.TransactionsTemporaryModule,
            withdra_wallet_detail_module_1.WithdraWalletDetailModule,
            notifications_module_1.NotificationsModule,
            backup_module_1.BackupModule,
            dolar_module_1.DolarModule,
            bitacora_module_1.BitacoraModule,
            publicidad_module_1.PublicidadModule,
            recharge_module_1.RechargeModule,
        ],
        controllers: [],
        providers: [seeder_service_1.SeederService],
    }),
    __metadata("design:paramtypes", [seeder_service_1.SeederService])
], AppModule);
//# sourceMappingURL=app.module.js.map