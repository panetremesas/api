export declare enum TYPE_ACCOUNT {
    ENVIO = "ENVIO",
    RECEPCION = "RECEPCION"
}
export declare enum TYPE_MONEDA {
    FIAT = "FIAT",
    CRIPTO = "CRIPTO"
}
export declare enum TYPE_PRESUPUESTO {
    INGRESO = "INGRESO",
    EGRESO = "EGRESO",
    INGRESOT = "INGRESO TRANSACCIONES",
    EGRESOT = "EGRESO TRANSACCIONES"
}
export declare enum TYPE_WALLET {
    PROFITS = "PROFITS",
    USER = "USER",
    TRANSACTIONS = "TRANSACTIONS"
}
export declare enum TYPE_ORDER_PAY {
    PAGO = "PAGO",
    TRANSFERENCIA = "TRANSFERENCIA"
}
export declare enum STATUS_ORDER_PAY {
    CREADA = "CREADA",
    EJECUTADA = "EJECUTADA",
    COMPLEATADA = "COMPLEATADA"
}
export declare enum STATUS_REFILL {
    CREADA = "CREADA",
    COMPLEATADA = "COMPLEATADA",
    ANULADA = "ANULADA",
    OBSERVADA = "OBSERVADA"
}
export declare enum TYPE_DOCUMENT {
    IMAGEN = "IMAGEN",
    TEXTO = "TEXTO"
}
export declare enum TYPE_EXPEDIENT {
    RECARGA = "RECARGA",
    TRANSACCION = "TRANSACCION"
}
export declare enum STATUS_RETIRO {
    CREADA = "CREADA",
    ORDENADA = "ORDENADA"
}
export declare enum STATUS_QUEUE {
    CREADA = "CREADA",
    OBSERVADA = "OBSERVADA",
    COMPLEATADA = "COMPLEATADA"
}
export declare enum STATUS_NOTIFICATION {
    PENDING = "PENDING",
    COMPLETE = "COMPLETE",
    QUEUE = "QUEUE"
}
export declare enum STATUS_RECARGA {
    CREADA = "CREADA",
    ANULADA = "ANULADA",
    COMPLETADA = "COMPLETADA"
}
