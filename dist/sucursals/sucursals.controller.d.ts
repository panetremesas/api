import { SucursalsService } from './sucursals.service';
import { CreateSucursalDto } from './dto/create-sucursal.dto';
import { UpdateSucursalDto } from './dto/update-sucursal.dto';
export declare class SucursalsController {
    private readonly sucursalsService;
    constructor(sucursalsService: SucursalsService);
    create(createSucursalDto: CreateSucursalDto): Promise<{
        module: string;
        error: boolean;
        message: string;
        data: CreateSucursalDto & import("./entities/sucursal.entity").Sucursal;
    }>;
    findAll(): Promise<{
        module: string;
        error: boolean;
        message: string;
        data: import("./entities/sucursal.entity").Sucursal[];
    }>;
    findOne(id: any): Promise<{
        module: string;
        error: boolean;
        message: string;
        data: any;
    }>;
    update(id: any, updateSucursalDto: UpdateSucursalDto): Promise<{
        module: string;
        error: boolean;
        message: string;
        data: Promise<import("typeorm").UpdateResult>;
    }>;
}
