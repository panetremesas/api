import { CreateSucursalDto } from './create-sucursal.dto';
declare const UpdateSucursalDto_base: import("@nestjs/mapped-types").MappedType<Partial<CreateSucursalDto>>;
export declare class UpdateSucursalDto extends UpdateSucursalDto_base {
    status: boolean;
}
export {};
