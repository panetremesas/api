export declare class CreateSucursalDto {
    code: string;
    name: string;
    address: string;
    phone: string;
    remittances: boolean;
    shipments: boolean;
    userSurAmerica: string;
    passwordSurAmerica: string;
}
