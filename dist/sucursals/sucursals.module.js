"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SucursalsModule = void 0;
const common_1 = require("@nestjs/common");
const sucursals_service_1 = require("./sucursals.service");
const sucursals_controller_1 = require("./sucursals.controller");
const typeorm_1 = require("@nestjs/typeorm");
const sucursal_entity_1 = require("./entities/sucursal.entity");
let SucursalsModule = class SucursalsModule {
};
exports.SucursalsModule = SucursalsModule;
exports.SucursalsModule = SucursalsModule = __decorate([
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature([sucursal_entity_1.Sucursal])],
        controllers: [sucursals_controller_1.SucursalsController],
        providers: [sucursals_service_1.SucursalsService],
    })
], SucursalsModule);
//# sourceMappingURL=sucursals.module.js.map