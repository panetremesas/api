"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SucursalsService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const sucursal_entity_1 = require("./entities/sucursal.entity");
const typeorm_2 = require("typeorm");
let SucursalsService = class SucursalsService {
    constructor(sucursalRepository) {
        this.sucursalRepository = sucursalRepository;
    }
    async create(createSucursalDto) {
        const sucursal = await this.sucursalRepository.save(createSucursalDto);
        const data = {
            module: 'Sucursales',
            error: false,
            message: 'Sucursal creada con exito',
            data: sucursal
        };
        return data;
    }
    async findAll() {
        const data = {
            module: 'Sucursales',
            error: false,
            message: 'Listado Obtenido con exito',
            data: await this.sucursalRepository.find()
        };
        return data;
    }
    async findOne(id) {
        const sucursal = await this.sucursalRepository.findOne({ where: { id }, relations: ['users'] });
        if (sucursal == null) {
            const data = {
                module: 'Sucursales',
                error: true,
                message: 'Sucursal no existe',
                data: null
            };
            return data;
        }
        const data = {
            module: 'Sucursales',
            error: false,
            message: 'Listado Obtenido con exito',
            data: sucursal
        };
        return data;
    }
    async update(id, updateSucursalDto) {
        const sucursal = this.sucursalRepository.update(id, updateSucursalDto);
        const data = {
            module: 'Sucursales',
            error: false,
            message: 'Sucursal Actualizada con exito',
            data: sucursal
        };
        return data;
    }
};
exports.SucursalsService = SucursalsService;
exports.SucursalsService = SucursalsService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(sucursal_entity_1.Sucursal)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], SucursalsService);
//# sourceMappingURL=sucursals.service.js.map