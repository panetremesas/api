import { User } from "src/users/entities/user.entity";
export declare class Sucursal {
    id: number;
    code: string;
    name: string;
    address: string;
    phone: string;
    remittances: boolean;
    shipments: boolean;
    userSurAmerica: string;
    passwordSurAmerica: string;
    status: boolean;
    users: User[];
    createdAt: Date;
    updatedAt: Date;
}
