import { CreateSucursalDto } from './dto/create-sucursal.dto';
import { UpdateSucursalDto } from './dto/update-sucursal.dto';
import { Sucursal } from './entities/sucursal.entity';
import { Repository } from 'typeorm';
export declare class SucursalsService {
    private readonly sucursalRepository;
    constructor(sucursalRepository: Repository<Sucursal>);
    create(createSucursalDto: CreateSucursalDto): Promise<{
        module: string;
        error: boolean;
        message: string;
        data: CreateSucursalDto & Sucursal;
    }>;
    findAll(): Promise<{
        module: string;
        error: boolean;
        message: string;
        data: Sucursal[];
    }>;
    findOne(id: any): Promise<{
        module: string;
        error: boolean;
        message: string;
        data: any;
    }>;
    update(id: any, updateSucursalDto: UpdateSucursalDto): Promise<{
        module: string;
        error: boolean;
        message: string;
        data: Promise<import("typeorm").UpdateResult>;
    }>;
}
