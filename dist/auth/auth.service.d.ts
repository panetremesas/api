import { LoginAuthDto } from './dto/login-auth.dto';
import { Repository } from 'typeorm';
import { User } from 'src/users/entities/user.entity';
import { JwtService } from '@nestjs/jwt';
import { Bitacora } from 'src/bitacora/entities/bitacora.entity';
export declare class AuthService {
    private readonly userRepository;
    private readonly bitacoraRepository;
    private jwtAuthService;
    constructor(userRepository: Repository<User>, bitacoraRepository: Repository<Bitacora>, jwtAuthService: JwtService);
    login(loginAuthDto: LoginAuthDto): Promise<{
        user: {
            password: any;
            id: number;
            name: string;
            user: string;
            typeUser: string;
            puller: boolean;
            account_owner: boolean;
            dispatcher: boolean;
            remittances: boolean;
            shipments: boolean;
            profit_commission: number;
            sucursal: import("../sucursals/entities/sucursal.entity").Sucursal;
            accounts: import("../accounts/entities/account.entity").Account[];
            clients: import("../clients/entities/client.entity").Client[];
            presupuestos: import("../presupuesto/entities/presupuesto.entity").Presupuesto[];
            presupuestos_wallet: import("../presupuesto/entities/presupuesto.entity").Presupuesto[];
            transactions: import("../transactions/entities/transaction.entity").Transaction[];
            bitacoras: Bitacora[];
            wallets: import("../wallet-users/entities/wallet-user.entity").WalletUser[];
            cola: import("../queues/entities/queue.entity").Queue[];
            online: boolean;
            status: boolean;
            tipoCuentas: boolean;
            token_puler: string;
            token_owner: string;
            token_dispatcher: string;
            referer: string;
            phone: string;
            createdAt: Date;
            updatedAt: Date;
        };
        token: string;
    }>;
}
