"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const user_entity_1 = require("../users/entities/user.entity");
const typeorm_2 = require("@nestjs/typeorm");
const bcrypt_1 = require("bcrypt");
const jwt_1 = require("@nestjs/jwt");
const bitacora_entity_1 = require("../bitacora/entities/bitacora.entity");
let AuthService = class AuthService {
    constructor(userRepository, bitacoraRepository, jwtAuthService) {
        this.userRepository = userRepository;
        this.bitacoraRepository = bitacoraRepository;
        this.jwtAuthService = jwtAuthService;
    }
    async login(loginAuthDto) {
        const { user, password } = loginAuthDto;
        const findUser = await this.userRepository.findOne({ where: { user }, relations: ['sucursal'] });
        if (!findUser)
            throw new common_1.HttpException('USER_NOT_FOUND', 404);
        const checkPassword = await (0, bcrypt_1.compare)(password, findUser.password);
        if (!checkPassword)
            throw new common_1.HttpException('PASSWORD_ERROR', 403);
        const playload = {
            id: findUser.id,
            name: findUser.name,
            typeUser: findUser.typeUser,
            sucursalId: findUser.sucursal,
            puller: findUser.puller,
            account_owner: findUser.account_owner,
            system: 'ApiRemesas'
        };
        const token = this.jwtAuthService.sign(playload);
        const data = {
            user: { ...findUser, password: null },
            token: token
        };
        await this.bitacoraRepository.save({
            user: findUser,
            accion: 'Inicio de sesion'
        });
        return data;
    }
};
exports.AuthService = AuthService;
exports.AuthService = AuthService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_2.InjectRepository)(user_entity_1.User)),
    __param(1, (0, typeorm_2.InjectRepository)(bitacora_entity_1.Bitacora)),
    __metadata("design:paramtypes", [typeorm_1.Repository,
        typeorm_1.Repository,
        jwt_1.JwtService])
], AuthService);
//# sourceMappingURL=auth.service.js.map