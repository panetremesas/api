import { OnApplicationBootstrap } from "@nestjs/common";
import { SeederService } from "./seeder/seeder.service";
export declare class AppModule implements OnApplicationBootstrap {
    private readonly seederService;
    constructor(seederService: SeederService);
    onApplicationBootstrap(): Promise<void>;
}
