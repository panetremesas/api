import { BanksService } from './banks.service';
import { CreateBankDto } from './dto/create-bank.dto';
import { UpdateBankDto } from './dto/update-bank.dto';
export declare class BanksController {
    private readonly banksService;
    constructor(banksService: BanksService);
    create(createBankDto: CreateBankDto): Promise<{
        module: string;
        error: boolean;
        message: string;
        data: CreateBankDto & import("./entities/bank.entity").Bank;
    }>;
    findAll(countrie: any): Promise<import("../utils/response.util").ResponseUtil<import("./entities/bank.entity").Bank[]>>;
    findOne(id: string): string;
    update(id: number, updateBankDto: UpdateBankDto): Promise<{
        module: string;
        error: boolean;
        message: string;
        data: import("typeorm").UpdateResult;
    }>;
    remove(id: string): string;
}
