import { Country } from "src/countries/entities/country.entity";
export declare class CreateBankDto {
    name: string;
    code: string;
    countrie: Country;
}
