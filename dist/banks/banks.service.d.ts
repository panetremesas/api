import { CreateBankDto } from "./dto/create-bank.dto";
import { UpdateBankDto } from "./dto/update-bank.dto";
import { Bank } from "./entities/bank.entity";
import { Repository } from "typeorm";
import { ResponseUtil } from "src/utils/response.util";
export declare class BanksService {
    private readonly bankRepository;
    constructor(bankRepository: Repository<Bank>);
    create(createBankDto: CreateBankDto): Promise<{
        module: string;
        error: boolean;
        message: string;
        data: CreateBankDto & Bank;
    }>;
    findAll(countrie: any): Promise<ResponseUtil<Bank[]>>;
    findOne(id: number): string;
    update(id: number, updateBankDto: UpdateBankDto): Promise<{
        module: string;
        error: boolean;
        message: string;
        data: import("typeorm").UpdateResult;
    }>;
    remove(id: number): string;
}
