"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BanksService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const bank_entity_1 = require("./entities/bank.entity");
const typeorm_2 = require("typeorm");
const response_util_1 = require("../utils/response.util");
let BanksService = class BanksService {
    constructor(bankRepository) {
        this.bankRepository = bankRepository;
    }
    async create(createBankDto) {
        const bank = await this.bankRepository.save(createBankDto);
        const data = {
            module: "Bancos",
            error: false,
            message: "Banco creado con exito",
            data: bank,
        };
        return data;
    }
    async findAll(countrie) {
        let banks = this.bankRepository.createQueryBuilder("bank");
        if (countrie) {
            banks = banks.andWhere("bank.countrie = :countrie", { countrie });
        }
        const result = await banks.getMany();
        return response_util_1.ResponseUtil.success(result, "Bancos obtenidos con exito");
    }
    findOne(id) {
        return `This action returns a #${id} bank`;
    }
    async update(id, updateBankDto) {
        const bank = await this.bankRepository.update(id, updateBankDto);
        const data = {
            module: "Bancos",
            error: false,
            message: "Banco editado con exito",
            data: bank,
        };
        return data;
    }
    remove(id) {
        return `This action removes a #${id} bank`;
    }
};
exports.BanksService = BanksService;
exports.BanksService = BanksService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(bank_entity_1.Bank)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], BanksService);
//# sourceMappingURL=banks.service.js.map