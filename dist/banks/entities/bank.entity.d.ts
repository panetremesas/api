import { Account } from "src/accounts/entities/account.entity";
import { Country } from "src/countries/entities/country.entity";
export declare class Bank {
    id: number;
    name: string;
    code: string;
    status: boolean;
    countrie: Country;
    accounts: Account[];
    createdAt: Date;
    updatedAt: Date;
}
