"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CountriesService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const country_entity_1 = require("./entities/country.entity");
const typeorm_2 = require("typeorm");
const response_util_1 = require("../utils/response.util");
const transactions_temporary_entity_1 = require("../transactions-temporary/entities/transactions-temporary.entity");
const transaction_entity_1 = require("../transactions/entities/transaction.entity");
const rate_entity_1 = require("../rates/entities/rate.entity");
const user_entity_1 = require("../users/entities/user.entity");
const account_entity_1 = require("../accounts/entities/account.entity");
const wallet_user_entity_1 = require("../wallet-users/entities/wallet-user.entity");
const type_account_enum_1 = require("../enums/type_account.enum");
const notification_entity_1 = require("../notifications/entities/notification.entity");
let CountriesService = class CountriesService {
    constructor(countrieRepository, temporalRepository, transactionRepository, rateRepository, userRepository, accountRepository, walletUserRepository, notificationRepository) {
        this.countrieRepository = countrieRepository;
        this.temporalRepository = temporalRepository;
        this.transactionRepository = transactionRepository;
        this.rateRepository = rateRepository;
        this.userRepository = userRepository;
        this.accountRepository = accountRepository;
        this.walletUserRepository = walletUserRepository;
        this.notificationRepository = notificationRepository;
    }
    async create(createCountryDto) {
        const countrie = await this.countrieRepository.save(createCountryDto);
        return response_util_1.ResponseUtil.success(countrie, "Pais Creado con exito");
    }
    async findAll(status, report) {
        const queryBuilder = this.countrieRepository.createQueryBuilder("countrie");
        let result = [];
        if (report) {
            queryBuilder.leftJoinAndSelect("countrie.wallets", "walletUser", "walletUser.type_wallet = :type", { type: "USER" });
            queryBuilder.groupBy("countrie.id");
            queryBuilder.select([
                "countrie.id as countrieId",
                "countrie.name as name",
                "countrie.currency as currency",
                "countrie.abbreviation as flag",
                "SUM(walletUser.available) as totalAmount",
                `SUM(walletUser.available) / countrie.rate_purchase as usdtAmount`,
            ]);
            result = await queryBuilder.getRawMany();
        }
        if (status) {
            queryBuilder.andWhere("countrie.deleteAt IS NULL");
            result = await queryBuilder.getMany();
        }
        return response_util_1.ResponseUtil.success(result, "Paises obtenidos con exito");
    }
    async findOne(id) {
        const countrie = await this.countrieRepository.findOne({
            where: { id },
            relations: ["banks", "accounts"],
        });
        if (countrie == null) {
            return response_util_1.ResponseUtil.error("Pais no existe en la base de datos");
        }
        return response_util_1.ResponseUtil.success(countrie, "Pais Obtenido con exito");
    }
    update(id, updateCountryDto) {
        const countrie = this.countrieRepository.update(id, updateCountryDto);
        return response_util_1.ResponseUtil.success(countrie, "Pais Actualizado con exito");
    }
    async updateStatus(countrieId, status) {
        try {
            const countrie = await this.countrieRepository.findOne({
                where: { id: countrieId },
            });
            if (!countrie) {
                return response_util_1.ResponseUtil.error("País no encontrado");
            }
            let value = false;
            if (status == "true") {
                value = true;
            }
            const updateResult = await this.countrieRepository.update(countrieId, {
                status: value,
            });
            if (updateResult.affected === 0) {
                return response_util_1.ResponseUtil.error("No se pudo actualizar el status del país");
            }
            const updatedCountrie = await this.countrieRepository.findOne({
                where: { id: countrieId },
            });
            if (status) {
                let trans = await this.temporalRepository.createQueryBuilder("tran")
                    .where("tran.destinationId = :countrieId", { countrieId })
                    .leftJoinAndSelect("tran.origin", "origin")
                    .leftJoinAndSelect("tran.destination", "destination")
                    .leftJoinAndSelect("tran.creator", "creator")
                    .leftJoinAndSelect("tran.account", "account")
                    .leftJoinAndSelect("tran.client", "client")
                    .leftJoinAndSelect("account.user", "user")
                    .leftJoinAndSelect("account.instrument", "instrument")
                    .where("tran.status = :status", { status: false })
                    .getMany();
                for (let tran of trans) {
                    let rate = await this.rateRepository.createQueryBuilder("rate")
                        .where("rate.originId = :origin", { origin: tran.origin.id })
                        .andWhere("rate.destinationId = :destination", { destination: tran.destination.id })
                        .leftJoinAndSelect("rate.origin", "origin")
                        .leftJoinAndSelect("rate.destination", "destination")
                        .getOne();
                    const lastTransaction = await this.transactionRepository
                        .createQueryBuilder("transaction")
                        .orderBy("transaction.id", "DESC")
                        .getOne();
                    let nextId = 1;
                    if (lastTransaction) {
                        nextId = lastTransaction.id + 1;
                    }
                    let number = Math.floor(Math.random() * 9000000) + 1000000;
                    const referencia = "PT-" + number + "-" + nextId;
                    const type_profit = rate.type_profit;
                    const jalador = await this.userRepository.findOneBy({
                        id: tran.creator.id,
                    });
                    let profit_puller = 0;
                    if (jalador.profit_commission > 0) {
                        profit_puller = this.calcularValorPorcentaje(tran.amount, jalador.profit_commission);
                    }
                    let account = await this.accountRepository.findOne({
                        where: {
                            id: tran.account.id,
                        },
                        relations: ['user']
                    });
                    let profit_account = 0;
                    if (account.commision === true) {
                        profit_account = this.calcularValorPorcentaje(tran.amount, account.profit_commission);
                    }
                    let comision = +tran.amount * rate.origin[type_profit] -
                        +tran.amount;
                    let comisionHome = +comision - +profit_account - +profit_puller;
                    let vWalletOrigin = null;
                    vWalletOrigin = await this.walletUserRepository
                        .createQueryBuilder("wallet")
                        .where("userId = :user AND countrieId = :countrie AND type_wallet = :type_wallet", {
                        user: account.user.id,
                        countrie: rate.origin.id,
                        type_wallet: "USER",
                    })
                        .getOne();
                    if (vWalletOrigin === null) {
                        vWalletOrigin = await this.walletUserRepository.save({
                            countrie: rate.origin,
                            user: account.user,
                            available: 0,
                            type_wallet: type_account_enum_1.TYPE_WALLET.USER,
                        });
                    }
                    let amount = 0;
                    if (rate.origin.id !== 2 && rate.origin.id !== 1) {
                        amount = tran.amount * rate.rate;
                    }
                    else {
                        amount = tran.amount * rate.rate;
                    }
                    if (rate.origin.id == 1 && rate.destination.id == 2) {
                        amount = tran.amount * rate.rate;
                    }
                    console.log(rate);
                    if (rate.origin.id === 1 && rate.destination.id !== 2) {
                        amount = tran.amount / rate.rate;
                    }
                    if (rate.origin.id === 2 && rate.destination.id === 1) {
                        amount = tran.amount / rate.rate;
                    }
                    const save = await this.transactionRepository.save({
                        amountSend: tran.amount,
                        currencySend: tran.origin.currency,
                        amountReceived: amount,
                        currencyReceived: tran.destination.currency,
                        dateProofOfSend: tran.fechaComprobante,
                        numberProofOfSend: tran.codigo_comprobante,
                        client: tran.client,
                        instrument: tran.instrument,
                        rate: rate,
                        creator: tran.creator,
                        proofOfSend: tran.comprobante,
                        reference: referencia,
                        puller_profit: profit_puller,
                        account_profit: profit_account,
                        home_profit: comisionHome,
                        account: account,
                        owner: tran.account.user,
                        origin: rate.origin,
                        destination: rate.destination,
                        wallet_reception: vWalletOrigin,
                        account_reception: account,
                    });
                    await this.temporalRepository.update(tran.id, { status: true });
                    let vClient = tran.client;
                    let numero = vClient.phone;
                    let encodedMessage = "Hola, estimado " +
                        vClient.full_name +
                        " te notificamos que tu transaccion por el monto de " +
                        save.amountSend +
                        " " +
                        save.currencySend +
                        ", con destino a " +
                        tran.destination.name +
                        " por " +
                        save.amountReceived +
                        " " +
                        save.currencyReceived +
                        " ha sido generada con exito en nuestro sistema, en los proximos minutos te enviaremos el comprobante de procesado. \n Su Numero de Transaccion es: " +
                        save.reference;
                    let payload = {
                        message: encodedMessage,
                        image: null,
                        phone: numero
                    };
                    let encodedMessage2 = "Referencia: " +
                        save.reference +
                        "\n" +
                        "Numero de Comprobante: " +
                        save.numberProofOfSend +
                        "Fecha y Hora: " +
                        save.createdAt;
                    let payload3 = {
                        message: encodedMessage2,
                        image: tran.comprobante,
                        phone: tran.account.user.phone
                    };
                    this.notificationRepository.save(payload);
                    this.notificationRepository.save(payload3);
                }
            }
            return response_util_1.ResponseUtil.success(updatedCountrie, "Status del país actualizado con éxito");
        }
        catch (error) {
            console.error("Error al actualizar el status del país:", error);
            return response_util_1.ResponseUtil.error("Error al actualizar el status del país");
        }
    }
    calcularValorPorcentaje(montoInicial, valorDecimal) {
        const porcentajeReal = valorDecimal - 1;
        const valorPorcentaje = montoInicial * porcentajeReal;
        return valorPorcentaje;
    }
};
exports.CountriesService = CountriesService;
exports.CountriesService = CountriesService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(country_entity_1.Country)),
    __param(1, (0, typeorm_1.InjectRepository)(transactions_temporary_entity_1.TransactionsTemporary)),
    __param(2, (0, typeorm_1.InjectRepository)(transaction_entity_1.Transaction)),
    __param(3, (0, typeorm_1.InjectRepository)(rate_entity_1.Rate)),
    __param(4, (0, typeorm_1.InjectRepository)(user_entity_1.User)),
    __param(5, (0, typeorm_1.InjectRepository)(account_entity_1.Account)),
    __param(6, (0, typeorm_1.InjectRepository)(wallet_user_entity_1.WalletUser)),
    __param(7, (0, typeorm_1.InjectRepository)(notification_entity_1.Notification)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], CountriesService);
//# sourceMappingURL=countries.service.js.map