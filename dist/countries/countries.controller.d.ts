import { CountriesService } from "./countries.service";
import { CreateCountryDto } from "./dto/create-country.dto";
import { UpdateCountryDto } from "./dto/update-country.dto";
export declare class CountriesController {
    private readonly countriesService;
    constructor(countriesService: CountriesService);
    create(createCountryDto: CreateCountryDto): Promise<import("../utils/response.util").ResponseUtil<CreateCountryDto & import("./entities/country.entity").Country>>;
    findAll(status: any, report: any): Promise<import("../utils/response.util").ResponseUtil<any[]>>;
    findOne(id: string): Promise<import("../utils/response.util").ResponseUtil<unknown>>;
    update(id: number, updateCountryDto: UpdateCountryDto): import("../utils/response.util").ResponseUtil<Promise<import("typeorm").UpdateResult>>;
    updateStatus(countrieId: any, status: any): Promise<import("../utils/response.util").ResponseUtil<unknown>>;
}
