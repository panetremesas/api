import { Account } from "src/accounts/entities/account.entity";
import { Bank } from "src/banks/entities/bank.entity";
import { Presupuesto } from "src/presupuesto/entities/presupuesto.entity";
import { WalletUser } from "src/wallet-users/entities/wallet-user.entity";
export declare class Country {
    id: number;
    name: string;
    abbreviation: string;
    code: number;
    amount: number;
    currency: string;
    profit: number;
    ven_profit: number;
    especial_profit: number;
    rate_purchase: number;
    rate_sales: number;
    rate_wholesale: number;
    status: boolean;
    accounts: Account[];
    banks: Bank[];
    presupuestos: Presupuesto[];
    wallets: WalletUser[];
    createdAt: Date;
    updatedAt: Date;
    deleteAt: Date;
}
