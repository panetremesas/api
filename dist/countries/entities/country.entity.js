"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Country = void 0;
const account_entity_1 = require("../../accounts/entities/account.entity");
const bank_entity_1 = require("../../banks/entities/bank.entity");
const presupuesto_entity_1 = require("../../presupuesto/entities/presupuesto.entity");
const wallet_user_entity_1 = require("../../wallet-users/entities/wallet-user.entity");
const typeorm_1 = require("typeorm");
let Country = class Country {
};
exports.Country = Country;
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], Country.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], Country.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], Country.prototype, "abbreviation", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], Country.prototype, "code", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: 0 }),
    __metadata("design:type", Number)
], Country.prototype, "amount", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], Country.prototype, "currency", void 0);
__decorate([
    (0, typeorm_1.Column)("decimal", { precision: 15, scale: 2, default: 0.0, nullable: true }),
    __metadata("design:type", Number)
], Country.prototype, "profit", void 0);
__decorate([
    (0, typeorm_1.Column)("decimal", { precision: 15, scale: 2, default: 0.0, nullable: true }),
    __metadata("design:type", Number)
], Country.prototype, "ven_profit", void 0);
__decorate([
    (0, typeorm_1.Column)("decimal", { precision: 15, scale: 2, default: 0.0, nullable: true }),
    __metadata("design:type", Number)
], Country.prototype, "especial_profit", void 0);
__decorate([
    (0, typeorm_1.Column)("decimal", { precision: 15, scale: 3, default: 0.0, nullable: true }),
    __metadata("design:type", Number)
], Country.prototype, "rate_purchase", void 0);
__decorate([
    (0, typeorm_1.Column)("decimal", { precision: 15, scale: 3, default: 0.0, nullable: true }),
    __metadata("design:type", Number)
], Country.prototype, "rate_sales", void 0);
__decorate([
    (0, typeorm_1.Column)("decimal", { precision: 15, scale: 3, default: 0.0, nullable: true }),
    __metadata("design:type", Number)
], Country.prototype, "rate_wholesale", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'boolean', default: true }),
    __metadata("design:type", Boolean)
], Country.prototype, "status", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => account_entity_1.Account, (account) => account.countrie),
    __metadata("design:type", Array)
], Country.prototype, "accounts", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => bank_entity_1.Bank, (bank) => bank.countrie),
    __metadata("design:type", Array)
], Country.prototype, "banks", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => presupuesto_entity_1.Presupuesto, (bank) => bank.countrie),
    __metadata("design:type", Array)
], Country.prototype, "presupuestos", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => wallet_user_entity_1.WalletUser, (wallet) => wallet.countrie),
    __metadata("design:type", Array)
], Country.prototype, "wallets", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: "timestamp" }),
    __metadata("design:type", Date)
], Country.prototype, "createdAt", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: "timestamp" }),
    __metadata("design:type", Date)
], Country.prototype, "updatedAt", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ type: "timestamp", nullable: true }),
    __metadata("design:type", Date)
], Country.prototype, "deleteAt", void 0);
exports.Country = Country = __decorate([
    (0, typeorm_1.Entity)()
], Country);
//# sourceMappingURL=country.entity.js.map