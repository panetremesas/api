import { CreateCountryDto } from "./dto/create-country.dto";
import { UpdateCountryDto } from "./dto/update-country.dto";
import { Country } from "./entities/country.entity";
import { Repository } from "typeorm";
import { ResponseUtil } from "src/utils/response.util";
import { TransactionsTemporary } from "src/transactions-temporary/entities/transactions-temporary.entity";
import { Transaction } from "src/transactions/entities/transaction.entity";
import { Rate } from "src/rates/entities/rate.entity";
import { User } from "src/users/entities/user.entity";
import { Account } from "src/accounts/entities/account.entity";
import { WalletUser } from "src/wallet-users/entities/wallet-user.entity";
import { Notification } from "src/notifications/entities/notification.entity";
export declare class CountriesService {
    private readonly countrieRepository;
    private readonly temporalRepository;
    private readonly transactionRepository;
    private readonly rateRepository;
    private readonly userRepository;
    private readonly accountRepository;
    private readonly walletUserRepository;
    private readonly notificationRepository;
    constructor(countrieRepository: Repository<Country>, temporalRepository: Repository<TransactionsTemporary>, transactionRepository: Repository<Transaction>, rateRepository: Repository<Rate>, userRepository: Repository<User>, accountRepository: Repository<Account>, walletUserRepository: Repository<WalletUser>, notificationRepository: Repository<Notification>);
    create(createCountryDto: CreateCountryDto): Promise<ResponseUtil<CreateCountryDto & Country>>;
    findAll(status: any, report: any): Promise<ResponseUtil<any[]>>;
    findOne(id: any): Promise<ResponseUtil<unknown>>;
    update(id: any, updateCountryDto: UpdateCountryDto): ResponseUtil<Promise<import("typeorm").UpdateResult>>;
    updateStatus(countrieId: any, status: any): Promise<ResponseUtil<unknown>>;
    calcularValorPorcentaje(montoInicial: any, valorDecimal: any): number;
}
