"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CountriesModule = void 0;
const common_1 = require("@nestjs/common");
const countries_service_1 = require("./countries.service");
const countries_controller_1 = require("./countries.controller");
const typeorm_1 = require("@nestjs/typeorm");
const country_entity_1 = require("./entities/country.entity");
const transactions_temporary_entity_1 = require("../transactions-temporary/entities/transactions-temporary.entity");
const transaction_entity_1 = require("../transactions/entities/transaction.entity");
const account_entity_1 = require("../accounts/entities/account.entity");
const rate_entity_1 = require("../rates/entities/rate.entity");
const user_entity_1 = require("../users/entities/user.entity");
const wallet_user_entity_1 = require("../wallet-users/entities/wallet-user.entity");
const notification_entity_1 = require("../notifications/entities/notification.entity");
let CountriesModule = class CountriesModule {
};
exports.CountriesModule = CountriesModule;
exports.CountriesModule = CountriesModule = __decorate([
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature([country_entity_1.Country, transactions_temporary_entity_1.TransactionsTemporary, transaction_entity_1.Transaction, account_entity_1.Account, rate_entity_1.Rate, user_entity_1.User, wallet_user_entity_1.WalletUser, notification_entity_1.Notification])],
        controllers: [countries_controller_1.CountriesController],
        providers: [countries_service_1.CountriesService],
    })
], CountriesModule);
//# sourceMappingURL=countries.module.js.map