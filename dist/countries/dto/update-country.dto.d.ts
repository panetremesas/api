import { CreateCountryDto } from './create-country.dto';
declare const UpdateCountryDto_base: import("@nestjs/mapped-types").MappedType<Partial<CreateCountryDto>>;
export declare class UpdateCountryDto extends UpdateCountryDto_base {
    rate_purchase: number;
    rate_sales: number;
    rate_wholesale: number;
    especial_profit: any;
    ven_profit: number;
    profit: number;
    deleteAt: Date;
    status: boolean;
}
export {};
