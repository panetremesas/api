export declare class CreateCountryDto {
    name: string;
    abbreviation: string;
    currency: string;
    profit: number;
    ven_profit: number;
    especial_profit: number;
    code: number;
    amount: number;
}
