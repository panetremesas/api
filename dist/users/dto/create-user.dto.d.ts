import { Sucursal } from "src/sucursals/entities/sucursal.entity";
export declare class CreateUserDto {
    name: string;
    user: string;
    password: string;
    typeUser: string;
    puller: boolean;
    account_owner: boolean;
    dispatcher: boolean;
    remittances: boolean;
    shipments: boolean;
    profit_commission: number;
    sucursal: Sucursal;
    online: boolean;
    token_puler: string;
    token_owner: string;
    token_dispatcher: string;
    phone: string;
}
