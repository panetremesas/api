"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const user_entity_1 = require("./entities/user.entity");
const typeorm_2 = require("typeorm");
const bcrypt_1 = require("bcrypt");
const response_util_1 = require("../utils/response.util");
let UsersService = class UsersService {
    constructor(userRepository) {
        this.userRepository = userRepository;
    }
    async create(createUserDto) {
        const hashed = await (0, bcrypt_1.hash)(createUserDto.password, 10);
        const user = { ...createUserDto, password: hashed };
        try {
            const usuario = await this.userRepository.save(user);
            return response_util_1.ResponseUtil.success(usuario, "Usuario creado con exito!");
        }
        catch (error) {
            if (error.code == 'ER_DUP_ENTRY') {
                return response_util_1.ResponseUtil.error("Usuario duplicado, intente con otro nombre de usuario.");
            }
            else {
                return response_util_1.ResponseUtil.error("Error undefinied: " + error);
            }
        }
    }
    async findAll(typeUser, sucursal, type_user) {
        const queryBuilder = this.userRepository.createQueryBuilder("user");
        if (typeUser == "POPERARIO" || typeUser == "PREVISION") {
            return response_util_1.ResponseUtil.error("No Autorizado para obtener lista de usuarios");
        }
        if (typeUser == "PADMIN") {
            queryBuilder.andWhere("user.sucursalId = :sucursal", {
                sucursal: sucursal,
            });
        }
        if (type_user && type_user == "account_owner") {
            queryBuilder.where("user.account_owner = :account_owner", {
                account_owner: true,
            });
        }
        if (type_user && type_user == "puller") {
            queryBuilder.where("user.puller = :puller", {
                puller: true,
            });
        }
        if (type_user && type_user == "dispatcher") {
            queryBuilder.where("user.dispatcher = :dispatcher", {
                dispatcher: true,
            });
        }
        const result = await queryBuilder.getMany();
        return response_util_1.ResponseUtil.success(result, "Listado de Usuarios Obtenidos con exito");
    }
    async update(id, updateUserDto) {
        const update = this.userRepository.update(id, updateUserDto);
        return response_util_1.ResponseUtil.success(update, "Usuario actualizado con exito");
    }
    async getDistpatcherWiethWallets() {
        const dispatchers = await this.userRepository.createQueryBuilder("user")
            .where("user.dispatcher = :dispatcher", { dispatcher: true })
            .leftJoinAndSelect("user.wallets", "wallets")
            .leftJoinAndSelect("wallets.countrie", "countrie")
            .andWhere("wallets.type_wallet = :typeWallet", { typeWallet: 'USER' })
            .getMany();
        return response_util_1.ResponseUtil.success(dispatchers, "Listado de Despachadores obtenidos con exito");
    }
    async updatePassword(data) {
        let passwordHashed = await (0, bcrypt_1.hash)(data.newPassword, 10);
        let result = await this.userRepository.update(data.userId, { password: passwordHashed });
        return response_util_1.ResponseUtil.success(result, "Clave actualizada con exito");
    }
    async updateAccounts(user, status) {
        let value = false;
        if (status == "true") {
            value = true;
        }
        console.log(user);
        console.log(value);
        let data = await this.userRepository.update(user, { tipoCuentas: value });
        return { data, message: 'Actualizacion correcta' };
    }
};
exports.UsersService = UsersService;
exports.UsersService = UsersService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(user_entity_1.User)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], UsersService);
//# sourceMappingURL=users.service.js.map