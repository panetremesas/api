import { UsersService } from "./users.service";
import { CreateUserDto } from "./dto/create-user.dto";
import { UpdateUserDto } from "./dto/update-user.dto";
export declare class UsersController {
    private readonly usersService;
    constructor(usersService: UsersService);
    create(createUserDto: CreateUserDto): Promise<import("../utils/response.util").ResponseUtil<unknown>>;
    findAll(request: Request, type_user: any, owners: any): Promise<import("../utils/response.util").ResponseUtil<unknown>>;
    update(id: number, updateUserDto: UpdateUserDto): Promise<import("../utils/response.util").ResponseUtil<Promise<import("typeorm").UpdateResult>>>;
    getDistpatcherWiethWallets(): Promise<import("../utils/response.util").ResponseUtil<import("./entities/user.entity").User[]>>;
    updatePassword(data: any): Promise<import("../utils/response.util").ResponseUtil<import("typeorm").UpdateResult>>;
    updateAccounts(user: any, status: any): Promise<{
        data: import("typeorm").UpdateResult;
        message: string;
    }>;
}
