import { CreateUserDto } from "./dto/create-user.dto";
import { UpdateUserDto } from "./dto/update-user.dto";
import { User } from "./entities/user.entity";
import { Repository } from "typeorm";
import { ResponseUtil } from "src/utils/response.util";
export declare class UsersService {
    private readonly userRepository;
    constructor(userRepository: Repository<User>);
    create(createUserDto: CreateUserDto): Promise<ResponseUtil<unknown>>;
    findAll(typeUser: any, sucursal: any, type_user: any): Promise<ResponseUtil<unknown>>;
    update(id: number, updateUserDto: UpdateUserDto): Promise<ResponseUtil<Promise<import("typeorm").UpdateResult>>>;
    getDistpatcherWiethWallets(): Promise<ResponseUtil<User[]>>;
    updatePassword(data: any): Promise<ResponseUtil<import("typeorm").UpdateResult>>;
    updateAccounts(user: any, status: any): Promise<{
        data: import("typeorm").UpdateResult;
        message: string;
    }>;
}
