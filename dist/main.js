"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const common_1 = require("@nestjs/common");
const times_zones_1 = require("./middlewares/times-zones");
const path_1 = require("path");
const fs_1 = require("fs");
const express = require("express");
const js_1 = require("@bugsnag/js");
const plugin_express_1 = require("@bugsnag/plugin-express");
js_1.default.start({
    apiKey: process.env.BUGSNAG_API_KEY,
    plugins: [plugin_express_1.default],
    appVersion: process.env.APP_VERSION,
});
async function bootstrap() {
    const logger = new common_1.Logger("MainProvider");
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    const bugsnagMiddleware = js_1.default.getPlugin('express');
    app.use(bugsnagMiddleware.requestHandler);
    app.use(bugsnagMiddleware.errorHandler);
    const uploadDir = (0, path_1.join)(process.cwd(), 'uploads');
    if (!(0, fs_1.existsSync)(uploadDir)) {
        (0, fs_1.mkdirSync)(uploadDir);
    }
    app.enableCors();
    app.setGlobalPrefix("api/v1");
    app.use('/uploads', express.static((0, path_1.join)(process.cwd(), 'uploads')));
    app.use(new times_zones_1.TimezoneMiddleware().use);
    app.useGlobalPipes(new common_1.ValidationPipe({
        whitelist: true,
        forbidNonWhitelisted: true,
        transform: true,
    }));
    await app.listen(process.env.PORT_SERVER);
    logger.log("Api corriendo en el puerto: ", process.env.PORT_SERVER);
}
bootstrap();
//# sourceMappingURL=main.js.map