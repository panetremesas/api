import { AccountsService } from "./accounts.service";
import { CreateAccountDto } from "./dto/create-account.dto";
import { UpdateAccountDto } from "./dto/update-account.dto";
export declare class AccountsController {
    private readonly accountsService;
    constructor(accountsService: AccountsService);
    create(createAccountDto: CreateAccountDto): import("../utils/response.util").ResponseUtil<Promise<CreateAccountDto & import("./entities/account.entity").Account>>;
    findAll(countrie: any, bank: any, userId: any, status: any, req: any, tipo: any): Promise<import("../utils/response.util").ResponseUtil<import("./entities/account.entity").Account[]>>;
    findByUser(id: number): Promise<{
        module: string;
        error: boolean;
        message: string;
        data: import("../users/entities/user.entity").User;
    }>;
    findByBank(id: number): Promise<import("../utils/response.util").ResponseUtil<unknown>>;
    findByCountrie(id: number): Promise<import("../utils/response.util").ResponseUtil<import("../countries/entities/country.entity").Country>>;
    findOne(id: string): string;
    update(id: string, updateAccountDto: UpdateAccountDto): import("../utils/response.util").ResponseUtil<Promise<import("typeorm").UpdateResult>>;
    updateStatus(id: number, status: boolean): import("../utils/response.util").ResponseUtil<Promise<import("typeorm").UpdateResult>>;
    updateTipo(id: number, status: boolean): import("../utils/response.util").ResponseUtil<Promise<import("typeorm").UpdateResult>>;
    remove(id: string): string;
}
