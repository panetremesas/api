import { CreateAccountDto } from "./dto/create-account.dto";
import { UpdateAccountDto } from "./dto/update-account.dto";
import { Account } from "./entities/account.entity";
import { Repository } from "typeorm";
import { User } from "src/users/entities/user.entity";
import { Bank } from "src/banks/entities/bank.entity";
import { Country } from "src/countries/entities/country.entity";
import { ResponseUtil } from "src/utils/response.util";
export declare class AccountsService {
    private readonly accountRepository;
    private readonly userRepository;
    private readonly bankRepository;
    private readonly countrieRepository;
    constructor(accountRepository: Repository<Account>, userRepository: Repository<User>, bankRepository: Repository<Bank>, countrieRepository: Repository<Country>);
    create(createAccountDto: CreateAccountDto): ResponseUtil<Promise<CreateAccountDto & Account>>;
    findByUser(id: number): Promise<{
        module: string;
        error: boolean;
        message: string;
        data: User;
    }>;
    findByBank(id: number): Promise<ResponseUtil<unknown>>;
    findByCountrie(id: number): Promise<ResponseUtil<Country>>;
    findByCountrieReception(id: number): Promise<ResponseUtil<unknown>>;
    findAll(countrie: any, bank: any, userId: any, status: any, user: any, tipo: any, origen: any): Promise<ResponseUtil<Account[]>>;
    findOne(id: number): string;
    update(id: number, updateAccountDto: UpdateAccountDto): ResponseUtil<Promise<import("typeorm").UpdateResult>>;
    updateStatus(id: number, status: boolean): ResponseUtil<Promise<import("typeorm").UpdateResult>>;
    updateTipo(id: number, status: boolean): ResponseUtil<Promise<import("typeorm").UpdateResult>>;
    remove(id: number): string;
}
