import { Bank } from "src/banks/entities/bank.entity";
import { Country } from "src/countries/entities/country.entity";
import { TYPE_MONEDA } from "src/enums/type_account.enum";
import { User } from "src/users/entities/user.entity";
export declare class CreateAccountDto {
    titular: string;
    number: string;
    type_account: string;
    document: string;
    email: string;
    type_currency: TYPE_MONEDA;
    commision: boolean;
    profit_commission: number;
    user: User;
    countrie: Country;
    bank: Bank;
    message: string;
}
