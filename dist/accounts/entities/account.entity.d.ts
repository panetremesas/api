import { Bank } from "src/banks/entities/bank.entity";
import { Country } from "src/countries/entities/country.entity";
import { TYPE_MONEDA } from "src/enums/type_account.enum";
import { TransactionsTemporary } from "src/transactions-temporary/entities/transactions-temporary.entity";
import { Transaction } from "src/transactions/entities/transaction.entity";
import { User } from "src/users/entities/user.entity";
export declare class Account {
    id: number;
    titular: string;
    type_account: string;
    number: string;
    document: string;
    email: string;
    type_currency: TYPE_MONEDA;
    commision: boolean;
    profit_commission: number;
    message: string;
    status: boolean;
    user: User;
    countrie: Country;
    bank: Bank;
    transactions: Transaction[];
    transactions_temporales: TransactionsTemporary[];
    tipo: boolean;
    createdAt: Date;
    updatedAt: Date;
}
