"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountsService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const account_entity_1 = require("./entities/account.entity");
const typeorm_2 = require("typeorm");
const user_entity_1 = require("../users/entities/user.entity");
const bank_entity_1 = require("../banks/entities/bank.entity");
const country_entity_1 = require("../countries/entities/country.entity");
const response_util_1 = require("../utils/response.util");
let AccountsService = class AccountsService {
    constructor(accountRepository, userRepository, bankRepository, countrieRepository) {
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
        this.bankRepository = bankRepository;
        this.countrieRepository = countrieRepository;
    }
    create(createAccountDto) {
        const account = this.accountRepository.save(createAccountDto);
        return response_util_1.ResponseUtil.success(account, "Cuenta creada con exito");
    }
    async findByUser(id) {
        const accounts = await this.userRepository.findOne({
            where: { id },
            relations: ["accounts", "accounts.bank", "accounts.countrie"],
        });
        const data = {
            module: "Cuentas",
            error: false,
            message: "Cuentas obtenidas con exito",
            data: accounts,
        };
        return data;
    }
    async findByBank(id) {
        const accounts = await this.bankRepository.findOne({
            where: { id: id },
            relations: [
                "accounts",
                "accounts.bank",
                "accounts.countrie",
                "accounts.user",
            ],
        });
        if (accounts == null) {
            return response_util_1.ResponseUtil.error("Este Banco no existe");
        }
        return response_util_1.ResponseUtil.success(accounts, "Cuentas obtenidas con exito");
    }
    async findByCountrie(id) {
        const accounts = await this.countrieRepository.findOne({
            where: { id: id },
            relations: ["accounts", "accounts.bank", "accounts.countrie"],
        });
        return response_util_1.ResponseUtil.success(accounts, "Cuentas obtenidas con exito");
    }
    async findByCountrieReception(id) {
        const accounts = await this.countrieRepository.findOne({
            where: { id: id },
            relations: ["accounts", "accounts.bank", "accounts.user"],
        });
        if (accounts) {
            const result = accounts.accounts;
            return response_util_1.ResponseUtil.success(accounts, "Cuentas de Recepcion obtenidas con exito");
        }
        else {
            return response_util_1.ResponseUtil.error("Pais o cuentas no existen");
        }
    }
    async findAll(countrie, bank, userId, status, user, tipo, origen) {
        let queryBuilder = await this.accountRepository
            .createQueryBuilder("account")
            .leftJoinAndSelect("account.countrie", "countrie")
            .leftJoinAndSelect("account.bank", "bank");
        if (!status) {
            queryBuilder = queryBuilder.andWhere("account.status = 1");
        }
        if (countrie) {
            queryBuilder = queryBuilder.andWhere("account.countrieId = :countrie", {
                countrie,
            });
        }
        if (bank) {
            queryBuilder = queryBuilder.andWhere("account.bankid = :bank", { bank });
        }
        if (userId) {
            queryBuilder = queryBuilder.andWhere("account.userId = :userId", {
                userId,
            });
        }
        if (origen == 'APP') {
            console.log("Peticion desde la app");
            queryBuilder = queryBuilder.andWhere("account.status = 1");
        }
        const result = await queryBuilder.getMany();
        return response_util_1.ResponseUtil.success(result, "Cuentas obtenidas con exito");
    }
    findOne(id) {
        return `This action returns a #${id} account`;
    }
    update(id, updateAccountDto) {
        const account = this.accountRepository.update(id, updateAccountDto);
        return response_util_1.ResponseUtil.success(account, "Cuenta actualizada");
    }
    updateStatus(id, status) {
        const account = this.accountRepository.update(id, { status });
        return response_util_1.ResponseUtil.success(account, "Estado de Cuenta Actualizado");
    }
    updateTipo(id, status) {
        const account = this.accountRepository.update(id, { tipo: status });
        return response_util_1.ResponseUtil.success(account, "Tipo de cuenta Actualizado");
    }
    remove(id) {
        return `This action removes a #${id} account`;
    }
};
exports.AccountsService = AccountsService;
exports.AccountsService = AccountsService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(account_entity_1.Account)),
    __param(1, (0, typeorm_1.InjectRepository)(user_entity_1.User)),
    __param(2, (0, typeorm_1.InjectRepository)(bank_entity_1.Bank)),
    __param(3, (0, typeorm_1.InjectRepository)(country_entity_1.Country)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], AccountsService);
//# sourceMappingURL=accounts.service.js.map