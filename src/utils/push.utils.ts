import { Expo } from "expo-server-sdk";

export class PushNotificationUtil {
  static async sendPush(token, message) {
    const expo = new Expo();

    if (!Expo.isExpoPushToken(token)) {
      console.error(`El token ${token} no es válido para notificaciones de Expo`);
      return;
    }

    const notification = {
      to: token,
      body: message,
      data: {data: null}
    };

    try {
      const receipts = await expo.sendPushNotificationsAsync([notification]);
      return true;
    } catch (error) {
      console.error('Error al enviar la notificación:', error);
      return false;
    }
  }
}