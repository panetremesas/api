import { STATUS_RECARGA } from "src/enums/type_account.enum";
import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Recharge {
    @PrimaryGeneratedColumn('uuid')
    id: number;

    @Column()
    idFloid: string

    @Column()
    observacion: string

    @Column({ type: 'enum', enum: STATUS_RECARGA, default: STATUS_RECARGA.CREADA })
    status: STATUS_RECARGA

    @Column({ type: 'decimal', precision: 10, scale: 5 })
    monto: number;

    @Column()
    pais: string

    @Column({ type: 'json', nullable: true }) 
    pasarelaResponse: any; 

    @CreateDateColumn({ type: 'timestamp' })
    createdAt: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt: Date;
}