import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { RechargeService } from './recharge.service';
import { CreateRechargeDto } from './dto/create-recharge.dto';
import { UpdateRechargeDto } from './dto/update-recharge.dto';
import { AuthGuard } from 'src/auth/auth.guard';

@Controller('recharge')
export class RechargeController {
  constructor(private readonly rechargeService: RechargeService) {}

  @Post()
  @UseGuards(AuthGuard)
  create(@Body() createRechargeDto) {
    return this.rechargeService.create(createRechargeDto);
  }

  @Post('floidUpdate/:id')
  updatePasarela(@Param('id') id: string) {
    return this.rechargeService.updatePasarela(id);
  }

  @Get()
  @UseGuards(AuthGuard)
  findAll() {
    return this.rechargeService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.rechargeService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateRechargeDto: UpdateRechargeDto) {
    return this.rechargeService.update(+id, updateRechargeDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.rechargeService.remove(+id);
  }
}
