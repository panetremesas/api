import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateRechargeDto } from './dto/create-recharge.dto';
import { UpdateRechargeDto } from './dto/update-recharge.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Recharge } from './entities/recharge.entity';
import { Repository } from 'typeorm';
import axios from 'axios';
import { STATUS_RECARGA } from 'src/enums/type_account.enum';
import { Notification } from 'src/notifications/entities/notification.entity';

@Injectable()
export class RechargeService {

  constructor(
    @InjectRepository(Recharge)
    private readonly rechargeRepository: Repository<Recharge>,
    @InjectRepository(Notification)
    private readonly notificationRepository: Repository<Notification>,
  ) { }

  async create(createRechargeDto) {
    console.log(createRechargeDto)
    const countryLowercase = createRechargeDto.pais.toLowerCase();

    const recarga = await this.rechargeRepository.save({
      monto: createRechargeDto.monto,
      observacion: createRechargeDto.observacion,
      idFloid: 'sin crear',
      pais: createRechargeDto.pais
    })

    const payload: { amount: string; currency?: string; sandbox?: boolean, webhook_url?: string } = {
      amount: createRechargeDto.monto.toString(),
      webhook_url: 'https://desarrollo.paneteirl.store/api/v1/recharge/floidUpdate/' + recarga.id
    };

    if (createRechargeDto.pais === "PE") {
      payload.currency = "PEN";
    }

    try {
      console.log(payload)
      const response = await axios.post(
        `https://api.floid.app/${countryLowercase}/payments/create`,
        payload,
        {
          headers: {
            Authorization: `Bearer 786b64a673122aa03a5fa3909c6d100adad544fa3be9be01cfbb129cb11488d566a733bd98ca7204118baaaf3e086cd17b15ab969eb7b149084f10a898e9c2da`,
            "Content-Type": "application/json",
            Cookie: "PHPSESSID=rjku07cupvna4bjuf5bigs4ntk"
          },
        }
      );

      await this.rechargeRepository.update(recarga.id, {
        idFloid: response.data.payment_token,
      })

      return { data: response.data, message: 'Recarga Creada con exito' };
    } catch (error) {
      if (error.response && error.response.status === 400) {
        const errorMessage = error.response.data?.message || "Error desconocido";
        console.error("Error 400:", errorMessage);
        return { error: true, message: errorMessage };
      }

      console.error("Error inesperado:", error.message);
      throw error; // Lanza el error si no es un 400
    }
  }

  async updatePasarela(id) {
    const data = await this.rechargeRepository.findOne({ where: { id } })

    const payload = {
      payment_token: data.idFloid,
    };

    const response = await axios.post(
      `https://api.floid.app/${data.pais}/payments/check`,
      payload,
      {
        headers: {
          Authorization: `Bearer cc051fc11360e1e313ee4c99579648972ab3ca514b73d947cb13283caf81861d9c8eb1691e33e3c39ab881c180cef59391285ea5fcc4c77431b29d1d4f0b0aa1`,
          "Content-Type": "application/json",
        },
      }
    );

    const transactionStatus = response.data?.status; // Ajustar según la estructura de la respuesta de la API

    if (!transactionStatus) {
      throw new BadRequestException(
        "Estado de la transacción no disponible en la respuesta."
      );
    }

    if (response.data.status === "SUCCESS") {
      await this.rechargeRepository.update(id, {
        status: STATUS_RECARGA.COMPLETADA,
        pasarelaResponse: response.data
      })
    }
    else {
      await this.rechargeRepository.update(id, {
        status: STATUS_RECARGA.ANULADA,
        pasarelaResponse: response.data
      })
    }


    let encodedMessage = `La recarga correspondiente a: ${data.observacion}, ha cambiado de estado, entra en el panel y validala.`

    let payload2 = {
      message: encodedMessage,
      image: null,
      phone: '51916902417'
    }

    let notificacion = this.notificationRepository.save(payload2)
    await axios.get(process.env.URL_QUEUE_NOTIFICATIONS)

    return { data, message: 'Recarga Actualizada con exito' }
  }


  async findAll() {
    const data = await this.rechargeRepository.find()

    return { data, message: 'Recargas obtenidas con exito' }
  }

  findOne(id: number) {
    return `This action returns a #${id} recharge`;
  }

  update(id: number, updateRechargeDto: UpdateRechargeDto) {
    return `This action updates a #${id} recharge`;
  }

  remove(id: number) {
    return `This action removes a #${id} recharge`;
  }
}
