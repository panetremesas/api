import { Module } from '@nestjs/common';
import { RechargeService } from './recharge.service';
import { RechargeController } from './recharge.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Recharge } from './entities/recharge.entity';
import { Notification } from 'src/notifications/entities/notification.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Recharge, Notification])],
  controllers: [RechargeController],
  providers: [RechargeService],
})
export class RechargeModule {}
