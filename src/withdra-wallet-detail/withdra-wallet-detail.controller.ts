import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { WithdraWalletDetailService } from './withdra-wallet-detail.service';
import { CreateWithdraWalletDetailDto } from './dto/create-withdra-wallet-detail.dto';
import { UpdateWithdraWalletDetailDto } from './dto/update-withdra-wallet-detail.dto';

@Controller('withdra-wallet-detail')
export class WithdraWalletDetailController {
  constructor(private readonly withdraWalletDetailService: WithdraWalletDetailService) {}

  @Post()
  create(@Body() createWithdraWalletDetailDto: CreateWithdraWalletDetailDto) {
    return this.withdraWalletDetailService.create(createWithdraWalletDetailDto);
  }

  @Get()
  findAll() {
    return this.withdraWalletDetailService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.withdraWalletDetailService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateWithdraWalletDetailDto: UpdateWithdraWalletDetailDto) {
    return this.withdraWalletDetailService.update(+id, updateWithdraWalletDetailDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.withdraWalletDetailService.remove(+id);
  }
}
