import { WalletUser } from "src/wallet-users/entities/wallet-user.entity";
import { Withdrawallet } from "src/withdrawallet/entities/withdrawallet.entity";
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class WithdraWalletDetail {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => Withdrawallet, (retiro) => retiro.detalles)
    retiro: Withdrawallet

    @ManyToOne(() => WalletUser, (wallet) => wallet.retiros)
    wallet: WalletUser

    @Column("decimal", { precision: 15, scale: 4, default: 0.0 })
    amount: number;

    @Column("decimal", { precision: 15, scale: 4, default: 0.0 })
    amount_transform: number;

    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;

    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: Date;
}
