import { Module } from '@nestjs/common';
import { WithdraWalletDetailService } from './withdra-wallet-detail.service';
import { WithdraWalletDetailController } from './withdra-wallet-detail.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WithdraWalletDetail } from './entities/withdra-wallet-detail.entity';

@Module({
  imports: [TypeOrmModule.forFeature([WithdraWalletDetail])],
  controllers: [WithdraWalletDetailController],
  providers: [WithdraWalletDetailService],
})
export class WithdraWalletDetailModule {}
