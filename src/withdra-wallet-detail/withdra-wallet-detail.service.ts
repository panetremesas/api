import { Injectable } from '@nestjs/common';
import { CreateWithdraWalletDetailDto } from './dto/create-withdra-wallet-detail.dto';
import { UpdateWithdraWalletDetailDto } from './dto/update-withdra-wallet-detail.dto';

@Injectable()
export class WithdraWalletDetailService {
  create(createWithdraWalletDetailDto: CreateWithdraWalletDetailDto) {
    return 'This action adds a new withdraWalletDetail';
  }

  findAll() {
    return `This action returns all withdraWalletDetail`;
  }

  findOne(id: number) {
    return `This action returns a #${id} withdraWalletDetail`;
  }

  update(id: number, updateWithdraWalletDetailDto: UpdateWithdraWalletDetailDto) {
    return `This action updates a #${id} withdraWalletDetail`;
  }

  remove(id: number) {
    return `This action removes a #${id} withdraWalletDetail`;
  }
}
