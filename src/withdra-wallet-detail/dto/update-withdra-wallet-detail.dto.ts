import { PartialType } from '@nestjs/mapped-types';
import { CreateWithdraWalletDetailDto } from './create-withdra-wallet-detail.dto';

export class UpdateWithdraWalletDetailDto extends PartialType(CreateWithdraWalletDetailDto) {}
