import { Injectable } from '@nestjs/common';
import { DatabaseDolarService } from 'src/database-dolar/database-service';

@Injectable()
export class DolarService {

    constructor(
        private readonly databaseService: DatabaseDolarService
    ) { }

    async getAll(status) {
        let data = null;

        if (!status) {
            // Consulta para obtener todas las transacciones, ordenadas primero por estado "creada" y luego por fecha de creación
            data = await this.databaseService.query(`
                SELECT * FROM transaction 
                ORDER BY 
                    CASE WHEN status = 'CREADA' THEN 1 ELSE 2 END, 
                    created_at DESC
            `);
        }
        else {
            // Consulta para obtener transacciones filtradas por estado, ordenadas por fecha de creación
            data = await this.databaseService.query(`
                SELECT * FROM transaction 
                WHERE status = '${status}'
                ORDER BY created_at DESC
            `);
        }

        return { data, message: 'Transacciones obtenidas con éxito' };
    }


    async getOne(id) {
        const data = await this.databaseService.query(`
            SELECT t.*, b.*
            FROM transaction t
            LEFT JOIN bank b ON t.bankId = b.id
            WHERE t.id = '${id}'
            LIMIT 1
        `);

        return { data, message: 'Transaccion obtenida con exito' }
    }

    async getOneApp(id) {
        let bank = null
        let transaction = await this.databaseService.query(` SELECT * FROM transaction WHERE id = '${id}'`);

        if (transaction[0].bankId != null) {
            bank = await this.databaseService.query(`SELECT * FROM bank WHERE id = '${transaction[0].bankId}'`)
        }

        let data = { transaction: transaction[0], bank: bank ? bank[0] : null }

        return { data, message: 'Transaccion obtenida con exito' }
    }

    async update(id, file) {
        const imageUrl = process.env.URL_IMAGENES + file;

        await this.databaseService.query(`
            UPDATE transaction
            SET status = 'COMPLETADA', comprobante = '${imageUrl}'
            WHERE id = '${id}'
        `);

        return { message: 'Transacción actualizada con éxito', data: imageUrl };
    }

}
