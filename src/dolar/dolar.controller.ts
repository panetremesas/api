import { Controller, Get, Param, Patch, Query, UploadedFile, UseInterceptors } from '@nestjs/common';
import { DolarService } from './dolar.service';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('dolar')
export class DolarController {
  constructor(private readonly dolarService: DolarService) { }

  @Get('')
  getAll(@Query("status") status) {
    return this.dolarService.getAll(status)
  }

  @Get(":id")
  getOne(@Param("id") id) {
    return this.dolarService.getOne(id)
  }

  @Patch(":id")
  @UseInterceptors(FileInterceptor("image"))
  update(@Param("id") id, @UploadedFile() file) {
    console.log(file)
    return this.dolarService.update(id, file.filename)
  }
}
