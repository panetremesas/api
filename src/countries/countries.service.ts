import { Injectable } from "@nestjs/common";
import { CreateCountryDto } from "./dto/create-country.dto";
import { UpdateCountryDto } from "./dto/update-country.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { Country } from "./entities/country.entity";
import { IsNull, Repository } from "typeorm";
import { ResponseUtil } from "src/utils/response.util";
import { TransactionsTemporary } from "src/transactions-temporary/entities/transactions-temporary.entity";
import { Transaction } from "src/transactions/entities/transaction.entity";
import { Rate } from "src/rates/entities/rate.entity";
import { User } from "src/users/entities/user.entity";
import { Account } from "src/accounts/entities/account.entity";
import { WalletUser } from "src/wallet-users/entities/wallet-user.entity";
import { TYPE_WALLET } from "src/enums/type_account.enum";
import { Notification } from "src/notifications/entities/notification.entity";

@Injectable()
export class CountriesService {
  constructor(
    @InjectRepository(Country)
    private readonly countrieRepository: Repository<Country>,
    @InjectRepository(TransactionsTemporary)
    private readonly temporalRepository: Repository<TransactionsTemporary>,
    @InjectRepository(Transaction)
    private readonly transactionRepository: Repository<Transaction>,
    @InjectRepository(Rate)
    private readonly rateRepository: Repository<Rate>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(Account)
    private readonly accountRepository: Repository<Account>,
    @InjectRepository(WalletUser)
    private readonly walletUserRepository: Repository<WalletUser>,
    @InjectRepository(Notification)
    private readonly notificationRepository: Repository<Notification>,
  ) { }

  async create(createCountryDto: CreateCountryDto) {
    const countrie = await this.countrieRepository.save(createCountryDto);

    return ResponseUtil.success(countrie, "Pais Creado con exito");
  }

  async findAll(status, report) {
    const queryBuilder = this.countrieRepository.createQueryBuilder("countrie");

    let result = [];
    if (report) {
      queryBuilder.leftJoinAndSelect(
        "countrie.wallets",
        "walletUser",
        "walletUser.type_wallet = :type",
        { type: "USER" }
      );
      queryBuilder.groupBy("countrie.id");
      queryBuilder.select([
        "countrie.id as countrieId",
        "countrie.name as name",
        "countrie.currency as currency",
        "countrie.abbreviation as flag",
        "SUM(walletUser.available) as totalAmount",
        `SUM(walletUser.available) / countrie.rate_purchase as usdtAmount`,
      ]);
      result = await queryBuilder.getRawMany();
    }

    if (status) {
      queryBuilder.andWhere("countrie.deleteAt IS NULL");
      result = await queryBuilder.getMany();
    }

    return ResponseUtil.success(result, "Paises obtenidos con exito");
  }

  async findOne(id: any) {
    const countrie = await this.countrieRepository.findOne({
      where: { id },
      relations: ["banks", "accounts"],
    });
    if (countrie == null) {
      return ResponseUtil.error("Pais no existe en la base de datos");
    }

    return ResponseUtil.success(countrie, "Pais Obtenido con exito");
  }

  update(id: any, updateCountryDto: UpdateCountryDto) {
    const countrie = this.countrieRepository.update(id, updateCountryDto);

    return ResponseUtil.success(countrie, "Pais Actualizado con exito");
  }

  async updateStatus(countrieId, status) {
    try {
      // Encuentra el país con el ID especificado
      const countrie = await this.countrieRepository.findOne({
        where: { id: countrieId },
      });

      if (!countrie) {
        return ResponseUtil.error("País no encontrado");
      }

      let value = false
      if (status == "true") {
        value = true
      }
      // Actualiza el estado del país
      const updateResult = await this.countrieRepository.update(countrieId, {
        status: value,
      });

      if (updateResult.affected === 0) {
        return ResponseUtil.error("No se pudo actualizar el status del país");
      }

      // Volver a buscar el país actualizado para devolverlo en la respuesta
      const updatedCountrie = await this.countrieRepository.findOne({
        where: { id: countrieId },
      });

      if (status) {
        let trans = await this.temporalRepository.createQueryBuilder("tran")
          .where("tran.destinationId = :countrieId", { countrieId })
          .leftJoinAndSelect("tran.origin", "origin")
          .leftJoinAndSelect("tran.destination", "destination")
          .leftJoinAndSelect("tran.creator", "creator")
          .leftJoinAndSelect("tran.account", "account")
          .leftJoinAndSelect("tran.client", "client")
          .leftJoinAndSelect("account.user", "user")
          .leftJoinAndSelect("account.instrument", "instrument")
          .where("tran.status = :status", { status: false })
          .getMany()

        for (let tran of trans) {
          let rate = await this.rateRepository.createQueryBuilder("rate")
            .where("rate.originId = :origin", { origin: tran.origin.id })
            .andWhere("rate.destinationId = :destination", { destination: tran.destination.id })
            .leftJoinAndSelect("rate.origin", "origin")
            .leftJoinAndSelect("rate.destination", "destination")
            .getOne()

          const lastTransaction = await this.transactionRepository
            .createQueryBuilder("transaction")
            .orderBy("transaction.id", "DESC")
            .getOne();

          let nextId = 1;
          if (lastTransaction) {
            nextId = lastTransaction.id + 1;
          }
          let number = Math.floor(Math.random() * 9000000) + 1000000;
          const referencia = "PT-" + number + "-" + nextId;

          const type_profit = rate.type_profit;

          const jalador = await this.userRepository.findOneBy({
            id: tran.creator.id,
          });
          
          let profit_puller = 0;
          if (jalador.profit_commission > 0) {
            profit_puller = this.calcularValorPorcentaje(
              tran.amount,
              jalador.profit_commission
            );
          }
          
          let account = await this.accountRepository.findOne({
            where: {
              id: tran.account.id,
            },
            relations: ['user']
          });

          let profit_account = 0;
          if (account.commision === true) {
            profit_account = this.calcularValorPorcentaje(
              tran.amount,
              account.profit_commission
            );
          }
          // return rate;
          let comision =
            +tran.amount * rate.origin[type_profit] -
            +tran.amount;

          let comisionHome = +comision - +profit_account - +profit_puller;

          let vWalletOrigin = null;
          vWalletOrigin = await this.walletUserRepository
            .createQueryBuilder("wallet")
            .where(
              "userId = :user AND countrieId = :countrie AND type_wallet = :type_wallet",
              {
                user: account.user.id,
                countrie: rate.origin.id,
                type_wallet: "USER",
              }
            )
            .getOne();

          if (vWalletOrigin === null) {
            vWalletOrigin = await this.walletUserRepository.save({
              countrie: rate.origin,
              user: account.user,
              available: 0,
              type_wallet: TYPE_WALLET.USER,
            });
          }

          let amount = 0
          if (rate.origin.id !== 2 && rate.origin.id !== 1) {
            amount = tran.amount * rate.rate
          }
          else {
            amount = tran.amount * rate.rate
          }

          if (rate.origin.id == 1 && rate.destination.id == 2) {
            amount = tran.amount * rate.rate
          }
          console.log(rate)
          if (rate.origin.id === 1 && rate.destination.id !== 2) {
            amount = tran.amount / rate.rate
          }

          if (rate.origin.id === 2 && rate.destination.id === 1) {
            amount = tran.amount / rate.rate
          }


          const save = await this.transactionRepository.save({
            amountSend: tran.amount,
            currencySend: tran.origin.currency,
            amountReceived: amount,
            currencyReceived: tran.destination.currency,
            dateProofOfSend: tran.fechaComprobante,
            numberProofOfSend: tran.codigo_comprobante,
            client: tran.client,
            instrument: tran.instrument,
            rate: rate,
            creator: tran.creator,
            proofOfSend: tran.comprobante,
            reference: referencia,
            puller_profit: profit_puller,
            account_profit: profit_account,
            home_profit: comisionHome,
            account: account,
            owner: tran.account.user,
            origin: rate.origin,
            destination: rate.destination,
            wallet_reception: vWalletOrigin,
            account_reception: account,
          });

          await this.temporalRepository.update(tran.id, { status: true })
          let vClient = tran.client
          let numero = vClient.phone
          let encodedMessage =
            "Hola, estimado " +
            vClient.full_name +
            " te notificamos que tu transaccion por el monto de " +
            save.amountSend +
            " " +
            save.currencySend +
            ", con destino a " +
            tran.destination.name +
            " por " +
            save.amountReceived +
            " " +
            save.currencyReceived +
            " ha sido generada con exito en nuestro sistema, en los proximos minutos te enviaremos el comprobante de procesado. \n Su Numero de Transaccion es: " +
            save.reference

          let payload = {
            message: encodedMessage,
            image: null,
            phone: numero
          }

          let encodedMessage2 =
            "Referencia: " +
            save.reference +
            "\n" +
            "Numero de Comprobante: " +
            save.numberProofOfSend +
            "Fecha y Hora: " +
            save.createdAt

          let payload3 = {
            message: encodedMessage2,
            image: tran.comprobante,
            phone: tran.account.user.phone
          }

          this.notificationRepository.save(payload)
          this.notificationRepository.save(payload3)
        }
      }

      return ResponseUtil.success(
        updatedCountrie,
        "Status del país actualizado con éxito"
      );
    } catch (error) {
      console.error("Error al actualizar el status del país:", error);
      return ResponseUtil.error("Error al actualizar el status del país");
    }
  }

  calcularValorPorcentaje(montoInicial, valorDecimal) {
    const porcentajeReal = valorDecimal - 1;
    const valorPorcentaje = montoInicial * porcentajeReal;
    return valorPorcentaje;
  }
}
