import { IsDecimal, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";
import { Entity } from "typeorm";

@Entity()
export class CreateCountryDto {
  @IsString()
  name: string;
  @IsString()
  abbreviation: string;
  @IsString()
  currency: string;
  @IsNotEmpty()
  profit: number;
  @IsNotEmpty()
  ven_profit: number;
  @IsNotEmpty()
  especial_profit: number;
  @IsNotEmpty()
  code: number
  @IsOptional()
  amount: number
}
