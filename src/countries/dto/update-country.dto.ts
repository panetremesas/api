import { PartialType } from '@nestjs/mapped-types';
import { CreateCountryDto } from './create-country.dto';
import { IsDate, IsNumber, IsNotEmpty, IsOptional } from 'class-validator';

export class UpdateCountryDto extends PartialType(CreateCountryDto) {
    @IsNotEmpty()
    rate_purchase: number;

    @IsNotEmpty()
    rate_sales: number;

    @IsNotEmpty()
    rate_wholesale: number;

    @IsOptional()
    especial_profit: any

    @IsOptional()
    ven_profit: number

    @IsOptional()
    profit: number;

    @IsOptional()
    deleteAt: Date

    @IsOptional()
    status: boolean
}
