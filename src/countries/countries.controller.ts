import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UseGuards,
} from "@nestjs/common";
import { CountriesService } from "./countries.service";
import { CreateCountryDto } from "./dto/create-country.dto";
import { UpdateCountryDto } from "./dto/update-country.dto";
import { AuthGuard } from "src/auth/auth.guard";

@Controller("countries")
export class CountriesController {
  constructor(private readonly countriesService: CountriesService) {}

  @Post()
  create(@Body() createCountryDto: CreateCountryDto) {
    return this.countriesService.create(createCountryDto);
  }

  @Get()
  findAll(@Query("status") status, @Query("report") report) {
    return this.countriesService.findAll(status, report);
  }

  @Get(":id")
  findOne(@Param("id") id: string) {
    return this.countriesService.findOne(+id);
  }

  @Patch(":id")
  update(@Param("id") id: number, @Body() updateCountryDto: UpdateCountryDto) {
    return this.countriesService.update(id, updateCountryDto);
  }

  @Get("/update-status/:countrieId/:status")
  updateStatus(@Param("countrieId") countrieId, @Param("status") status) {
    return this.countriesService.updateStatus(countrieId, status)
  }
}
