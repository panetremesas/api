import { Module } from '@nestjs/common';
import { CountriesService } from './countries.service';
import { CountriesController } from './countries.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Country } from './entities/country.entity';
import { TransactionsTemporary } from 'src/transactions-temporary/entities/transactions-temporary.entity';
import { Transaction } from 'src/transactions/entities/transaction.entity';
import { Account } from 'src/accounts/entities/account.entity';
import { Rate } from 'src/rates/entities/rate.entity';
import { User } from 'src/users/entities/user.entity';
import { WalletUser } from 'src/wallet-users/entities/wallet-user.entity';
import { Notification } from 'src/notifications/entities/notification.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Country, TransactionsTemporary, Transaction, Account, Rate, User, WalletUser, Notification  ])],
  controllers: [CountriesController],
  providers: [CountriesService],
})
export class CountriesModule { }
