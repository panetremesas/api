import { Account } from "src/accounts/entities/account.entity";
import { Bank } from "src/banks/entities/bank.entity";
import { Presupuesto } from "src/presupuesto/entities/presupuesto.entity";
import { WalletUser } from "src/wallet-users/entities/wallet-user.entity";
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class Country {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  abbreviation: string;
  
  @Column()
  code: number;

  @Column({default: 0})
  amount: number;

  @Column()
  currency: string;

  @Column("decimal", { precision: 15, scale: 2, default: 0.0, nullable: true })
  profit: number;

  @Column("decimal", { precision: 15, scale: 2, default: 0.0, nullable: true })
  ven_profit: number;

  @Column("decimal", { precision: 15, scale: 2, default: 0.0, nullable: true })
  especial_profit: number;

  @Column("decimal", { precision: 15, scale: 3, default: 0.0, nullable: true })
  rate_purchase: number;

  @Column("decimal", { precision: 15, scale: 3, default: 0.0, nullable: true })
  rate_sales: number;

  @Column("decimal", { precision: 15, scale: 3, default: 0.0, nullable: true })
  rate_wholesale: number;

  @Column({ type: 'boolean', default: true })
  status: boolean;

  @OneToMany(() => Account, (account) => account.countrie)
  accounts: Account[];

  @OneToMany(() => Bank, (bank) => bank.countrie)
  banks: Bank[];

  @OneToMany(() => Presupuesto, (bank) => bank.countrie)
  presupuestos: Presupuesto[];

  @OneToMany(() => WalletUser, (wallet) => wallet.countrie)
  wallets: WalletUser[]

  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt: Date;

  @DeleteDateColumn({type: "timestamp", nullable: true})
  deleteAt: Date
}
