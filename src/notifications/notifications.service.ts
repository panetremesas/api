import { Injectable } from '@nestjs/common';
import { CreateNotificationDto } from './dto/create-notification.dto';
import { UpdateNotificationDto } from './dto/update-notification.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Notification } from './entities/notification.entity';
import { Repository } from 'typeorm';
import { ResponseUtil } from 'src/utils/response.util';
import { STATUS_NOTIFICATION } from 'src/enums/type_account.enum';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';

@Injectable()
export class NotificationsService {

  constructor(
    @InjectRepository(Notification)
    private readonly notificationRepository: Repository<Notification>,
    @InjectQueue('notification') private notificationQueue: Queue,
  ) { }

  async create(createNotificationDto: CreateNotificationDto) {
    try {
      let data = await this.notificationRepository.save(createNotificationDto);
      console.log(`Notificación guardada en la base de datos: ${JSON.stringify(data)}`);

      return ResponseUtil.success(data, "Notificación agregada con éxito");
    } catch (error) {
      return error
    }
  }

  async findAll() {
    let data = await this.notificationRepository.find()
    return ResponseUtil.success(data, "Notificaciones obtenidas cojn exito")
  }

  findOne(id: number) {
    return `This action returns a #${id} notification`;
  }

  async updateNotificationStatus(id, status) {
    await this.notificationRepository.update(id, { status: status })
    return true
  }

  async execute() {
    const pendingNotifications = await this.notificationRepository
      .createQueryBuilder('notification')
      .where('status = :status', { status: STATUS_NOTIFICATION.PENDING })
      .getMany();

    for (const notification of pendingNotifications) {
      await this.notificationQueue.add(notification);
      await this.notificationRepository.update(notification.id, {status: STATUS_NOTIFICATION.QUEUE});
      
    }

    return ResponseUtil.success(pendingNotifications, "Notificaciones agregadas a la cola con exito")
  }

  async cola() {
    const pendingNotifications = await this.notificationRepository
      .createQueryBuilder('notification')
      .where('status = :status', { status: STATUS_NOTIFICATION.QUEUE })
      .getMany();

    for (const notification of pendingNotifications) {
      await this.notificationQueue.add(notification);
    }

    return ResponseUtil.success(pendingNotifications, "Notificaciones agregadas a la cola con exito")
  }
}
