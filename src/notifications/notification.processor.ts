// notification.processor.ts
import { Process, Processor } from '@nestjs/bull';
import { Job } from 'bull';
import { STATUS_NOTIFICATION } from 'src/enums/type_account.enum';
import axios from 'axios';
import { NotificationsService } from './notifications.service';
import { Logger } from '@nestjs/common';

const loger = new Logger("NotificationLogger")

@Processor('notification')
export class NotificationProcessor {
  
  constructor(private readonly notificationService: NotificationsService) { }

  async sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  @Process({ concurrency: 1 }) 
  async handleNotification(job: Job) {
    const notification = job.data;

    try {
      let apiUrl = ""
      const encodedMessage = encodeURIComponent(notification.message);
      if (notification.image !== null) {
        const encodedImageUrl = encodeURIComponent(notification.image);
        apiUrl = `https://api-whatsapp.paneteirl.store/send-message?number=${notification.phone}&message=${encodedMessage}&imageUrl=${encodedImageUrl}`;
      }
      else {
        apiUrl = `https://api-whatsapp.paneteirl.store/send-message/text?number=${notification.phone}&message=${encodedMessage}`;
      }

      const response = await axios.get(apiUrl);

      if (response.status === 200) {
        notification.status = STATUS_NOTIFICATION.COMPLETE;
        await this.notificationService.updateNotificationStatus(notification.id, STATUS_NOTIFICATION.COMPLETE);
        loger.log("Notificacion procesada: " + notification.id)
      }
    } catch (error) {
      console.error(`Error al ejecutar la URL para la notificación ${notification.id}:`);
    }

    await this.sleep(5000);
  }
}
