import { IsNotEmpty, IsOptional } from "class-validator"

export class CreateNotificationDto {
    @IsNotEmpty()
    phone: string

    @IsNotEmpty()
    message: string

    @IsOptional()
    image: string
}
