import { STATUS_NOTIFICATION } from "src/enums/type_account.enum";
import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Notification {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    phone: string

    @Column({ length: 2000 })
    message: string

    @Column({ length: 1000, nullable: true })
    image: string

    @Column({ type: 'enum', enum: STATUS_NOTIFICATION, default: STATUS_NOTIFICATION.PENDING })
    status: STATUS_NOTIFICATION;

    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;

    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: Date;
}
