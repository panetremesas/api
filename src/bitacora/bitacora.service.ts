import { Injectable } from '@nestjs/common';
import { CreateBitacoraDto } from './dto/create-bitacora.dto';
import { UpdateBitacoraDto } from './dto/update-bitacora.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Bitacora } from './entities/bitacora.entity';
import { Repository } from 'typeorm';

@Injectable()
export class BitacoraService {

  constructor(
    @InjectRepository(Bitacora)
    private readonly bitacoraRepository: Repository<Bitacora>
  ) { }

  async create(createBitacoraDto: CreateBitacoraDto) {
    let data = await this.bitacoraRepository.save(createBitacoraDto)

    return { data, message: 'Bitacora registrada con exito!' }
  }

  async findAll(userId, status, user) {
    let query = this.bitacoraRepository.createQueryBuilder("reg")

    if (user) {
      console.log("levanto consulta")
      query = query.where("reg.userId = :user", { user })
    }
    else {
      query = query.where("reg.userId = :userId", { userId })
    }

    if (status) {
      query = query.andWhere("reg.accion LIKE :status", { status: `%${status}%` });
    }

    let data = await query.orderBy("reg.id", "DESC").getMany()


    return { data, message: 'Obtenido con exito' }
  }

  findOne(id: number) {
    return `This action returns a #${id} bitacora`;
  }

  update(id: number, updateBitacoraDto: UpdateBitacoraDto) {
    return `This action updates a #${id} bitacora`;
  }

  remove(id: number) {
    return `This action removes a #${id} bitacora`;
  }
}
