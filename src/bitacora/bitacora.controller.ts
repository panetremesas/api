import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Request, Query } from '@nestjs/common';
import { BitacoraService } from './bitacora.service';
import { CreateBitacoraDto } from './dto/create-bitacora.dto';
import { UpdateBitacoraDto } from './dto/update-bitacora.dto';
import { AuthGuard } from 'src/auth/auth.guard';

@Controller('bitacora')
export class BitacoraController {
  constructor(private readonly bitacoraService: BitacoraService) {}

  @Post()
  create(@Body() createBitacoraDto: CreateBitacoraDto) {
    return this.bitacoraService.create(createBitacoraDto);
  }

  @Get()
  @UseGuards(AuthGuard)
  findAll(@Request() req, @Query('status') status, @Query("user") user) {
    return this.bitacoraService.findAll(req.user.id, status, user);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.bitacoraService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateBitacoraDto: UpdateBitacoraDto) {
    return this.bitacoraService.update(+id, updateBitacoraDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.bitacoraService.remove(+id);
  }
}
