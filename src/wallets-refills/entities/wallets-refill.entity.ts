import { STATUS_REFILL } from "src/enums/type_account.enum";
import { IsNumber } from "class-validator";
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { Country } from "src/countries/entities/country.entity";
import { Account } from "src/accounts/entities/account.entity";
import { User } from "src/users/entities/user.entity";
import { Expedient } from "src/expedient/entities/expedient.entity";

@Entity()
export class WalletsRefill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  amount: number;

  @Column()
  proof: string;

  @Column({ nullable: true })
  reference: string;

  @Column({ type: "date", nullable: true })
  dateProof: Date;

  @Column({ nullable: true })
  numberProof: string;

  @Column({ type: "enum", enum: STATUS_REFILL, default: STATUS_REFILL.CREADA })
  status: STATUS_REFILL;

  @Column({ type: "text", nullable: true })
  observation: string;

  @ManyToOne(() => Country)
  countrie: Country;

  @ManyToOne(() => Account)
  account: Account;

  @ManyToOne(() => User)
  owner_account: User;

  @Column("decimal", { precision: 15, scale: 3, nullable: true })
  profit_owner: number;

  @ManyToOne(() => User)
  user: User;

  @ManyToOne(() => Expedient, { nullable: true })
  expedient: Expedient;

  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt: Date;
}
