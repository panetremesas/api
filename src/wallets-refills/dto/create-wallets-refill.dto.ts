import { IsDate, IsDateString, IsNumber, IsNumberString, IsOptional, IsString } from "class-validator";
import { Account } from "src/accounts/entities/account.entity";
import { Country } from "src/countries/entities/country.entity";

export class CreateWalletsRefillDto {

    @IsNumberString()
    countrie: Country

    @IsNumberString()
    amount: number

    @IsNumberString()
    account: Account

    @IsString()
    numberProof: string

    @IsDateString()
    dateProof: string

    image: any;
}
