import { PartialType } from '@nestjs/mapped-types';
import { CreateWalletsRefillDto } from './create-wallets-refill.dto';

export class UpdateWalletsRefillDto extends PartialType(CreateWalletsRefillDto) {}
