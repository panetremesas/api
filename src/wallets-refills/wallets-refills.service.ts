import { Injectable } from "@nestjs/common";
import { CreateWalletsRefillDto } from "./dto/create-wallets-refill.dto";
import { UpdateWalletsRefillDto } from "./dto/update-wallets-refill.dto";
import { v2 as cloudinary } from "cloudinary";
import { InjectRepository } from "@nestjs/typeorm";
import { WalletsRefill } from "./entities/wallets-refill.entity";
import { Repository } from "typeorm";
import { ResponseUtil } from "src/utils/response.util";
import { Account } from "src/accounts/entities/account.entity";
import { WalletUser } from "src/wallet-users/entities/wallet-user.entity";
import {
  STATUS_QUEUE,
  STATUS_REFILL,
  TYPE_EXPEDIENT,
  TYPE_PRESUPUESTO,
  TYPE_WALLET,
} from "src/enums/type_account.enum";
import { TransWall } from "src/trans-wall/entities/trans-wall.entity";
import { User } from "src/users/entities/user.entity";
import { PushNotificationUtil } from "src/utils/push.utils";
import { Expedient } from "src/expedient/entities/expedient.entity";
import { DocumentsExpedient } from "src/documents-expedient/entities/documents-expedient.entity";
import { Queue } from "src/queues/entities/queue.entity";
import {
  TYPE_DESPACHO_ACCOUNT,
  TYPES_QUEUES,
} from "src/enums/types-queues.enum";
import { Console } from "console";

@Injectable()
export class WalletsRefillsService {
  constructor(
    @InjectRepository(WalletsRefill)
    private readonly walletRefillsRepository: Repository<WalletsRefill>,
    @InjectRepository(Account)
    private readonly accountRepository: Repository<Account>,
    @InjectRepository(WalletUser)
    private readonly walletuserRepository: Repository<WalletUser>,
    @InjectRepository(TransWall)
    private readonly transWallsRepository: Repository<TransWall>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(Expedient)
    private readonly expedientRepository: Repository<Expedient>,
    @InjectRepository(DocumentsExpedient)
    private readonly documentsExpedientRepository: Repository<DocumentsExpedient>,
    @InjectRepository(Queue)
    private readonly queueRepository: Repository<Queue>
  ) {
    cloudinary.config({
      cloud_name: "dtxr4opxp",
      api_key: "368541571316882",
      api_secret: "els_JKHH3Hf-Y4z_zjsRS-Rgc40",
    });
  }
  async create(createWalletsRefillDto: CreateWalletsRefillDto, file, user) {
    const imageUrl = process.env.URL_IMAGENES + file;

    let wallet = await this.walletuserRepository
      .createQueryBuilder("wallet")
      .where("wallet.countrieId = :countrieId", {
        countrieId: createWalletsRefillDto.countrie,
      })
      .andWhere("wallet.userId = :userId", { userId: user })
      .andWhere("wallet.type_wallet = :type_wallet", {
        type_wallet: TYPE_WALLET.TRANSACTIONS,
      })
      .getOne();

    if (!wallet) {
      wallet = await this.walletuserRepository.save({
        countrie: createWalletsRefillDto.countrie,
        user: user,
        type_wallet: TYPE_WALLET.TRANSACTIONS,
      });
    }

    const vAccount = await this.accountRepository.findOne({
      where: { id: createWalletsRefillDto.account.id },
      relations: ["user"],
    });

    let valueComission = 0;
    if (vAccount.commision) {
      valueComission =
        createWalletsRefillDto.amount * vAccount.profit_commission -
        createWalletsRefillDto.amount;
    }

    const lastRecords = await this.walletRefillsRepository.find({
      order: { id: "DESC" },
      take: 1,
    });

    const newId = lastRecords.length ? lastRecords[0].id + 1 : 1;

    const year = new Date().getFullYear();
    const reference = `REC-${year}-${newId}`;

    const data = await this.walletRefillsRepository.save({
      amount: createWalletsRefillDto.amount,
      proof: imageUrl,
      dateProof: createWalletsRefillDto.dateProof,
      numberProof: createWalletsRefillDto.numberProof,
      countrie: createWalletsRefillDto.countrie,
      account: vAccount,
      owner_account: vAccount.user,
      profit_owner: valueComission,
      user: user,
      reference, // Guardar la referencia generada
    });

    await this.queueRepository.save({
      wallet_refill: data,
      dispatcher: vAccount.user,
      type: TYPES_QUEUES.CUENTA,
      type_despacho: TYPE_DESPACHO_ACCOUNT.RECARGA,
    });

    return ResponseUtil.success(data, "Recarga Creada con exito");
  }

  async findAll(user, owner_account, home) {
    const refills = await this.walletRefillsRepository
      .createQueryBuilder("refills")
      .leftJoinAndSelect("refills.countrie", "countrie")
      .leftJoinAndSelect("refills.user", "user");
    if (!owner_account) {
      refills.andWhere("refills.userId = :userId", { userId: user.id });
    } else {
      refills.andWhere("refills.ownerAccountId = :ownerAccountId", {
        ownerAccountId: user.id,
      });
    }
    if (home === "true") {
      refills
        .andWhere("refills.status = :status", {
          status: STATUS_REFILL.CREADA,
        })
        .orWhere("refills.status = :status", {
          status: STATUS_REFILL.OBSERVADA,
        })
        .take(5);
    }
    const result = await refills.getMany();
    return ResponseUtil.success(result, "Recargas obtenidas con exito");
  }

  async findOne(id: number) {
    const refill = await this.walletRefillsRepository
      .createQueryBuilder("refills")
      .leftJoinAndSelect("refills.countrie", "countrie")
      .leftJoinAndSelect("refills.account", "account")
      .leftJoinAndSelect("refills.expedient", "expedient")
      .andWhere("refills.id = :id", { id: id })
      .getOne();

    if (refill == null) {
      return ResponseUtil.error("No se ha encontrado esa recarga");
    }

    return ResponseUtil.success(refill, "Recarga Encontrada con exito");
  }

  async aprove(id) {
    const vRefill = await this.walletRefillsRepository.findOne({
      where: { id: id },
      relations: ["countrie", "owner_account", "user"],
    });

    // suma el monto al wallet del cliente, o lo crea
    const vWalletClient = await this.walletuserRepository
      .createQueryBuilder("wallet")
      .where(
        "wallet.countrieId = :countrieId AND wallet.userId = :userId AND wallet.type_wallet = :type_wallet",
        {
          countrieId: vRefill.countrie.id,
          userId: vRefill.user.id,
          type_wallet: TYPE_WALLET.TRANSACTIONS,
        }
      )
      .getOne();
    let old_amount = 0;
    let new_amount = 0;
    if (vWalletClient === null) {
      await this.walletuserRepository.save({
        countrie: vRefill.countrie,
        user: vRefill.user,
        type_wallet: TYPE_WALLET.TRANSACTIONS,
        available: vRefill.amount,
      });
      new_amount = vRefill.amount;
    } else {
      old_amount = +vWalletClient.available;
      const newAmount = +vWalletClient.available + +vRefill.amount;
      new_amount = newAmount;
      await this.walletuserRepository.update(vWalletClient.id, {
        available: newAmount,
      });
    }

    await this.transWallsRepository.save({
      concept: "RECARGA DE SALDO",
      old_amount: old_amount,
      new_amount: new_amount,
      amount_transaction: +vRefill.amount,
      type: TYPE_PRESUPUESTO.INGRESO,
      wallet: vWalletClient,
    });

    // actualizar el estado de la recarga
    await this.walletRefillsRepository.update(id, {
      status: STATUS_REFILL.COMPLEATADA,
    });

    // PushNotificationUtil.sendPush(
    //   vRefill.user.token_puler,
    //   "Su Recarga ha sido aprovada con exito, y añadida a su Wallet"
    // );

    // sacar de cola la recarga
    await this.queueRepository.update(
      { wallet_refill: vRefill },
      { status: STATUS_QUEUE.COMPLEATADA }
    );

    return ResponseUtil.success(vRefill, "Recarga Procesada con exito");
  }

  async desaprove(data) {
    await this.walletRefillsRepository.update(data.refillId, {
      observation: data.observation,
      status: STATUS_REFILL.ANULADA,
    });

    const vRefill = await this.walletRefillsRepository.findOne({
      where: { id: data.refillId },
      relations: ["countrie", "owner_account", "user"],
    });

    // PushNotificationUtil.sendPush(
    //   vRefill.user.token_puler,
    //   "Su reacarga se ha anulado, puede en su aplicacion revisar el estado, Razon de Anulacion: " +
    //     data.observation
    // );

    await this.queueRepository.update(
      { wallet_refill: vRefill },
      { status: STATUS_QUEUE.COMPLEATADA }
    );

    return ResponseUtil.success(
      vRefill,
      "Rechazo registrado correctamente, administracion se pondra en contacto con el cliente afectado."
    );
  }

  async observer(data) {
    let expedient = null;
    // se genera el expdiente
    if (data.documents.length > 0) {
      const exp = {
        type_expedient: TYPE_EXPEDIENT.RECARGA,
        id_trans: data.refillId,
      };
      expedient = await this.expedientRepository.save(exp);
      const documents = [];
      // agrega los documentos al expediente
      data.documents.map(async (document) => {
        if (document.status) {
          const doc = {
            expedient: expedient,
            document: document.id,
          };
          await this.documentsExpedientRepository.save(doc);
        }
      });
    }

    try {
      await this.walletRefillsRepository.update(data.refillId, {
        expedient: expedient ? expedient : null,
        observation: data.observation,
        status: STATUS_REFILL.OBSERVADA,
      });
    } catch (error) {
      console.log(error);
    }

    const vRefill = await this.walletRefillsRepository.findOne({
      where: { id: data.refillId },
      relations: ["countrie", "owner_account", "user"],
    });

    // PushNotificationUtil.sendPush(
    //   vRefill.user.token_puler,
    //   "Su reacarga se ha observado, puede en su aplicacion revisar el estado, y ver el link de expediente a corregir. Razon de Observacion: " +
    //     data.observation
    // );

    await this.queueRepository.update(
      { wallet_refill: vRefill },
      { status: STATUS_QUEUE.COMPLEATADA }
    );
    return ResponseUtil.success(vRefill, "Expediente Creado");
  }

  update(id: number, updateWalletsRefillDto: UpdateWalletsRefillDto) {
    return `This action updates a #${id} walletsRefill`;
  }

  remove(id: number) {
    return `This action removes a #${id} walletsRefill`;
  }

  // funcion que carga la imagen
  async uploadImageToCloudinary(
    imageBuffer: Buffer,
    imageName: string
  ): Promise<string> {
    return new Promise((resolve, reject) => {
      const uploadStream = cloudinary.uploader.upload_stream(
        { resource_type: "auto", public_id: imageName },
        (error, result) => {
          if (error) {
            reject(`Error al subir imagen a Cloudinary: ${error.message}`);
          } else {
            resolve(result.secure_url);
          }
        }
      );

      uploadStream.end(imageBuffer);
    });
  }
}
