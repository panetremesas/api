import { Module } from '@nestjs/common';
import { WalletsRefillsService } from './wallets-refills.service';
import { WalletsRefillsController } from './wallets-refills.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WalletsRefill } from './entities/wallets-refill.entity';
import { Account } from 'src/accounts/entities/account.entity';
import { WalletUser } from 'src/wallet-users/entities/wallet-user.entity';
import { TransWall } from 'src/trans-wall/entities/trans-wall.entity';
import { User } from 'src/users/entities/user.entity';
import { Expedient } from 'src/expedient/entities/expedient.entity';
import { DocumentsExpedient } from 'src/documents-expedient/entities/documents-expedient.entity';
import { Queue } from 'src/queues/entities/queue.entity';

@Module({
  imports: [TypeOrmModule.forFeature([WalletsRefill, Account, WalletUser, TransWall, User, Expedient, DocumentsExpedient, Queue])],
  controllers: [WalletsRefillsController],
  providers: [WalletsRefillsService],
})
export class WalletsRefillsModule {}
