import { Controller, Get, Post, Body, Patch, Param, Delete, Request, Query, UseGuards } from '@nestjs/common';
import { WalletsRefillsService } from './wallets-refills.service';
import { CreateWalletsRefillDto } from './dto/create-wallets-refill.dto';
import { UpdateWalletsRefillDto } from './dto/update-wallets-refill.dto';
import { AuthGuard } from 'src/auth/auth.guard';

@Controller('wallets-refills')
@UseGuards(AuthGuard)
export class WalletsRefillsController {
  constructor(private readonly walletsRefillsService: WalletsRefillsService) {}

  @Post()
  create(@Body() createWalletsRefillDto: CreateWalletsRefillDto, @Request() req) {
    const file = null
    return this.walletsRefillsService.create(createWalletsRefillDto, file, req.user.id);
  }

  @Get()
  findAll(@Request() req, @Query('owner_account') owner_account, @Query('home') home) {
    return this.walletsRefillsService.findAll(req, owner_account, home);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.walletsRefillsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateWalletsRefillDto: UpdateWalletsRefillDto) {
    return this.walletsRefillsService.update(+id, updateWalletsRefillDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.walletsRefillsService.remove(+id);
  }
}
