import { HttpException, Injectable } from "@nestjs/common";
import { LoginAppDto } from "./dto/login.dto";
import { JwtService } from "@nestjs/jwt";
import { InjectRepository } from "@nestjs/typeorm";
import { User } from "src/users/entities/user.entity";
import { Between, Repository } from "typeorm";
import { ResponseUtil } from "src/utils/response.util";
import { compare, hash } from "bcrypt";
import { Transaction } from "src/transactions/entities/transaction.entity";
import { Client } from "src/clients/entities/client.entity";
import { TransactionsTracking } from "src/transactions-tracking/entities/transactions-tracking.entity";
import { UpdateUserDto } from "src/users/dto/update-user.dto";

export interface ClientWithTransactionCount extends Client {
  transactionsCount: number;
}

@Injectable()
export class AppsService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,

    @InjectRepository(Transaction)
    private readonly transactionRepository: Repository<Transaction>,

    @InjectRepository(TransactionsTracking)
    private readonly trackingsTransaction: Repository<TransactionsTracking>,

    @InjectRepository(Client)
    private readonly clientsRepository: Repository<Client>,

    private jwtAuthService: JwtService
  ) { }

  async login(loginAppDto: LoginAppDto) {
    const { user, password, role, tokenPush } = loginAppDto;
    const findUser = await this.userRepository.findOne({
      where: { user },
      relations: ["sucursal"],
    });

    if (!findUser) return ResponseUtil.error("Usuario no existe");

    const checkPassword = await compare(password, findUser.password);
    if (!checkPassword)
      return ResponseUtil.error("La clave no coincide con el usuario");

    if (role == "jalador") {
      if (!findUser.puller) {
        return ResponseUtil.error(
          "Este Usuario no tiene permisos de Intermediario."
        );
      }
      if (tokenPush != null) {
        await this.userRepository.update(findUser.id, {
          token_puler: tokenPush,
        });
      }
    }

    if (role == "cuenta") {
      if (!findUser.account_owner) {
        return ResponseUtil.error(
          "Este usuario no tiene permisos de Dueño de Cuenta."
        );
      }
      if (tokenPush != null) {
        await this.userRepository.update(findUser.id, {
          token_owner: tokenPush,
        });
      }
    }

    if (role == "despachador") {
      if (!findUser.dispatcher) {
        return ResponseUtil.error(
          "Este usuario no tiene permisos de Despachador."
        );
      }
      if (tokenPush != null) {
        await this.userRepository.update(findUser.id, {
          token_dispatcher: tokenPush,
        });
      }
    }

    const playload = {
      id: findUser.id,
      name: findUser.name,
      typeUser: findUser.typeUser,
      sucursalId: findUser.sucursal,
      system: "AppDuenoCuenta",
    };
    const token = this.jwtAuthService.sign(playload);

    const data = {
      user: { ...findUser, password: null },
      token: token,
    };

    return ResponseUtil.success(data, "Sesion Iniciada con exito");
  }

  async getProfile(id) {
    const user = await this.userRepository.findOne({
      where: { id },
      relations: ["wallets", "wallets.countrie"],
    });
    return ResponseUtil.success(user, "Usuario obtenido con exito");
  }

  async updateProfile(updateData, user) {
    if (updateData.password == "") {
      const update = await this.userRepository.update(user.id, {
        name: updateData.name,
        referer: updateData.referer
      });
      return ResponseUtil.success(
        update,
        "Has Actualizado correctamente tu nombre"
      );
    } else {
      const hashed = await hash(updateData.password, 10);
      const update = await this.userRepository.update(user.id, {
        name: updateData.name,
        password: hashed,
      });
      return ResponseUtil.success(
        update,
        "Has Actualizado correctamente tu nombre y clave"
      );
    }
  }

  async getTransactionsPuller(user, init) {
    let consulta = this.transactionRepository
      .createQueryBuilder("transactions")
      .where("creatorId = :creatorId", { creatorId: user.id })
      .orderBy("transactions.id", "DESC");

    if (init) {
      consulta = consulta.take(5);
    }

    const resultado = await consulta.getMany();

    return ResponseUtil.success(resultado, "Todas tus transacciones");
  }

  async getTransactionsOwnerAccount(user, status) {
    let consulta = this.transactionRepository
      .createQueryBuilder("transactions")
      .leftJoinAndSelect("transactions.origin", "origin")
      .leftJoinAndSelect("transactions.destination", "destination")
      .where("transactions.ownerId = :ownerId", { ownerId: user.id });

    if (status) {
      consulta = consulta.andWhere("transactions.status = :status", { status });
    }

    consulta = consulta.orderBy("transactions.id", "DESC");
    const resultado = await consulta.getMany();

    return ResponseUtil.success(
      resultado,
      "Todas tus transacciones: " + status
    );
  }

  async updateStatusTransaction(transaction, status) {
    const update = this.transactionRepository.update(transaction, {
      status: status,
    });

    const tracking = await this.trackingsTransaction.save({
      status: status,
      fecha: new Date(),
      transaction: transaction,
    });

    // if (status == 'APROBADA') {
    //     const transaction =
    // }

    return ResponseUtil.success(update, "Transaccion " + status + " con exito");
  }

  async getClientsPuller(user) {
    let clients = this.clientsRepository
      .createQueryBuilder("clients")
      .where("jaladorId = :jaladorId", { jaladorId: user.id });
    const resultado = await clients.getMany();
    const filtrados: ClientWithTransactionCount[] = [];

    for (const client of resultado) {
      const filtrado: ClientWithTransactionCount = {
        ...client,
        transactionsCount: 0,
      };

      const clientr = new Client();
      clientr.id = client.id;

      const transactionCount = await this.transactionRepository.count({
        where: { client: clientr },
      });

      filtrado.transactionsCount = transactionCount;
      filtrados.push(filtrado);
    }

    return ResponseUtil.success(resultado, "Todas tus clientes");
  }

  async estadisticas() {
    const clientes = await this.clientsRepository.count();
    const transactions = await this.transactionRepository.count();

    const startOfDay = new Date();
    startOfDay.setHours(0, 0, 0, 0); // Establece el inicio del día
    const endOfDay = new Date();
    endOfDay.setHours(23, 59, 59, 999); // Establece el final del día

    // Cuenta las transacciones que tienen createdAt dentro del rango de hoy
    const transactionsToday = await this.transactionRepository.count({
      where: {
        createdAt: Between(startOfDay, endOfDay),
      },
    });

    // Define el rango de fecha para las barras de los últimos 10 días
    const startDate = new Date();
    startDate.setDate(startDate.getDate() - 10);

    // Consulta para obtener el número de transacciones por día en los últimos 10 días
    const results = await this.transactionRepository
      .createQueryBuilder('transaction')
      .select('DATE(transaction.createdAt)', 'day')
      .addSelect('COUNT(*)', 'ctda')
      .where('transaction.createdAt >= :startDate', { startDate: startDate.toISOString().slice(0, 10) })
      .groupBy('day')
      .orderBy('day', 'ASC')
      .getRawMany();

    // Mapeo de resultados en el formato requerido
    const barras = results.map(result => ({
      name: new Date(result.day).toLocaleDateString('es-ES', {
        day: '2-digit',
        month: 'short'
      }),
      total: parseInt(result.ctda, 10),
    }));

    // Define la fecha de inicio para los últimos 30 días
    const startDate30Days = new Date();
    startDate30Days.setDate(startDate30Days.getDate() - 30);

    // Consulta para obtener el número de transacciones por creador en los últimos 30 días
    const transactionsByCreator = await this.transactionRepository
      .createQueryBuilder('transaction')
      .leftJoinAndSelect("transaction.creator", "creator")
      .select("creator.id", "creatorId")
      .addSelect("creator.name", "creatorName")
      .addSelect("COUNT(transaction.id)", "transactionCount")
      .where('transaction.createdAt >= :startDate30Days', { startDate30Days: startDate30Days.toISOString().slice(0, 10) })
      .groupBy("creator.id")
      .orderBy("transactionCount", "DESC")
      .getRawMany();

    // Formateamos los datos para incluirlos en la respuesta
    const creatorsData = transactionsByCreator.map(result => ({
      name: result.creatorName,
      total: parseInt(result.transactionCount, 10),
    }));

    return ResponseUtil.success(
      { clientes, transactions, transactionsToday, barras, creatorsData },
      "Estadísticas obtenidas"
    );
  }

}
