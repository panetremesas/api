import {
  Body,
  Controller,
  Post,
  Get,
  UseGuards,
  Request,
  Param,
  UseInterceptors,
  UploadedFile,
  Query,
  Patch,
} from "@nestjs/common";
import { AppsService } from "./apps.service";
import { LoginAppDto } from "./dto/login.dto";
import { AuthGuard } from "src/auth/auth.guard";
import { ClientsService } from "src/clients/clients.service";
import { CreateClientDto } from "src/clients/dto/create-client.dto";
import { CreateClientInstrumentDto } from "src/client-instruments/dto/create-client-instrument.dto";
import { ClientInstrumentsService } from "src/client-instruments/client-instruments.service";
import { CountriesService } from "src/countries/countries.service";
import { RatesService } from "src/rates/rates.service";
import { AccountsService } from "src/accounts/accounts.service";
import { TransactionsService } from "src/transactions/transactions.service";
import { FileInterceptor } from "@nestjs/platform-express";
import { Express } from "express";
import { CreateWalletsRefillDto } from "src/wallets-refills/dto/create-wallets-refill.dto";
import { WalletsRefillsService } from "src/wallets-refills/wallets-refills.service";
import { WalletUsersService } from "src/wallet-users/wallet-users.service";
import { WithdrawalletService } from "../withdrawallet/withdrawallet.service";
import { CreateWithdrawalletDto } from "src/withdrawallet/dto/create-withdrawallet.dto";
import { DocumentsService } from "src/documents/documents.service";
import { ExpedientService } from "src/expedient/expedient.service";
import { TransWallService } from "src/trans-wall/trans-wall.service";
import { BanksService } from "src/banks/banks.service";
import { QueuesService } from "src/queues/queues.service";
import { UpdateUserDto } from "src/users/dto/update-user.dto";
import { DolarService } from "src/dolar/dolar.service";
import { TransactionsTemporaryService } from "src/transactions-temporary/transactions-temporary.service";
import { CreateTransactionsTemporaryDto } from "src/transactions-temporary/dto/create-transactions-temporary.dto";
import { STATUS_QUEUE } from "src/enums/type_account.enum";
import { TYPES_QUEUES } from "src/enums/types-queues.enum";

@Controller("apps")
export class AppsController {
  constructor(
    private readonly appsService: AppsService,
    private readonly clientsService: ClientsService,
    private readonly clientInstrumentService: ClientInstrumentsService,
    private readonly countriesService: CountriesService,
    private readonly ratesService: RatesService,
    private readonly accountsService: AccountsService,
    private readonly transactionService: TransactionsService,
    private readonly walletsRefillsService: WalletsRefillsService,
    private readonly walletUserService: WalletUsersService,
    private readonly withdrawalletService: WithdrawalletService,
    private readonly documentsService: DocumentsService,
    private readonly expedientService: ExpedientService,
    private readonly transWalletService: TransWallService,
    private readonly bankService: BanksService,
    private readonly queueService: QueuesService,
    private readonly dolarService: DolarService,
    private readonly transaccionesTeporalesService: TransactionsTemporaryService
  ) { }

  @Post("login")
  login(@Body() dataLogin: LoginAppDto) {
    return this.appsService.login(dataLogin);
  }

  @UseGuards(AuthGuard)
  @Get("profile")
  profile(@Request() req) {
    return this.appsService.getProfile(req.user.id);
  }

  @UseGuards(AuthGuard)
  @Patch("profile")
  update(@Request() req, @Body() updateUserDto: UpdateUserDto) {
    return this.appsService.updateProfile(updateUserDto, req.user);
  }

  @UseGuards(AuthGuard)
  @Post("updateProfile")
  updateProfile(@Body() dataUpdate, @Request() req) {
    return this.appsService.updateProfile(dataUpdate, req.user);
  }

  // Proceso de cliente
  @UseGuards(AuthGuard)
  @Get("clients/consult/:document")
  findOne(@Param("document") document: string) {
    return this.clientsService.findOne(document);
  }

  @UseGuards(AuthGuard)
  @Post("clients")
  storeClient(@Body() createClientDto: CreateClientDto) {
    return this.clientsService.create(createClientDto);
  }

  @UseGuards(AuthGuard)
  @Get("clients/instruments/:idCliente")
  getInstrumenClient(
    @Param("idCliente") idCliente,
    @Query("document") document
  ) {
    return this.clientsService.findById(idCliente, document);
  }

  @UseGuards(AuthGuard)
  @Post("clients/instruments")
  storeInstrumenClient(
    @Body() createClientInstrumentDto: CreateClientInstrumentDto
  ) {
    return this.clientInstrumentService.create(createClientInstrumentDto);
  }

  // gets para datos generales
  @UseGuards(AuthGuard)
  @Get("countries")
  getCountries(@Query("status") status, @Query("report") report) {
    return this.countriesService.findAll(status, report);
  }

  @UseGuards(AuthGuard)
  @Get("countries/bank/:idCountrie")
  getBanksCountrie(@Param("idCountrie") idCountrie) {
    return this.countriesService.findOne(idCountrie);
  }

  @UseGuards(AuthGuard)
  @Get("rates/origen/actives/:idPaisOrigen")
  fidnOriginActives(@Param("idPaisOrigen") idPaisOrigen: number) {
    return this.ratesService.fidnOriginActive(idPaisOrigen);
  }

  @UseGuards(AuthGuard)
  @Get("accounst/byCountrie/:id")
  findByCountrie(@Param("id") id: number) {
    return this.accountsService.findByCountrieReception(id);
  }

  // transacciones
  @UseGuards(AuthGuard)
  @Post("transactions/create")
  @UseInterceptors(FileInterceptor("image"))
  storeTransaction(@Body() createTransactionDto, @UploadedFile() file) {
    return this.transactionService.create(createTransactionDto, file.filename);
  }

  @UseGuards(AuthGuard)
  @Post("transactions/create/free")
  @UseInterceptors(FileInterceptor("image"))
  storeTransactionFree(@Body() createTransactionDto) {
    return this.transactionService.createfree(createTransactionDto);
  }

  @UseGuards(AuthGuard)
  @Post("wallets-refills")
  @UseInterceptors(FileInterceptor("image"))
  storeRefil(
    @Body() createWalletsRefillDto: CreateWalletsRefillDto,
    @UploadedFile() file,
    @Request() req
  ) {
    return this.walletsRefillsService.create(
      createWalletsRefillDto,
      file.filename,
      req.user.id
    );
  }

  @UseGuards(AuthGuard)
  @Get("wallets-refills")
  getWalletsRefills(
    @Request() req,
    @Query("owner_account") owner_account,
    @Query("home") home
  ) {
    return this.walletsRefillsService.findAll(req.user, owner_account, home);
  }

  @UseGuards(AuthGuard)
  @Get("wallets-refills/:id")
  getWalletsRefillsOne(@Param("id") id) {
    return this.walletsRefillsService.findOne(id);
  }

  @UseGuards(AuthGuard)
  @Get("wallets-refills/aprove/:id")
  walletRefillAprove(@Param("id") id) {
    return this.walletsRefillsService.aprove(id);
  }

  @UseGuards(AuthGuard)
  @Post("wallets-refills/desaprove")
  walletRefillDesprove(@Body() data) {
    return this.walletsRefillsService.desaprove(data);
  }

  @UseGuards(AuthGuard)
  @Post("wallets-refills/observer")
  walletRefillObserver(@Body() data) {
    return this.walletsRefillsService.observer(data);
  }

  @UseGuards(AuthGuard)
  @Get("wallet-users")
  findAll(
    @Query("type") type_wallet,
    @Query("userId") userId,
    @Query("countrieId") countrieId
  ) {
    return this.walletUserService.findAll(type_wallet, userId, countrieId);
  }

  @UseGuards(AuthGuard)
  @Post("withdrawallet")
  storeWithdrawallet(@Body() createWithdrawalletDto, @Request() req) {
    return this.withdrawalletService.create(createWithdrawalletDto, req.user);
  }

  @UseGuards(AuthGuard)
  @Patch("withdrawallet/:id")
  updatewithdrawallet(@Param("id") id: number, @Body() updateWithdrawalletDto) {
    return this.withdrawalletService.update(id, updateWithdrawalletDto);
  }

  @UseGuards(AuthGuard)
  @Get("withdrawallet")
  getWithdrawallet(@Query("userId") userId) {
    return this.withdrawalletService.findAll(userId);
  }

  @UseGuards(AuthGuard)
  @Get("transactions/:idTransaction")
  getTransaction(@Param("idTransaction") idTransaction: number) {
    return this.transactionService.findOne(idTransaction);
  }

  @UseGuards(AuthGuard)
  @Post("transactions/aprove/:id")
  aproveTransaction(@Param("id") id, @Request() req) {
    return this.transactionService.aprove(id, req.user);
  }

  @UseGuards(AuthGuard)
  @Post("transactions/desaprove")
  desaaproveTransaction(@Body("") data) {
    return this.transactionService.desaprove(data);
  }

  @UseGuards(AuthGuard)
  @Post("transactions/penalizar")
  penalizarTransaccion(@Body("") data) {
    console.log("se esta penalizando");
    return this.transactionService.penalizar(data);
  }

  @UseGuards(AuthGuard)
  @Post("transactions/observer")
  observerTransaction(@Body("") data) {
    return this.transactionService.observer(data);
  }

  @UseGuards(AuthGuard)
  @Get("transactions/validate-comprobante/:numero/:fecha")
  validateComprobante(
    @Param("numero") numero: number,
    @Param("fecha") fecha: Date
  ) {
    return this.transactionService.findComprobante(numero, fecha);
  }

  @UseGuards(AuthGuard)
  @Post("transaction/complete")
  @UseInterceptors(FileInterceptor("image"))
  completeTransaction(@Body() data, @UploadedFile() file, @Request() req) {
    return this.transactionService.complete(data, file.filename, req.user);
  }

  @UseGuards(AuthGuard)
  @Get("getTransactions/puller")
  getTransactionsPuller(@Request() req, @Query("init") init) {
    return this.appsService.getTransactionsPuller(req.user, init);
  }

  @UseGuards(AuthGuard)
  @Get("getTransactions/owner-account")
  getTransactionsOwner(@Request() req, @Query("status") status) {
    console.log("esta ejecutando aqui");
    return this.queueService.getOperations(
      false,
      req.user,
      status,
      TYPES_QUEUES.CUENTA
    );
  }

  @UseGuards(AuthGuard)
  @Get("getTransactions/owner-account-history")
  getTransactionsOwnerAccount(@Request() req, @Query("status") status) {
    console.log("esta ejecutando aqui");
    return this.appsService.getTransactionsOwnerAccount(req.user, status);
  }

  @UseGuards(AuthGuard)
  @Get("update-status-transaction")
  updateStatusTransaction(
    @Query("transaction") transaction,
    @Query("status") status
  ) {
    return this.appsService.updateStatusTransaction(transaction, status);
  }

  @UseGuards(AuthGuard)
  @Get("getClients/puller")
  getClientsPuller(@Request() req) {
    return this.appsService.getClientsPuller(req.user);
  }

  @UseGuards(AuthGuard)
  @Get("client/detail/:id")
  clientDetail(@Param("id") id: string, @Query("document") document) {
    return this.clientsService.findById(id, document);
  }

  @UseGuards(AuthGuard)
  @Get("documents")
  getDocument() {
    return this.documentsService.findAll();
  }

  @UseGuards(AuthGuard)
  @Get("expedient/:id")
  getExpedient(@Param("id") id: string) {
    return this.expedientService.findOne(id);
  }

  @UseGuards(AuthGuard)
  @Get("owner-account-report")
  getOwnerAccountReport(@Query("userId") userId, @Query("date") date) {
    return this.transWalletService.generateReporstProfitsOwner(userId, date);
  }

  @UseGuards(AuthGuard)
  @Get("banks")
  getBanks(@Query("countrie") countrie) {
    return this.bankService.findAll(countrie);
  }

  @UseGuards(AuthGuard)
  @Get("accounts")
  getAccount(
    @Query("countrie") countrie,
    @Query("bank") bank,
    @Query("userId") userId,
    @Query("param") status,
    @Request() req,
    @Query("tipo") tipo
  ) {
    return this.accountsService.findAll(
      countrie,
      bank,
      userId,
      status,
      req.user.id,
      tipo,
      "APP"
    );
  }

  // despachador
  @UseGuards(AuthGuard)
  @Get("operations")
  getOperations(@Query("home") home, @Query("status") status, @Request() req) {
    return this.queueService.getOperations(home, req.user, status);
  }

  @UseGuards(AuthGuard)
  @Get("operation/:id")
  getOperation(@Param("id") id) {
    return this.queueService.getOne(id);
  }

  @UseGuards(AuthGuard)
  @Get("estadisticas")
  estadisticas() {
    return this.appsService.estadisticas();
  }

  @UseGuards(AuthGuard)
  @Get("dolar")
  getAllDolar(@Query("status") status) {
    return this.dolarService.getAll(status);
  }

  @UseGuards(AuthGuard)
  @Get("dolar/get-transaction/:id")
  getOneDollar(@Param("id") id) {
    return this.dolarService.getOneApp(id);
  }

  @Get("versions")
  getVersions() {
    return {
      intermediarios: "1.2.0",
      despachadores: "1.1.0",
      duenos: "128",
    };
  }

  @Post("generate-comporobante")
  generateComprobante(@Body() data) {
    return this.transactionService.generateComprobante(data);
  }

  //transacciones fuera de linea
  @Post("generate-temporal")
  @UseGuards(AuthGuard)
  @UseInterceptors(FileInterceptor("image"))
  generateTemporal(
    @UploadedFile() file: Express.Multer.File,
    @Body() createTransactionsTemporaryDto
  ) {
    return this.transaccionesTeporalesService.create(
      createTransactionsTemporaryDto,
      file
    );
  }

  @Get("temporales")
  @UseGuards(AuthGuard)
  getTemporales(@Request() req, @Query("status") status) {
    return this.transaccionesTeporalesService.findAll(req.user, status);
  }

  @Get("temporal/:id")
  @UseGuards(AuthGuard)
  getTemporal(@Param("id") id) {
    return this.transaccionesTeporalesService.findOne(id);
  }

  @Get('wallets/user/transactions')
  @UseGuards(AuthGuard)
  walletsTransactions(@Request() req) {
    return this.walletUserService.walletsTransactions(req.user)
  }
}
