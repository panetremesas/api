import { Module } from '@nestjs/common';
import { AppsService } from './apps.service';
import { AppsController } from './apps.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { Client } from 'src/clients/entities/client.entity';
import { ClientsService } from 'src/clients/clients.service';
import { ClientInstrumentsService } from 'src/client-instruments/client-instruments.service';
import { ClientInstrument } from 'src/client-instruments/entities/client-instrument.entity';
import { CountriesService } from 'src/countries/countries.service';
import { Country } from 'src/countries/entities/country.entity';
import { RatesService } from 'src/rates/rates.service';
import { Rate } from 'src/rates/entities/rate.entity';
import { AccountsService } from 'src/accounts/accounts.service';
import { Account } from 'src/accounts/entities/account.entity';
import { BanksService } from 'src/banks/banks.service';
import { Bank } from 'src/banks/entities/bank.entity';
import { TransactionsService } from 'src/transactions/transactions.service';
import { Transaction } from 'src/transactions/entities/transaction.entity';
import { TransactionsTrackingService } from 'src/transactions-tracking/transactions-tracking.service';
import { TransactionsTracking } from 'src/transactions-tracking/entities/transactions-tracking.entity';
import { WalletUser } from 'src/wallet-users/entities/wallet-user.entity';
import { WalletUsersService } from 'src/wallet-users/wallet-users.service';
import { WalletsRefill } from 'src/wallets-refills/entities/wallets-refill.entity';
import { WalletsRefillsService } from 'src/wallets-refills/wallets-refills.service';
import { Withdrawallet } from 'src/withdrawallet/entities/withdrawallet.entity';
import { WithdrawalletService } from 'src/withdrawallet/withdrawallet.service';
import { TransWall } from 'src/trans-wall/entities/trans-wall.entity';
import { TransWallService } from 'src/trans-wall/trans-wall.service';
import { PresupuestoService } from 'src/presupuesto/presupuesto.service';
import { Presupuesto } from 'src/presupuesto/entities/presupuesto.entity';
import { DocumentsService } from 'src/documents/documents.service';
import { Document } from 'src/documents/entities/document.entity';
import { ExpedientService } from 'src/expedient/expedient.service';
import { Expedient } from 'src/expedient/entities/expedient.entity';
import { DocumentsExpedient } from 'src/documents-expedient/entities/documents-expedient.entity';
import { DocumentsExpedientService } from 'src/documents-expedient/documents-expedient.service';
import { QueuesService } from 'src/queues/queues.service';
import { Queue } from 'src/queues/entities/queue.entity';
import { Notification } from 'src/notifications/entities/notification.entity';
import { NotificationsModule } from 'src/notifications/notifications.module';
import { DolarService } from 'src/dolar/dolar.service';
import { DatabaseDolarService } from 'src/database-dolar/database-service';
import { TransactionsTemporary } from 'src/transactions-temporary/entities/transactions-temporary.entity';
import { TransactionsTemporaryService } from 'src/transactions-temporary/transactions-temporary.service';
import { Bitacora } from 'src/bitacora/entities/bitacora.entity';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname, join } from 'path';
import { WithdraWalletDetail } from 'src/withdra-wallet-detail/entities/withdra-wallet-detail.entity';
import { WithdraWalletDetailService } from 'src/withdra-wallet-detail/withdra-wallet-detail.service';
const uploadDir = join(process.cwd(), 'uploads');

@Module({
  imports: [
    MulterModule.register({
      storage: diskStorage({
        destination: (req, file, cb) => {
          cb(null, uploadDir);
        },
        filename: (req, file, cb) => {
          const ext = extname(file.originalname);
          const filename = `${Date.now()}${ext}`;
          cb(null, filename);
        },
      }),
      fileFilter: (req, file, cb) => {
        if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
          cb(null, true);
        } else {
          cb(new Error('Only images are allowed...'), false);
        }
      },
    }),
    TypeOrmModule.forFeature([
      User,
      Client,
      ClientInstrument,
      Country,
      Rate,
      Account,
      Bank,
      Transaction,
      TransactionsTracking,
      WalletUser,
      WalletsRefill,
      Withdrawallet,
      TransWall,
      Presupuesto,
      Document,
      Expedient,
      DocumentsExpedient,
      Queue,
      Notification,
      TransactionsTemporary,
      Bitacora,
      WithdraWalletDetail,
      NotificationsModule
    ])],
  controllers: [AppsController],
  providers: [
    AppsService,
    ClientsService,
    ClientInstrumentsService,
    CountriesService,
    RatesService,
    AccountsService,
    BanksService,
    TransactionsService,
    TransactionsTrackingService,
    WalletUsersService,
    WalletsRefillsService,
    WithdrawalletService,
    TransWallService,
    PresupuestoService,
    DocumentsService,
    ExpedientService,
    DocumentsExpedientService,
    QueuesService,
    DolarService,
    DatabaseDolarService,
    WithdraWalletDetailService,
    TransactionsTemporaryService
  ]
})
export class AppsModule { }
