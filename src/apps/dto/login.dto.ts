import { IsNumber, IsString } from "class-validator";
import { Entity } from "typeorm";

export class LoginAppDto {
  @IsString()
  user: string;
  @IsString()
  password: string;
  @IsString()
  role: string
  @IsString()
  tokenPush: string;
}
