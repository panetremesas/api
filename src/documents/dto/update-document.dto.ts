import { PartialType } from "@nestjs/mapped-types";
import { CreateDocumentDto } from "./create-document.dto";
import { IsOptional } from "class-validator";
import { TYPE_DOCUMENT } from "src/enums/type_account.enum";

export class UpdateDocumentDto extends PartialType(CreateDocumentDto) {
  @IsOptional()
  document: string;

  @IsOptional()
  descripcion: string;

  @IsOptional()
  type: TYPE_DOCUMENT;
}
