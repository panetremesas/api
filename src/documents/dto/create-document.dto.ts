import { IsString } from "class-validator";
import { TYPE_DOCUMENT } from "src/enums/type_account.enum";

export class CreateDocumentDto {
  @IsString()
  document: string;

  @IsString()
  descripcion: string;

  @IsString()
  type: TYPE_DOCUMENT;
}
