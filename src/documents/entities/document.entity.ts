import { TYPE_DOCUMENT } from "src/enums/type_account.enum";
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class Document {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  document: string;

  @Column({ type: "text", nullable: true })
  descripcion: string;

  @Column({type: 'enum', enum: TYPE_DOCUMENT, default: TYPE_DOCUMENT.IMAGEN})
  type: TYPE_DOCUMENT

  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt: Date;

  @DeleteDateColumn({ type: "timestamp" })
  deleteAt: Date;
}
