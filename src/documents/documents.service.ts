import { Injectable } from "@nestjs/common";
import { CreateDocumentDto } from "./dto/create-document.dto";
import { UpdateDocumentDto } from "./dto/update-document.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { Document } from "./entities/document.entity";
import { Repository } from "typeorm";
import { ResponseUtil } from "src/utils/response.util";
import { Expo } from "expo-server-sdk";

@Injectable()
export class DocumentsService {
  constructor(
    @InjectRepository(Document)
    private readonly documentRepository: Repository<Document>
  ) {}
  async create(createDocumentDto: CreateDocumentDto) {
    const document = await this.documentRepository.save(createDocumentDto);

    return ResponseUtil.success(document, "Documento registrado con exito");
  }

  async findAll() {
    const documents = await this.documentRepository.find();
    return ResponseUtil.success(
      documents,
      "Listado de documentos obetenidos con exito"
    );
  }

  findOne(id: number) {
    return `This action returns a #${id} document`;
  }

  async update(id: number, updateDocumentDto: UpdateDocumentDto) {
    const data = await this.documentRepository.update(id, updateDocumentDto);
    return ResponseUtil.success(data, "Actualizazdo con exito");
  }

  remove(id: number) {
    return `This action removes a #${id} document`;
  }
}
