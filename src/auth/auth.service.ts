import { HttpException, Injectable } from '@nestjs/common';
import { LoginAuthDto } from './dto/login-auth.dto';
import { Repository } from 'typeorm';
import { User } from 'src/users/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { compare } from 'bcrypt'
import { JwtService } from '@nestjs/jwt';
import { Bitacora } from 'src/bitacora/entities/bitacora.entity';

@Injectable()
export class AuthService {

  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(Bitacora)
    private readonly bitacoraRepository: Repository<Bitacora>,
    private jwtAuthService: JwtService
  ) { }

  async login(loginAuthDto: LoginAuthDto) {
    const { user, password } = loginAuthDto
    const findUser = await this.userRepository.findOne({ where: { user }, relations: ['sucursal'] })
    if (!findUser) throw new HttpException('USER_NOT_FOUND', 404)

    const checkPassword = await compare(password, findUser.password)
    if (!checkPassword) throw new HttpException('PASSWORD_ERROR', 403)

    const playload = {
      id: findUser.id,
      name: findUser.name,
      typeUser: findUser.typeUser,
      sucursalId: findUser.sucursal,
      puller: findUser.puller,
      account_owner: findUser.account_owner,
      system: 'ApiRemesas'
    }

    const token = this.jwtAuthService.sign(playload)

    const data = {
      user: { ...findUser, password: null },
      token: token
    }

    await this.bitacoraRepository.save({
      user: findUser,
      accion: 'Inicio de sesion'
    })

    return data
  }


}

