import { Module } from '@nestjs/common';
import { TransactionsTemporaryService } from './transactions-temporary.service';
import { TransactionsTemporaryController } from './transactions-temporary.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TransactionsTemporary } from './entities/transactions-temporary.entity';
import { Client } from 'src/clients/entities/client.entity';
import { Notification } from 'src/notifications/entities/notification.entity';

@Module({
  imports: [TypeOrmModule.forFeature([TransactionsTemporary, Client, Notification])],
  controllers: [TransactionsTemporaryController],
  providers: [TransactionsTemporaryService],
})
export class TransactionsTemporaryModule {}
