import { IsNotEmpty, IsNumber } from "class-validator";
import { Account } from "src/accounts/entities/account.entity";
import { ClientInstrument } from "src/client-instruments/entities/client-instrument.entity";
import { Client } from "src/clients/entities/client.entity";
import { Country } from "src/countries/entities/country.entity";
import { User } from "src/users/entities/user.entity";

export class CreateTransactionsTemporaryDto {
   @IsNotEmpty()
   amount: number

   @IsNotEmpty()
   origin: Country

   @IsNotEmpty()
   comprobante: string

   @IsNotEmpty()
   codigo_comprobante: string

   @IsNotEmpty()
   fecha_comprobante: string

   @IsNotEmpty()
   destination: Country

   @IsNotEmpty()
   client: Client

   @IsNotEmpty()
   instrument: ClientInstrument

   @IsNotEmpty()
   creator: User

   @IsNotEmpty()
   account: Account
}
