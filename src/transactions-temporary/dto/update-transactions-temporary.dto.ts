import { PartialType } from '@nestjs/mapped-types';
import { CreateTransactionsTemporaryDto } from './create-transactions-temporary.dto';

export class UpdateTransactionsTemporaryDto extends PartialType(CreateTransactionsTemporaryDto) {}
