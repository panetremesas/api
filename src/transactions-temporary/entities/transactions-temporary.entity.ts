import { Account } from "src/accounts/entities/account.entity";
import { ClientInstrument } from "src/client-instruments/entities/client-instrument.entity";
import { Client } from "src/clients/entities/client.entity";
import { Country } from "src/countries/entities/country.entity";
import { User } from "src/users/entities/user.entity";
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class TransactionsTemporary {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("decimal", { precision: 15, scale: 2, default: 0.0 })
  amount: number;

  @Column()
  comprobante: string

  @Column()
  codigo_comprobante: string

  @Column({ type: "date" })
  fechaComprobante: Date;

  @ManyToOne(() => Country)
  origin: Country;

  @ManyToOne(() => Country)
  destination: Country;

  @ManyToOne(() => Client, (client) => client.transactions)
  client: Client;

  @ManyToOne(() => User, (user) => user.transactions)
  creator: User;

  @ManyToOne(() => ClientInstrument, (instrument) => instrument.transactions)
  instrument: ClientInstrument;

  @ManyToOne(() => Account, (account) => account.transactions_temporales, {nullable: true})
  account: Account;

  @Column({ type: 'boolean', default: false })
  status: boolean

  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt: Date;

  @DeleteDateColumn({ type: "timestamp" })
  deleteAt: Date;
}
