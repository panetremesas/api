import { Controller, Get, Post, Body, Patch, Param, Delete, Request, Query, UseGuards } from '@nestjs/common';
import { TransactionsTemporaryService } from './transactions-temporary.service';
import { CreateTransactionsTemporaryDto } from './dto/create-transactions-temporary.dto';
import { UpdateTransactionsTemporaryDto } from './dto/update-transactions-temporary.dto';
import { AuthGuard } from 'src/auth/auth.guard';

@Controller('transactions-temporary')
export class TransactionsTemporaryController {
  constructor(private readonly transactionsTemporaryService: TransactionsTemporaryService) {}

  // @Post()
  // create(@Body() createTransactionsTemporaryDto: CreateTransactionsTemporaryDto) {
  //   return this.transactionsTemporaryService.create(createTransactionsTemporaryDto);
  // }

  @Get()
  @UseGuards(AuthGuard)
  findAll(@Request() req, @Query("status") status) {
    return this.transactionsTemporaryService.findAll(req.user, status);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.transactionsTemporaryService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTransactionsTemporaryDto: UpdateTransactionsTemporaryDto) {
    return this.transactionsTemporaryService.update(+id, updateTransactionsTemporaryDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.transactionsTemporaryService.remove(+id);
  }
}
