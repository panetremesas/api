import { Injectable, Request } from '@nestjs/common';
import { CreateTransactionsTemporaryDto } from './dto/create-transactions-temporary.dto';
import { UpdateTransactionsTemporaryDto } from './dto/update-transactions-temporary.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { TransactionsTemporary } from './entities/transactions-temporary.entity';
import { Repository } from 'typeorm';
import { v2 as cloudinary } from "cloudinary";
import { Client } from 'src/clients/entities/client.entity';
import axios from 'axios';
import { Notification } from 'src/notifications/entities/notification.entity';

@Injectable()
export class TransactionsTemporaryService {

  constructor(
    @InjectRepository(TransactionsTemporary)
    private readonly temporaryRepository: Repository<TransactionsTemporary>,
    @InjectRepository(Client)
    private readonly clientRepository: Repository<Client>,
    @InjectRepository(Notification)
    private readonly notificationRepository: Repository<Notification>,
  ) {
    cloudinary.config({
      cloud_name: "dtxr4opxp",
      api_key: "368541571316882",
      api_secret: "els_JKHH3Hf-Y4z_zjsRS-Rgc40",
    });
  }

  async create(createTransactionsTemporaryDto: CreateTransactionsTemporaryDto, file) {
    const imageUrl = await this.uploadImageToCloudinary(
      file.buffer,
      file.originalname
    );

    let data = await this.temporaryRepository.save({
      amount: createTransactionsTemporaryDto.amount,
      comprobante: imageUrl,
      codigo_comprobante: createTransactionsTemporaryDto.codigo_comprobante,
      fechaComprobante: createTransactionsTemporaryDto.fecha_comprobante,
      origin: createTransactionsTemporaryDto.origin,
      destination: createTransactionsTemporaryDto.destination,
      client: createTransactionsTemporaryDto.client,
      creator: createTransactionsTemporaryDto.creator,
      instrument: createTransactionsTemporaryDto.instrument,
      account: createTransactionsTemporaryDto.account
    })

    let vClient = await this.temporaryRepository.findOne({where: {id: data.id}, relations: ['client']})

    let encodedMessage = "Tienes una transaccion en espera, cuando aperturemos nuevamente operaciones en ese pais, te llegara la notificacion de procesamiento de la misma. *Esto se puede dar, por que realizaste la misma fuera de nuestro horario laboral, o en su defecto no tenemos tasa activa en el momento*, agradecemos tu paciencia. ¡Pronto Recibiras noticias de nosotros por este mismo medio!"

    let payload = {
      message: encodedMessage,
      image: null,
      phone: vClient.client.phone
    }

    let notificacion = this.notificationRepository.save(payload)
    await axios.get(process.env.URL_QUEUE_NOTIFICATIONS)

    return { data, message: 'Transaccion temporal creada con exito' }
  }

  async findAll(user, status) {
    let query = this.temporaryRepository.createQueryBuilder("trans")
      .leftJoinAndSelect("trans.origin", "origin")
      .leftJoinAndSelect("trans.destination", "destination")

    if (user.typeUser === "POPERARIO") {
      query = query.where("trans.creatorId = :creatorId", { creatorId: user.id })
    }

    if (status) {
      query = query.where("trans.status = :status", { status })
    }

    let data = await query.orderBy("trans.id", "DESC").getMany()

    return { data, message: 'Transaccciones obtenidas con exito' }

  }

  async findOne(id: number) {
    let data = await this.temporaryRepository.findOne({ where: { id }, relations: ['origin', 'destination', 'client', 'creator', 'instrument',] })

    return { data, message: 'Transaccion obtenida con exito!' }
  }

  update(id: number, updateTransactionsTemporaryDto: UpdateTransactionsTemporaryDto) {
    return `This action updates a #${id} transactionsTemporary`;
  }

  remove(id: number) {
    return `This action removes a #${id} transactionsTemporary`;
  }

  async uploadImageToCloudinary(
    imageBuffer: Buffer,
    imageName: string
  ): Promise<string> {
    return new Promise((resolve, reject) => {
      const uploadStream = cloudinary.uploader.upload_stream(
        { resource_type: "auto", public_id: imageName },
        (error, result) => {
          if (error) {
            reject(`Error al subir imagen a Cloudinary: ${error.message}`);
          } else {
            resolve(result.secure_url);
          }
        }
      );

      uploadStream.end(imageBuffer);
    });
  }
}
