import { Account } from "src/accounts/entities/account.entity";
import { ClientInstrument } from "src/client-instruments/entities/client-instrument.entity";
import { Client } from "src/clients/entities/client.entity";
import { Country } from "src/countries/entities/country.entity";
import { Expedient } from "src/expedient/entities/expedient.entity";
import { Rate } from "src/rates/entities/rate.entity";
import { TransactionsTracking } from "src/transactions-tracking/entities/transactions-tracking.entity";
import { User } from "src/users/entities/user.entity";
import { WalletUser } from "src/wallet-users/entities/wallet-user.entity";
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class Transaction {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  reference: string;

  @ManyToOne(() => Client, (client) => client.transactions)
  client: Client;

  @ManyToOne(() => ClientInstrument, (instrument) => instrument.transactions)
  instrument: ClientInstrument;

  @ManyToOne(() => Account, (account) => account.transactions, { nullable: true })
  account_reception: Account;

  @ManyToOne(() => WalletUser, { nullable: true })
  wallet_reception: WalletUser;

  @ManyToOne(() => Rate)
  rate: Rate;

  @ManyToOne(() => Country)
  origin: Country;

  @Column("decimal", { precision: 15, scale: 3 })
  amountSend: number;

  @Column()
  currencySend: string;

  @ManyToOne(() => Country)
  destination: Country;

  @Column("decimal", { precision: 15, scale: 3 })
  amountReceived: number;

  @Column()
  currencyReceived: string;

  @Column()
  proofOfSend: string;

  @Column({ type: "date" })
  dateProofOfSend: Date;

  @Column()
  numberProofOfSend: string;

  // estos se llenan al momento del envio
  @Column({ nullable: true })
  proofOfReceived: string;

  @Column({ type: "date", nullable: true })
  dateProofOfReceived: Date;

  @Column({ nullable: true })
  numberProofOfReceived: string;

  // estos se actualizan en caso de que haya penalizadion
  @Column("decimal", { precision: 15, scale: 3, nullable: true })
  penaltyFee: number;

  @Column({ nullable: true })
  currencyPenaltyFee: string;

  @Column({ nullable: true })
  descriptionPenaltyFee: string;

  // estas con las comisiones jalador, dueño de cuenta, casa
  @Column("decimal", { precision: 15, scale: 3 })
  puller_profit: number;

  @Column("decimal", { precision: 15, scale: 3 })
  account_profit: number;

  @Column("decimal", { precision: 15, scale: 3 })
  home_profit: number;

  @Column({ default: "CREADA" })
  status: string;

  @Column({ type: "text", nullable: true })
  observation: string;

  @ManyToOne(() => Expedient, { nullable: true })
  expedient: Expedient;

  @ManyToOne(() => User, (user) => user.transactions)
  creator: User;

  @ManyToOne(() => User, (user) => user.transactions, { nullable: true })
  owner: User;

  @ManyToOne(() => WalletUser, { nullable: true })
  senderWallet: WalletUser;

  @OneToMany(() => TransactionsTracking, (tracking) => tracking.transaction)
  trackings: TransactionsTracking[];

  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt: Date;
}
