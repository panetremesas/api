import { Module } from '@nestjs/common';
import { TransactionsService } from './transactions.service';
import { TransactionsController } from './transactions.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Transaction } from './entities/transaction.entity';
import { Rate } from 'src/rates/entities/rate.entity';
import { TransactionsTracking } from 'src/transactions-tracking/entities/transactions-tracking.entity';
import { User } from 'src/users/entities/user.entity';
import { Account } from 'src/accounts/entities/account.entity';
import { WalletUser } from 'src/wallet-users/entities/wallet-user.entity';
import { Expedient } from 'src/expedient/entities/expedient.entity';
import { DocumentsExpedient } from 'src/documents-expedient/entities/documents-expedient.entity';
import { TransWall } from 'src/trans-wall/entities/trans-wall.entity';
import { Queue } from 'src/queues/entities/queue.entity';
import { Notification } from 'src/notifications/entities/notification.entity';
import { NotificationsModule } from 'src/notifications/notifications.module';
import { Client } from 'src/clients/entities/client.entity';
import { Bitacora } from 'src/bitacora/entities/bitacora.entity';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname, join } from 'path';
const uploadDir = join(process.cwd(), 'uploads');


@Module({
  imports: [
    MulterModule.register({
      storage: diskStorage({
        destination: (req, file, cb) => {
          cb(null, uploadDir);
        },
        filename: (req, file, cb) => {
          const ext = extname(file.originalname);
          const filename = `${Date.now()}${ext}`;
          cb(null, filename);
        },
      }),
      fileFilter: (req, file, cb) => {
        if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
          cb(null, true);
        } else {
          cb(new Error('Only images are allowed...'), false);
        }
      },
    }),
    TypeOrmModule.forFeature([Transaction, Rate, TransactionsTracking, Account, User, WalletUser, Expedient, Client, DocumentsExpedient, TransWall, Queue, Notification, Bitacora, NotificationsModule])],
  controllers: [TransactionsController],
  providers: [TransactionsService],
})

export class TransactionsModule { }
