import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
  Query,
  Request,
  UseGuards,
} from "@nestjs/common";
import { TransactionsService } from "./transactions.service";
import { CreateTransactionDto } from "./dto/create-transaction.dto";
import { UpdateTransactionDto } from "./dto/update-transaction.dto";
import { FileInterceptor } from "@nestjs/platform-express";
import { AuthGuard } from "src/auth/auth.guard";

@UseGuards(AuthGuard)
@Controller("transactions")
export class TransactionsController {
  constructor(private readonly transactionsService: TransactionsService) { }

  @Post()
  @UseInterceptors(FileInterceptor("image"))
  create(
    @Body() createTransactionDto: CreateTransactionDto,
    @UploadedFile() file
  ) {
    return "archivo subido con exito"
    return this.transactionsService.create(createTransactionDto, file);
  }

  @Get()
  findAll(
    @Query("status") status,
    @Query("owner") owner,
    @Query("creator") creator,
    @Query("date") date,
    @Request() req
  ) {
    return this.transactionsService.findAll(
      status,
      owner,
      creator,
      date,
      req.user
    );
  }

  @Get('reporte')
  reporte(
    @Query("origin") origin,
    @Query("destination") destination,
    @Query("creator") creator,
    @Query("client") client,
    @Query("amount") amount,
    @Query("date") date,
    @Query("numberProofOfSend") numberProofOfSend,
    @Request() req
  ) {
    return this.transactionsService.reporte(
      origin,
      destination,
      creator,
      client,
      amount,
      date,
      numberProofOfSend,
      req.user
    );
  }

  @Get(":id")
  findOne(@Param("id") id: string) {
    return this.transactionsService.findOne(+id);
  }

  @Get("aprove/free/:id")
  aproveFree(@Param("id") id, @Request() req) {
    return this.transactionsService.aprovefree(id, req.user);
  }

  @Patch(":id")
  update(
    @Param("id") id: string,
    @Body() updateTransactionDto: UpdateTransactionDto
  ) {
    return this.transactionsService.update(+id, updateTransactionDto);
  }

  @Delete(":id")
  remove(@Param("id") id: string) {
    return this.transactionsService.remove(+id);
  }

  @Post("penalizar")
  penalizar(@Body("") data) {
    return this.transactionsService.penalizar(data)
  }

  @Get("report/ganancias/intermediarios")
  getReportIntermediarios(@Query("creador") creador, @Query("inicio") inicio, @Query("fin") fin, @Query("pais") pais) {
    return this.transactionsService.reportUser(creador, inicio, fin, pais)
  }
}
