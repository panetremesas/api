import { IsDate, IsNotEmpty, IsNumber, IsNumberString, IsString, IsUrl } from "class-validator";
import { User } from "src/users/entities/user.entity";
import { Client } from '../../clients/entities/client.entity';
import { ClientInstrument } from "src/client-instruments/entities/client-instrument.entity";
import { Rate } from "src/rates/entities/rate.entity";
import { Account } from "src/accounts/entities/account.entity";

export class CreateTransactionDto {
    @IsNumberString()
    creator: User

    @IsNumberString()
    client: Client

    @IsNumberString()
    instrument: ClientInstrument

    @IsNumberString()
    rate: Rate

    @IsNumber()
    amountSend: number

    @IsNotEmpty()
    amountReceived: string

    @IsDate()
    dateProofOfSend: Date

    @IsNotEmpty()
    numberProofOfSend: string

    @IsNumberString()
    account: Account

    @IsNumber()
    idJalador: number

    @IsNumber()
    idDestino: number
}
