import { ConsoleLogger, Injectable } from "@nestjs/common";
import { CreateTransactionDto } from "./dto/create-transaction.dto";
import { UpdateTransactionDto } from "./dto/update-transaction.dto";
import { v2 as cloudinary } from "cloudinary";
import { InjectRepository } from "@nestjs/typeorm";
import { Transaction } from "./entities/transaction.entity";
import { OrderByCondition, Repository } from "typeorm";
import { Rate } from "src/rates/entities/rate.entity";
import { TransactionsTracking } from "src/transactions-tracking/entities/transactions-tracking.entity";
import { ResponseUtil } from "src/utils/response.util";
import { User } from "../users/entities/user.entity";
import { Account } from "src/accounts/entities/account.entity";
import { WalletUser } from "src/wallet-users/entities/wallet-user.entity";
import { PushNotificationUtil } from "src/utils/push.utils";
import { Canvas, createCanvas, loadImage } from "canvas";
import * as fs from "fs";
import * as path from "path";
import axios from "axios";
import {
  STATUS_NOTIFICATION,
  STATUS_QUEUE,
  STATUS_REFILL,
  TYPE_EXPEDIENT,
  TYPE_PRESUPUESTO,
  TYPE_WALLET,
} from "src/enums/type_account.enum";
import { createClient } from '@supabase/supabase-js';
import { Expedient } from "src/expedient/entities/expedient.entity";
import { DocumentsExpedient } from "src/documents-expedient/entities/documents-expedient.entity";
import { TransWall } from "src/trans-wall/entities/trans-wall.entity";
import { Queue } from "src/queues/entities/queue.entity";
import { Notification } from "src/notifications/entities/notification.entity";
import { Client } from "src/clients/entities/client.entity";
import { Bitacora } from "src/bitacora/entities/bitacora.entity";
import ImageKit from "imagekit";
import { TYPE_DESPACHO_ACCOUNT, TYPES_QUEUES } from "src/enums/types-queues.enum";

@Injectable()
export class TransactionsService {
  private supabase;
  private imageKit;

  constructor(
    @InjectRepository(Transaction)
    private readonly transactionRepository: Repository<Transaction>,
    @InjectRepository(Rate)
    private readonly rateRepository: Repository<Rate>,
    @InjectRepository(TransactionsTracking)
    private readonly trackingTransactionsRepository: Repository<TransactionsTracking>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(Account)
    private readonly accountRepository: Repository<Account>,
    @InjectRepository(WalletUser)
    private readonly walletUserRepository: Repository<WalletUser>,
    @InjectRepository(Expedient)
    private readonly expedientRepository: Repository<Expedient>,
    @InjectRepository(DocumentsExpedient)
    private readonly documentsExpedientRepository: Repository<DocumentsExpedient>,
    @InjectRepository(TransWall)
    private readonly transWallRepository: Repository<TransWall>,
    @InjectRepository(Queue)
    private readonly queueRepository: Repository<Queue>,
    @InjectRepository(Notification)
    private readonly notificationRepository: Repository<Notification>,
    @InjectRepository(Client)
    private readonly clientRepository: Repository<Client>,
    @InjectRepository(Bitacora)
    private readonly bitacoraRepository: Repository<Bitacora>,
  ) {

    cloudinary.config({
      cloud_name: "dtxr4opxp",
      api_key: "368541571316882",
      api_secret: "els_JKHH3Hf-Y4z_zjsRS-Rgc40",
    });
    this.supabase = createClient('https://xxbfqggjfjcbmxtlobrh.supabase.co', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Inh4YmZxZ2dqZmpjYm14dGxvYnJoIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTY1NjY1NTEsImV4cCI6MjAzMjE0MjU1MX0.M4Y0rMHaPb-ynXYM_VFPyVDBTJ4GNctgwE4Nv-taOgU');
  }

  async create(createTransactionDto, file) {
    try {
      const rate = await this.rateRepository.findOne({
        where: { id: createTransactionDto.rate },
        relations: ["origin", "destination"],
      });

      if (!rate) {
        return ResponseUtil.error("No se encontró la tasa especificada.");
      }

      if (rate.destination.status === false) {
        return ResponseUtil.error(
          "Actualmente no tenemos tasa activa para transacciones hacia " +
          rate.destination.name
        );
      }

      const imageUrl = process.env.URL_IMAGENES + file;

      const lastTransaction = await this.transactionRepository
        .createQueryBuilder("transaction")
        .orderBy("transaction.id", "DESC")
        .getOne();

      let nextId = 1;
      if (lastTransaction) {
        nextId = lastTransaction.id + 1;
      }
      let number = Math.floor(Math.random() * 9000000) + 1000000;
      const referencia = "PT-" + number + "-" + nextId;

      const type_profit = rate.type_profit;

      const jalador = await this.userRepository.findOneBy({
        id: createTransactionDto.creator,
      });

      if (!jalador) {
        return ResponseUtil.error("No se encontró el usuario jalador.");
      }

      let profit_puller = 0;
      if (jalador.profit_commission > 0) {
        profit_puller = this.calcularValorPorcentaje(
          createTransactionDto.amountSend,
          jalador.profit_commission
        );
      }

      const account = await this.accountRepository.findOne({
        where: { id: createTransactionDto.account },
        relations: ["user"],
      });

      if (!account) {
        return ResponseUtil.error("No se encontró la cuenta especificada.");
      }

      let profit_account = 0;
      if (account.commision === true) {
        profit_account = this.calcularValorPorcentaje(
          createTransactionDto.amountSend,
          account.profit_commission
        );
      }

      let comision =
        +createTransactionDto.amountSend * rate.origin[type_profit] -
        +createTransactionDto.amountSend;

      let comisionHome = +comision - +profit_account - +profit_puller;

      let vWalletOrigin = null;
      vWalletOrigin = await this.walletUserRepository
        .createQueryBuilder("wallet")
        .where(
          "userId = :user AND countrieId = :countrie AND type_wallet = :type_wallet",
          {
            user: account.user.id,
            countrie: rate.origin.id,
            type_wallet: "USER",
          }
        )
        .getOne();

      if (vWalletOrigin === null) {
        vWalletOrigin = await this.walletUserRepository.save({
          countrie: rate.origin,
          user: account.user,
          available: 0,
          type_wallet: TYPE_WALLET.USER,
        });
      }

      const save = await this.transactionRepository.save({
        amountSend: createTransactionDto.amountSend,
        currencySend: rate.origin.currency,
        amountReceived: createTransactionDto.amountReceived,
        currencyReceived: rate.destination.currency,
        dateProofOfSend: createTransactionDto.dateProofOfSend,
        numberProofOfSend: createTransactionDto.numberProofOfSend,
        client: createTransactionDto.client,
        instrument: createTransactionDto.instrument,
        rate: rate,
        creator: createTransactionDto.creator,
        proofOfSend: imageUrl,
        reference: referencia,
        puller_profit: profit_puller,
        account_profit: profit_account,
        home_profit: comisionHome,
        account: account,
        owner: account.user,
        origin: rate.origin,
        destination: rate.destination,
        wallet_reception: vWalletOrigin,
        account_reception: account,
      });

      await this.trackingTransactionsRepository.save({
        status: "CREADA",
        fecha: new Date(),
        transaction: save,
      });

      let vClient = await this.transactionRepository.findOne({
        where: { id: save.id },
        relations: ["client", "destination"],
      });

      if (!vClient) {
        return ResponseUtil.error("No se pudo encontrar la transacción recién creada.");
      }

      let numero = vClient.client.phone;
      let encodedMessage =
        "Hola, estimado " +
        vClient.client.full_name +
        " te notificamos que tu transaccion por el monto de " +
        vClient.amountSend +
        " " +
        vClient.currencySend +
        ", con destino a " +
        vClient.destination.name +
        " por " +
        vClient.amountReceived +
        " " +
        vClient.currencyReceived +
        " ha sido generada con exito en nuestro sistema, en los proximos minutos te enviaremos el comprobante de procesado. \n Su Numero de Transaccion es: " +
        vClient.reference;

      let payload = {
        message: encodedMessage,
        image: null,
        phone: numero,
      };

      await this.notificationRepository.save(payload);

      let encodedMessage2 =
        "Referencia: " +
        save.reference +
        "\n" +
        "Numero de Comprobante: " +
        save.numberProofOfSend +
        "Fecha y Hora: " +
        save.createdAt;

      let payload3 = {
        message: encodedMessage2,
        image: imageUrl,
        phone: account.user.phone,
      };

      await this.notificationRepository.save(payload3);

      const { data, error } = await this.supabase
        .from('transactions')
        .insert([
          { transactionId: save.id },
        ])
        .select();

      if (error) {
        return ResponseUtil.error("Error al insertar en Supabase: " + error.message);
      }

      await axios.get(process.env.URL_QUEUE_NOTIFICATIONS);

      await this.queueRepository.save({
        transaction: save,
        dispatcher: save.owner,
        type: TYPES_QUEUES.CUENTA,
        type_despacho: TYPE_DESPACHO_ACCOUNT.TRANSACCION,
      });

      await this.bitacoraRepository.save({
        user: createTransactionDto.creator,
        accion: 'Creacion de Transaccion: ' + save.id,
      });

      return ResponseUtil.success(save.id, "Transaccion Generada con exito");
    } catch (error) {
      console.error("Error en la creación de la transacción:", error);
      return ResponseUtil.error("Error interno del servidor: " + error.message);
    }
  }

  async createfree(createTransactionDto) {
    const rate = await this.rateRepository.findOne({
      where: { id: createTransactionDto.rate },
      relations: ["origin", "destination"],
    });

    if (rate.destination.status === false) {
      return ResponseUtil.error(
        "Actualmente no tenemos tasa activa para transacciones hacia " +
        rate.destination.name
      );
    }

    const imageUrl = 'NO PROCEDE'
    // consulta la tsas

    // se genera la referencia
    const lastTransaction = await this.transactionRepository
      .createQueryBuilder("transaction")
      .orderBy("transaction.id", "DESC")
      .getOne();

    let nextId = 1;

    if (lastTransaction) {
      nextId = lastTransaction.id + 1;
    }

    let number = Math.floor(Math.random() * 9000000) + 1000000;
    const referencia = "PT-" + number + "-" + nextId;

    const save = await this.transactionRepository.save({
      amountSend: 0, // si
      currencySend: rate.origin.currency,
      amountReceived: createTransactionDto.amountSend,
      currencyReceived: rate.destination.currency,
      dateProofOfSend: new Date(),
      numberProofOfSend: createTransactionDto.numberProofOfSend,
      client: createTransactionDto.client,
      instrument: createTransactionDto.instrument,
      rate: rate,
      creator: createTransactionDto.creator,
      proofOfSend: imageUrl,
      reference: referencia, // si
      puller_profit: 0,
      account_profit: 0,
      home_profit: 0,
      account: null,
      owner: createTransactionDto.creator,
      origin: rate.origin,
      destination: rate.destination,
      wallet_reception: null,
      account_reception: null,
      observation: createTransactionDto.concepto
    });

    await this.trackingTransactionsRepository.save({
      status: "CREADA",
      fecha: new Date(),
      transaction: save,
    });

    let vClient = await this.transactionRepository.findOne({
      where: { id: save.id },
      relations: ["client", "destination"],
    });

    let numero = vClient.client.phone;
    let encodedMessage =
      "Hola, estimado " +
      vClient.client.full_name +
      " te notificamos que tu transaccion por el monto de " +
      vClient.amountReceived + ' ' +
      vClient.currencyReceived +
      " ha sido generada con exito en nuestro sistema, en los proximos minutos te enviaremos el comprobante de procesado. \n Su Numero de Transaccion es: " +
      vClient.reference

    let payload = {
      message: encodedMessage,
      image: null,
      phone: numero
    }

    this.notificationRepository.save(payload)

    let numero2 = "584148383419"
    let encodedMessage2 = `*Notificacion de Transaccion Free:* \n Referencia ${save.reference} \n Cliente: ${vClient.client.full_name} \n Monto: ${save.amountReceived} ${vClient.currencyReceived} \n Origen:  ${save.origin.name} \n Destino: ${save.destination.name} \n Razon:${save.observation}`

    payload = {
      message: encodedMessage2,
      image: null,
      phone: numero2
    }

    this.notificationRepository.save(payload)

    const { data, error } = await this.supabase
      .from('transactions')
      .insert([
        { transactionId: save.id },
      ])
      .select()

    await axios.get(process.env.URL_QUEUE_NOTIFICATIONS)
    await this.bitacoraRepository.save({
      user: createTransactionDto.creator,
      accion: 'Creacion de Transaccion: ' + save.id
    })
    return ResponseUtil.success(save.id, "Transaccion Generada con exito");
  }

  async reporte(origin, destination, creator, client, amount, date, numberProofOfSend, user) {
    let transacions =
      this.transactionRepository.createQueryBuilder("transaccion")
        .leftJoinAndSelect("transaccion.origin", "origin")
        .leftJoinAndSelect("transaccion.destination", "destination")
        .leftJoinAndSelect("transaccion.client", "client")

    if (origin) {
      transacions = transacions.andWhere("transaccion.originId = :origin", { origin })
    }

    if (destination) {
      transacions = transacions.andWhere("transaccion.destinationId = :destination", { destination })
    }

    if (creator) {
      transacions = transacions.andWhere("transaccion.creatorId = :creator", { creator })
    }

    if (numberProofOfSend) {
      transacions = transacions.andWhere("transaccion.numberProofOfSend = :numberProofOfSend", { numberProofOfSend })
    }

    if (client) {
      console.log(client)
      let data = await this.clientRepository.findOne({ where: { document: client } })
      console.log(data)
      transacions = transacions.andWhere("transaccion.clientId = :client", { client: data.id })
    }

    if (amount) {
      transacions = transacions.andWhere("transaccion.amountSend = :amount", { amount })
    }

    if (date) {
      transacions = transacions.andWhere(
        "DATE(transaccion.createdAt) = :date",
        { date }
      );
    }

    transacions = transacions.orderBy("transaccion.id", "DESC").take(300);
    const result = await transacions.getMany();
    return ResponseUtil.success(result, "Transacciones obtenidas con exito");
  }

  async findAll(status, owner, creator, date, user) {
    let transacions =
      this.transactionRepository.createQueryBuilder("transaccion")
        .leftJoinAndSelect("transaccion.origin", "origin")

    if (user.typeUser === "PADMIN") {
      transacions = transacions.andWhere(
        "transaccion.sucursalId = :sucursalId",
        { sucursalId: user.sucursalId }
      );
    }

    if (user.typeUser === "POPERARIO") {
      transacions = transacions.andWhere(
        "(transaccion.creatorId = :creatorId OR transaccion.ownerId = :ownerId)",
        {
          creatorId: user.id,
          ownerId: user.id,
        }
      );
    }

    if (status) {
      transacions = transacions.andWhere("transaccion.status = :status", {
        status,
      });
    }

    if (owner) {
      transacions = transacions.andWhere("transaccion.ownerId = :owner", {
        owner,
      });
    }

    if (creator) {
      transacions = transacions.andWhere("transaccion.creatorId = :creator", {
        creator,
      });
    }

    if (date) {
      transacions = transacions.andWhere(
        "DATE(transaccion.createdAt) = :date",
        { date }
      );
    }

    transacions = transacions.orderBy("transaccion.id", "DESC").take(300);
    const result = await transacions.getMany();
    return ResponseUtil.success(result, "Transacciones obtenidas con exito");
  }

  async findOne(id: number) {
    const transaction = await this.transactionRepository.findOne({
      where: { id },
      relations: [
        "trackings",
        "instrument",
        "expedient",
        "creator",
        "account_reception",
        "origin",
        "destination",
        "client",
        "owner",
        "senderWallet",
        "senderWallet.user",
      ],
    });

    return ResponseUtil.success(transaction, "Transaccion obtenida con exito");
  }

  async findComprobante(numero: number, fecha: Date) {
    const transaction = await this.transactionRepository
      .createQueryBuilder("transaction")
      .where("transaction.numberProofOfSend = :numberProofOfSend", {
        numberProofOfSend: numero,
      })
      .getOne();

    if (transaction != null) {
      return ResponseUtil.error("Comprobante ya fue enviado Previamente");
    }

    return ResponseUtil.success(
      null,
      "No Existe un comprobante con esos datos"
    );
  }

  update(id: number, updateTransactionDto: UpdateTransactionDto) {
    return `This action updates a #${id} transaction`;
  }

  remove(id: number) {
    return `This action removes a #${id} transaction`;
  }

  async aprovefree(id, user) {

    const transaciont = await this.transactionRepository
      .createQueryBuilder("transaction")
      .leftJoinAndSelect("transaction.owner", "owner")
      .leftJoinAndSelect("transaction.creator", "creator")
      .leftJoinAndSelect("transaction.wallet_reception", "wallet_reception")
      .leftJoinAndSelect("transaction.origin", "origin")
      .leftJoinAndSelect("transaction.destination", "destination")
      .where("transaction.id = :id", { id })
      .getOne();

    const usersWithFilteredCounts = await this.userRepository
      .createQueryBuilder("user")
      .leftJoinAndSelect("user.cola", "cola", "cola.status = :status", {
        status: "CREADA",
      })
      .where("user.dispatcher = :dispatcher", { dispatcher: true })
      .andWhere((qb) => {
        const subQuery = qb
          .subQuery()
          .select("wallet_user.userId")
          .from("wallet_user", "wallet_user")
          .where("wallet_user.type_wallet = :type_wallet", {
            type_wallet: TYPE_WALLET.USER,
          })
          .andWhere("wallet_user.countrieId = :countrieId", {
            countrieId: transaciont.destination.id,
          })
          .andWhere("wallet_user.status = 1")
          .getQuery();
        return "user.id IN " + subQuery;
      })
      .select(["user.id", "user.name", "COUNT(cola.id) AS colaCount"])
      .groupBy("user.id")
      .orderBy("colaCount", "ASC")
      .limit(1)
      .getRawOne();

    if (!usersWithFilteredCounts) {
      return ResponseUtil.error(
        "No existe despachador disponible para el pais destino o con saldo, contacta con administracion"
      );
    }

    const queue = await this.queueRepository.save({
      transaction: transaciont,
      dispatcher: usersWithFilteredCounts.user_id,
    });

    let distpatcher = await this.userRepository.findOne({ where: { id: usersWithFilteredCounts.user_id } })

    let payload3 = {
      message: 'Tienes una transaccion por aprobar en tu app Despachador',
      image: null,
      phone: distpatcher.phone
    }

    this.notificationRepository.save(payload3)

    await this.transactionRepository.update(id, {
      status: "ACEPTADA",
    });

    await this.bitacoraRepository.save({
      user: user.id,
      accion: 'Aprobacion de Transaccion: ' + transaciont.id
    })

    return ResponseUtil.success(
      transaciont,
      "Su transaccion ha sido aceptada con exito"
    );
  }

  async aprove(id, user) {
    // se consulta la transaccion
    const transaciont = await this.transactionRepository
      .createQueryBuilder("transaction")
      .leftJoinAndSelect("transaction.owner", "owner")
      .leftJoinAndSelect("transaction.creator", "creator")
      .leftJoinAndSelect("transaction.wallet_reception", "wallet_reception")
      .leftJoinAndSelect("transaction.origin", "origin")
      .leftJoinAndSelect("transaction.destination", "destination")
      .where("transaction.id = :id", { id })
      .getOne();

    // consultar y validar que la transaccion no este en cola de ejecucion
    const vQueue = await this.queueRepository
      .createQueryBuilder("cola")
      .where("cola.transactionId = :transactionId", { transactionId: id })
      .andWhere("cola.type = :type", { type: TYPES_QUEUES.DESPACHO })
      .getOne();

    if (vQueue !== null) {
      await this.transactionRepository.update(id, {
        status: "ACEPTADA",
      });

      return ResponseUtil.error(
        "Esta Transaccion fue aprobada previamente, se encuentra en etapa de ejecucion, actualice y revise su estado."
      );
    }

    const vWalletReption = await this.walletUserRepository
      .createQueryBuilder("wallet")
      .where(
        "userId = :user AND countrieId = :countrie AND type_wallet = :type_wallet",
        {
          user: transaciont.owner.id,
          countrie: transaciont.origin.id,
          type_wallet: "USER",
        }
      )
      .getOne();

    if (vWalletReption === null) {
      return ResponseUtil.error("No existe el wallet de envio");
    }

    // genra el calculo descontando la comision del dueño de cuenta
    const amountSum = +transaciont.amountSend - +transaciont.account_profit;
    const newAmount = +amountSum + +vWalletReption.available;

    await this.transWallRepository.save({
      concept: "INGRESO POR TRANSACCION " + transaciont.reference,
      old_amount: vWalletReption.available,
      new_amount: newAmount,
      amount_transaction: amountSum,
      type: TYPE_PRESUPUESTO.INGRESOT,
      wallet: transaciont.wallet_reception,
    });

    await this.walletUserRepository.update(vWalletReption.id, {
      available: newAmount,
    });

    // sumar el monto de comision de ganancia al dueño de ceunta
    const vWalletProfit = await this.walletUserRepository
      .createQueryBuilder("wallet")
      .where(
        "wallet.userId = :userId AND wallet.countrieId = :countrieId AND wallet.type_wallet = :type_wallet",
        {
          userId: transaciont.owner.id,
          countrieId: transaciont.origin.id,
          type_wallet: "PROFITS",
        }
      )
      .getOne();

    // if (transaciont.account_profit > 0) {
    //   let amouret = newAmount - transaciont.account_profit
    //   await this.walletUserRepository.update(vWalletReption.id, {
    //     available: amouret,
    //   });

    //   await this.transWallRepository.save({
    //     concept: "EGRESO POR PAGO DE COMISION A CUENTA",
    //     old_amount: newAmount,
    //     new_amount: amouret,
    //     amount_transaction: transaciont.account_profit,
    //     type: TYPE_PRESUPUESTO.EGRESOT,
    //     wallet: vWalletReption
    //   });
    // }

    await this.trackingTransactionsRepository.save({
      status: "ACEPTADA",
      fecha: new Date(),
      transaction: transaciont,
    });

    // asignar despachador
    const usersWithFilteredCounts = await this.userRepository
      .createQueryBuilder("user")
      .leftJoinAndSelect("user.cola", "cola", "cola.status = :status", {
        status: "CREADA",
      })
      .where("user.dispatcher = :dispatcher", { dispatcher: true })
      .andWhere((qb) => {
        const subQuery = qb
          .subQuery()
          .select("wallet_user.userId")
          .from("wallet_user", "wallet_user")
          .where("wallet_user.type_wallet = :type_wallet", {
            type_wallet: TYPE_WALLET.USER,
          })
          .andWhere("wallet_user.countrieId = :countrieId", {
            countrieId: transaciont.destination.id,
          })
          .andWhere("wallet_user.status = 1")
          .getQuery();
        return "user.id IN " + subQuery;
      })
      .select(["user.id", "user.name", "COUNT(cola.id) AS colaCount"])
      .groupBy("user.id")
      .orderBy("colaCount", "ASC")
      .limit(1)
      .getRawOne();

    if (!usersWithFilteredCounts) {
      return ResponseUtil.error(
        "No existe despachador disponible para el pais destino o con saldo, contacta con administracion"
      );
    }

    const queue = await this.queueRepository.save({
      transaction: transaciont,
      dispatcher: usersWithFilteredCounts.user_id,
    });

    await this.queueRepository.update({ transaction: transaciont, type: TYPES_QUEUES.CUENTA }, { status: STATUS_QUEUE.COMPLEATADA });

    let distpatcher = await this.userRepository.findOne({ where: { id: usersWithFilteredCounts.user_id } })

    let payload3 = {
      message: 'Tienes una transaccion por aprobar en tu app Despachador',
      image: null,
      phone: distpatcher.phone
    }

    this.notificationRepository.save(payload3)

    await this.transactionRepository.update(id, {
      status: "ACEPTADA",
    });

    await this.bitacoraRepository.save({
      user: user.id,
      accion: 'Aprobacion de Transaccion: ' + transaciont.id
    })

    return ResponseUtil.success(
      transaciont,
      "Su transaccion ha sido aceptada con exito"
    );
  }

  async observer(data) {
    // se genera el expdiente
    const exp = {
      type_expedient: TYPE_EXPEDIENT.RECARGA,
      id_trans: data.transactionId,
    };
    const expedient = await this.expedientRepository.save(exp);

    const documents = [];
    // agrega los documentos al expediente
    data.documents.map(async (document) => {
      if (document.status) {
        const doc = {
          expedient: expedient,
          document: document.id,
        };
        await this.documentsExpedientRepository.save(doc);
      }
    });

    await this.transactionRepository.update(data.transactionId, {
      expedient: expedient,
      observation: data.observation,
      status: STATUS_REFILL.OBSERVADA,
    });

    const vUpdate = await this.transactionRepository.findOne({
      where: { id: data.refillId },
      relations: ["owner", "client", "destination", "origin", "creator"],
    });

    let numero = vUpdate.client.phone;
    let encodedMessage =
      "Hola, estimado " +
      vUpdate.client.full_name +
      " te notificamos que tu transaccion por el monto de " +
      vUpdate.amountSend +
      " " +
      vUpdate.currencySend +
      ", con destino a " +
      vUpdate.destination.name +
      " por " +
      vUpdate.amountReceived +
      " " +
      vUpdate.origin.currency +
      " ha sido observada: " +
      data.observation +
      "ponte en contact con administracion +51916902417 Nro Transaccion: " +
      vUpdate.reference

    let payload = {
      message: encodedMessage,
      image: null,
      phone: numero
    }

    this.notificationRepository.save(payload)

    await this.queueRepository.delete({ transaction: data.transactionId, type: TYPES_QUEUES.CUENTA })

    await axios.get(process.env.URL_QUEUE_NOTIFICATIONS)

    await this.bitacoraRepository.save({
      user: vUpdate.creator,
      accion: 'Observacion de Transaccion: ' + vUpdate.id
    })

    return ResponseUtil.success(vUpdate, "Transaccion observada");
  }

  async desaprove(data) {
    await this.transactionRepository.update(data.transactionId, {
      status: "RECHAZADA",
      observation: data.observation,
    });

    const vUpdate = await this.transactionRepository.findOne({
      where: { id: data.transactionId },
      relations: ["owner", "client", "destination", "creator"],
    });

    let numero = vUpdate.client.phone;
    let encodedMessage =
      "Hola, estimado " +
      vUpdate.client.full_name +
      " te notificamos que tu transaccion por el monto de " +
      vUpdate.amountSend +
      " " +
      vUpdate.currencySend +
      ", con destino a " +
      vUpdate.destination.name +
      " por " +
      vUpdate.amountReceived +
      " " +
      vUpdate.currencyReceived +
      " ha sido rechazada por el siguiente motivo: " +
      data.observation
      ;

    let payload = {
      message: encodedMessage,
      image: null,
      phone: numero
    }

    this.notificationRepository.save(payload)

    await this.trackingTransactionsRepository.save({
      status: "RECHAZADA",
      fecha: new Date(),
      transaction: vUpdate,
    });

    await axios.get(process.env.URL_QUEUE_NOTIFICATIONS)

    await this.bitacoraRepository.save({
      user: vUpdate.creator,
      accion: 'Rechazo de Transaccion: ' + vUpdate.id
    })

    await this.queueRepository.update({ transaction: vUpdate, type: TYPES_QUEUES.CUENTA }, { status: STATUS_QUEUE.COMPLEATADA });

    return ResponseUtil.success(
      vUpdate,
      "Se ha RECHAZADO correctamente la transaccion"
    );
  }

  async complete(data, file, user) {
    // subir comprobante
    let imageUrl = null;
    imageUrl = process.env.URL_IMAGENES + file

    const vTransaccion = await this.transactionRepository.findOne({
      where: { id: data.idTransaction },
      relations: ["origin", "creator", "destination", "client", "instrument"],
    });

    const vWalletSender = await this.walletUserRepository
      .createQueryBuilder("transaction")
      .where(
        "userId = :user AND countrieId = :countrie AND type_wallet = :type_wallet",
        {
          user: user.id,
          countrie: vTransaccion.destination.id,
          type_wallet: TYPE_WALLET.USER,
        }
      )
      .getOne();

    if (vWalletSender === null) {
      return ResponseUtil.error(
        "Usted no tiene wallet en este pais, proceda a transferir la transaccion a quien le indique administracion."
      );
    }


    await this.transactionRepository.update(vTransaccion.id, {
      senderWallet: vWalletSender,
      status: "COMPLETADA",
      numberProofOfReceived: data.numero,
      proofOfReceived: imageUrl,
    });

    // retirar saldo del despachador
    let newAmountDistpacher =
      +vWalletSender.available - +vTransaccion.amountReceived;

    await this.walletUserRepository.update(vWalletSender.id, {
      available: newAmountDistpacher,
    });

    await this.transWallRepository.save({
      concept: "EGRESO POR TRANSACCION" + vTransaccion.reference,
      old_amount: +vWalletSender.available,
      new_amount: newAmountDistpacher,
      amount_transaction: +vTransaccion.amountReceived,
      type: TYPE_PRESUPUESTO.EGRESO,
      wallet: vWalletSender,
    });

    if (vTransaccion.puller_profit) {
      const vJalador = await this.walletUserRepository
        .createQueryBuilder("wallet")
        .where(
          "userId = :user AND countrieId = :countrie AND type_wallet = :type_wallet",
          {
            user: vTransaccion.creator.id,
            countrie: vTransaccion.origin.id,
            type_wallet: TYPE_WALLET.PROFITS,
          }
        )
        .getOne();

      // sumar la comision al jalador
      if (vJalador === null) {
        // crear el wallet
        const newWallet = await this.walletUserRepository.save({
          countrie: vTransaccion.origin,
          user: vTransaccion.creator,
          available: +vTransaccion.puller_profit,
          type_wallet: TYPE_WALLET.PROFITS,
        });

        await this.transWallRepository.save({
          concept:
            "INGRESO POR GANANCIA DE COMISION DE TRANSACCION " +
            vTransaccion.reference,
          old_amount: 0,
          new_amount: +vTransaccion.puller_profit,
          amount_transaction: +vTransaccion.puller_profit,
          type: TYPE_PRESUPUESTO.INGRESO,
          wallet: newWallet,
        });
      } else {
        let old_amount = vJalador.available;
        let new_amount = +vJalador.available + +vTransaccion.puller_profit;
        await this.walletUserRepository.update(vJalador.id, {
          available: new_amount,
        });
        await this.transWallRepository.save({
          concept:
            "INGRESO POR GANANCIA DE COMISION DE TRANSACCION " +
            vTransaccion.reference,
          old_amount: old_amount,
          new_amount: new_amount,
          amount_transaction: +vTransaccion.puller_profit,
          type: TYPE_PRESUPUESTO.INGRESO,
          wallet: vJalador,
        });
      }

      // PushNotificationUtil.sendPush(
      //   vTransaccion.creator.token_puler,
      //   "La transaccion " +
      //   vTransaccion.reference +
      //   " ha sido completada con exito, ya puedes rvisar tu comprobante en la app"
      // );

      await this.queueRepository.update(data.idOperation, {
        status: STATUS_QUEUE.COMPLEATADA,
      });

      const tracking = await this.trackingTransactionsRepository.save({
        status: "COMPLETADA",
        fecha: new Date(),
        transaction: vTransaccion,
      });

      let numero = vTransaccion.client.phone;
      let encodedMessage = "Su Transaccion " +
        vTransaccion.reference +
        " ha sido realizada con exito. te Adjuntamos tu comprobante de pago."


      let payload = {
        message: encodedMessage,
        image: imageUrl,
        phone: numero
      }

      this.notificationRepository.save(payload)

      if (vTransaccion.instrument.type_method == "PagoMovil" && vTransaccion.destination.id == 1) {
        const vWalletSender2 = await this.walletUserRepository
          .createQueryBuilder("transaction")
          .where(
            "userId = :user AND countrieId = :countrie AND type_wallet = :type_wallet",
            {
              user: user.id,
              countrie: vTransaccion.destination.id,
              type_wallet: TYPE_WALLET.USER,
            }
          )
          .getOne();

        let porcentaje = 0.3 / 100;
        let cantidadARestar = vTransaccion.amountReceived * porcentaje;
        let valorFinal = vWalletSender2.available - cantidadARestar;

        await this.walletUserRepository.update(vWalletSender2.id, {
          available: valorFinal,
        });

        await this.transWallRepository.save({
          concept:
            "EGRESO POR COBRO DEL 0.3% PAGO MOVIL DE TRANSACCION " +
            vTransaccion.reference,
          old_amount: vWalletSender2.available,
          new_amount: valorFinal,
          amount_transaction: +cantidadARestar,
          type: TYPE_PRESUPUESTO.EGRESO,
          wallet: vWalletSender2,
        });
      }

      await axios.get(process.env.URL_QUEUE_NOTIFICATIONS)

      await this.bitacoraRepository.save({
        user: user.id,
        accion: 'Completado de Transaccion: ' + vTransaccion.id
      })

      return ResponseUtil.success(
        vTransaccion,
        "Transaccion completada con exito"
      );
    }

  }

  async penalizar(data) {
    const vTransaccion = await this.transactionRepository.findOne({
      where: { id: data.transactionId },
      relations: ["rate", "client", "creator"],
    });

    let newAmountSend = vTransaccion.amountSend - data.amount;
    let newAmountReceive = newAmountSend * vTransaccion.rate.rate;

    await this.transactionRepository.update(vTransaccion.id, {
      amountSend: newAmountSend,
      amountReceived: newAmountReceive,
      penaltyFee: data.amount,
      currencyPenaltyFee: vTransaccion.currencySend,
      descriptionPenaltyFee: data.observation
    });

    const tracking = await this.trackingTransactionsRepository.save({
      status: "PENALIZADA",
      fecha: new Date(),
      transaction: vTransaccion,
    });

    let numero = vTransaccion.client.phone;
    let encodedMessage = "Hola, estimado " +
      vTransaccion.client.full_name +
      " te notificamos que tu transaccion " +
      vTransaccion.reference +
      " por el monto de " +
      vTransaccion.amountSend +
      " " +
      vTransaccion.currencySend +
      ", ha sido penalizada con el importe de  " +
      data.amount +
      " por la razon siguiente: " +
      data.observation

    let payload = {
      message: encodedMessage,
      image: null,
      phone: numero
    }

    this.notificationRepository.save(payload)

    await axios.get(process.env.URL_QUEUE_NOTIFICATIONS)

    await this.bitacoraRepository.save({
      user: vTransaccion.creator,
      accion: 'Penzalizado de Transaccion: ' + vTransaccion.id
    })

    return ResponseUtil.success(
      vTransaccion,
      "Transaccion penalizada conn exito"
    );
  }

  // funcion que carga la imagen
  async uploadImageToCloudinary(
    imageBuffer: Buffer,
    imageName: string
  ): Promise<string> {
    return new Promise((resolve, reject) => {
      const uploadStream = cloudinary.uploader.upload_stream(
        { resource_type: "auto", public_id: imageName },
        (error, result) => {
          if (error) {
            reject(`Error al subir imagen a Cloudinary: ${error.message}`);
          } else {
            resolve(result.secure_url);
          }
        }
      );

      uploadStream.end(imageBuffer);
    });
  }

  // funcion que calcula las ganancias
  calcularValorPorcentaje(montoInicial, valorDecimal) {
    const porcentajeReal = valorDecimal - 1;
    const valorPorcentaje = montoInicial * porcentajeReal;
    return valorPorcentaje;
  }

  async generateComprobante(data) {
    const canvas = createCanvas(600, 600);
    const ctx = canvas.getContext("2d");

    // Cargar la imagen de fondo
    const imageUrl =
      "https://res.cloudinary.com/dpe20ljig/image/upload/v1716356978/pn8pdhtmeiuk8lddmsac.png";
    const response = await axios.get(imageUrl, { responseType: "arraybuffer" });
    const backgroundImage = await loadImage(
      Buffer.from(response.data, "binary")
    );

    // Dibujar la imagen de fondo
    ctx.drawImage(backgroundImage, 0, 0, canvas.width, canvas.height);

    // Agregar el texto sobre la imagen de fondo
    ctx.fillStyle = "black";
    ctx.font = "12px Arial";
    ctx.fillText("Comprobante de Pago", 50, 50);
    ctx.fillText(`Referencia: ${data.reference}`, 50, 100);
    ctx.fillText(`Origen: ${data.source}`, 50, 150);
    ctx.fillText(`Monto: ${data.amount}`, 50, 200);
    ctx.fillText(`Destino: ${data.destination}`, 50, 250);
    ctx.fillText(`Monto: ${data.destinationAmount}`, 50, 300);

    const buffer = canvas.toBuffer("image/png");
    const imagePath = path.join(__dirname, "receipt.png");
    fs.writeFileSync(imagePath, buffer);

    // Subir a Cloudinary
    const result = await cloudinary.uploader.upload(imagePath, {
      folder: "receipts",
    });

    // Eliminar el archivo local
    fs.unlinkSync(imagePath);

    return result.secure_url;
  }


  async reportUser(creador, inicio, fin, pais) {
    let query = this.transactionRepository.createQueryBuilder("trans");

    if (creador) {
      query = query.where("trans.creatorId = :creador", { creador });
    }

    if (pais) {
      query = query.andWhere("trans.originId = :pais", { pais })
    }

    if (inicio || fin) {

      if (inicio && fin) {
        query = query.andWhere("DATE(trans.createdAt) BETWEEN :inicio AND :fin", { inicio, fin });
      } else if (inicio) {
        query = query.andWhere("DATE(trans.createdAt) >= :inicio", { inicio });
      } else if (fin) {
        query = query.andWhere("DATE(trans.createdAt) <= :fin", { fin });
      }
    }

    let data = await query.getMany();

    return { data, message: 'Reporte Obtenido con exito' };
  }


}
