import { Injectable } from '@nestjs/common';
import { S3 } from 'aws-sdk';
import { exec } from 'child_process';
import { promisify } from 'util';
import * as fs from 'fs';
import * as nodemailer from 'nodemailer';
import * as zlib from 'zlib';

@Injectable()
export class BackupService {
    private readonly s3: S3;
    private readonly transporter: nodemailer.Transporter;

    constructor() {
        this.s3 = new S3({
            region: 'us-east-2', // Reemplaza con tu región de AWS
            accessKeyId: process.env.AWS_ACCESS_KEY_ID,
            secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
        });

        this.transporter = nodemailer.createTransport({
            host: 'mail.acamposdigital.com', // Reemplaza con el host de tu proveedor de correo
            port: 465, // Puerto del servidor SMTP
            secure: true, // true para 465, false para otros puertos
            auth: {
                user: 'notificaciones@acamposdigital.com', // Reemplaza con tu correo electrónico
                pass: '1256Eli*a1', // Reemplaza con tu contraseña de correo electrónico
            },
        });
    }

    async createBackupAndUpload(): Promise<void> {
        const execPromise = promisify(exec);
    
        // Obtener la fecha y hora actual
        const now = new Date();
        const formattedDate = now.toISOString().replace(/:/g, '-').replace(/\..+/, '');
    
        // Nombre del archivo de respaldo con fecha y hora
        const backupFileName = `backup-${formattedDate}.sql`;
        const backupFilePath = `./${backupFileName}`;
    
        // Comando para hacer el respaldo de la base de datos (ajústalo según tu base de datos)
        const dumpCommand = `mysqldump -h desarrollo.paneteirl.store -P 3306 -u acampos -p'1256Eli*a1' panet > ${backupFileName}`;
    
        try {
            // Crear el respaldo
            await execPromise(dumpCommand);
            console.log("Se creó el respaldo");
    
            // Comprimir el archivo de respaldo
            const compressedFilePath = await this.compressFile(backupFilePath);
            console.log("Se comprimió el respaldo");
    
            // Enviar el correo con el archivo comprimido
            await this.sendEmailWithAttachment(compressedFilePath, `${backupFileName}.gz`);
    
            // Eliminar el archivo local después de subirlo
            fs.unlinkSync(backupFilePath);
            fs.unlinkSync(compressedFilePath);
    
        } catch (error) {
            console.error('Error creating or uploading backup:', error);
            throw new Error('Backup process failed');
        }
    }

    private async sendEmailWithAttachment(filePath: string, fileName: string): Promise<void> {
        const mailOptions = {
            from: '"SISPANET | Respaldos" <notificaciones@acamposdigital.com>', // Remitente
            to: ['admpanetmxn@gmail.com', 'armandocamposf@gmail.com'], // Lista de destinatarios
            subject: 'Database Backup', // Asunto
            text: 'Attached is the latest database backup.', // Texto del mensaje
            attachments: [
                {
                    filename: fileName,
                    path: filePath,
                },
            ],
        };

        try {
            await this.transporter.sendMail(mailOptions);
            console.log('Email sent successfully');
        } catch (error) {
            console.error('Error sending email:', error.message);
            console.error('Stack trace:', error.stack);
            throw new Error('Email sending failed');
        }
    }

    private async compressFile(filePath: string): Promise<string> {
        const gzip = promisify(zlib.gzip);
        const input = fs.createReadStream(filePath);
        const output = `${filePath}.gz`;
    
        const buffer = await gzip(await fs.promises.readFile(filePath));
        await fs.promises.writeFile(output, buffer);
    
        return output;
    }

}
