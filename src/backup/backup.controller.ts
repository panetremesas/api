import { Controller, Get } from '@nestjs/common';
import { BackupService } from './backup.service';

@Controller('backup')
export class BackupController {
  constructor(private readonly backupService: BackupService) {}

  @Get()
  async createBackup() {
    await this.backupService.createBackupAndUpload();
    return { message: 'Backup created and uploaded successfully' };
  }
}
