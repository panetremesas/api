import { Injectable, Logger } from "@nestjs/common";
import { CreateRateDto } from "./dto/create-rate.dto";
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { Country } from "src/countries/entities/country.entity";
import { Rate } from "./entities/rate.entity";
import { ResponseUtil } from "src/utils/response.util";

@Injectable()
export class RatesService {
  constructor(
    @InjectRepository(Country)
    private readonly countrieRepository: Repository<Country>,
    @InjectRepository(Rate)
    private readonly rateRepository: Repository<Rate>
  ) {}

  async create(createRateDto: CreateRateDto) {}

  async findAll() {
    const rates = await this.rateRepository.find({
      relations: ["origin", "destination"],
    });
    return ResponseUtil.success(rates, "Tasas Obtenidas con exito");
  }

  findOne(id: number) {
    return `This action returns a #${id} rate`;
  }

  async fidnOrigin(id: number) {
    const origin = new Country();
    origin.id = id;
    const rates = await this.rateRepository.find({
      where: { origin: origin },
      relations: ["origin", "destination"],
    });
    return ResponseUtil.success(rates, "Tasas Obtenidas con exito");
  }

  async fidnOriginActive(id: number) {
    const origin = new Country();
    origin.id = id;
    const rates = await this.rateRepository.find({
      where: { origin: origin },
      relations: ["origin", "destination"],
    });
    return ResponseUtil.success(rates, "Tasas Obtenidas con exito");
  }

  async update() {
    const countries = await this.countrieRepository.find();
    let type_profit = "";
  
    await Promise.all(countries.map(async (origin) => {
      const idOrigen = origin.id;
  
      await Promise.all(countries.map(async (destination) => {
        let calculo = 0;
  
        // venezuela otros paises
        if (idOrigen === 1) {
          // venezuela a otro pais que no sea colombia
          if (destination.id !== 2) {
            let punto = destination.rate_wholesale / 100
            let porcentaje = punto * destination.especial_profit
            const generar =  parseFloat(destination.rate_wholesale.toString()) + parseFloat(porcentaje.toString())
            calculo = generar;
          }
          // venezuela a colombia
          if (destination.id === 2) {
            const punto = destination.rate_wholesale / 100;
            const valor = destination.especial_profit.toString();
            const porcentaje = parseFloat(valor);
            const restar = punto * porcentaje;
            const generar = destination.rate_wholesale - restar;
            calculo = generar;
          }
        }
  
        // colombia a otros paises
        if (idOrigen === 2) {
          if (destination.id === 1) {
            const mayor = origin.rate_purchase / destination.rate_sales;
            const punto = mayor / 100;
            const porcentaje = parseFloat(destination.profit.toString());
            const restar = punto * porcentaje;
            const generar = mayor + restar;
            calculo = generar;
          } else {
            const mayor = destination.rate_sales / origin.rate_purchase;
            const punto = mayor / 100;
            const valor = destination.profit.toString();
            const porcentaje = parseFloat(valor);
            const restar = punto * porcentaje;
            const generar = mayor - restar;
            calculo = generar;
          }
        }
  
        if (idOrigen !== 2 && idOrigen !== 1) {
          const venta = destination.rate_sales;
          const compra = origin.rate_purchase;
          const montoInicial = venta / compra;
          const punto = montoInicial / 100;
  
          let valor = "";
          if (destination.id === 1) {
            valor = origin.ven_profit.toString();
          } else {
            valor = origin.profit.toString();
          }
  
          const porcentaje = parseFloat(valor);
          const restar = punto * porcentaje;
          const resultado = montoInicial - restar;
          calculo = resultado;
        }
  
        if (idOrigen == 1) {
          type_profit = "especial_profit";
        }
        if (idOrigen == 2) {
          type_profit = "especial_profit";
        }
        if (idOrigen !== 1 && idOrigen !== 2) {
          if (destination.id === 1) {
            type_profit = "ven_profit";
          } else {
            type_profit = "profit";
          }
        }
  
        let assigment = type_profit;
        const vExist = await this.rateRepository
          .createQueryBuilder("rate")
          .where("rate.originId = :originId", { originId: idOrigen })
          .andWhere("rate.destinationId = :destinationId", {
            destinationId: destination.id,
          })
          .getOne();
  
        if (calculo > 0) {
          if (vExist === null) {
            const data = {
              origin: origin,
              destination: destination,
              rate: calculo,
              type_profit: assigment,
            };
            await this.rateRepository.save(data);
          } else {
            const data = {
              rate: calculo,
              type_profit: assigment,
            };
            await this.rateRepository.update(vExist.id, data);
          }
        }
      }));
    }));
  }
  

  async updateStatus(status, id) {
    const update = await this.rateRepository.update(id, { status });
    return ResponseUtil.success(update, "Status Actualizado con exito");
  }

  remove(id: number) {
    return `This action removes a #${id} rate`;
  }
}
