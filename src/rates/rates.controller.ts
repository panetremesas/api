import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { RatesService } from './rates.service';
import { CreateRateDto } from './dto/create-rate.dto';
import { UpdateRateDto } from './dto/update-rate.dto';
import { AuthGuard } from 'src/auth/auth.guard';

@UseGuards(AuthGuard)
@Controller('rates')
export class RatesController {
  constructor(private readonly ratesService: RatesService) {}

  @Post()
  create(@Body() createRateDto: CreateRateDto) {
    return this.ratesService.create(createRateDto);
  }

  @Get()
  findAll() {
    return this.ratesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.ratesService.findOne(+id);
  }

  @Get('update-status/:status/:id')
  updateStatus(@Param('status') status: boolean, @Param('id') id: number) {
    return this.ratesService.updateStatus(status, id);
  }

  @Get('origen/:idPaisOrigen')
  fidnOrigin(@Param('idPaisOrigen') idPaisOrigen: number) {
    return this.ratesService.fidnOrigin(idPaisOrigen);
  }

  @Get('origen/actives/:idPaisOrigen')
  fidnOriginActives(@Param('idPaisOrigen') idPaisOrigen: number) {
    return this.ratesService.fidnOriginActive(idPaisOrigen);
  }

  @Patch()
  update() {
    return this.ratesService.update();
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.ratesService.remove(+id);
  }
}
