import { Country } from "src/countries/entities/country.entity";
import { Column, CreateDateColumn, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Rate {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => Country)
    origin: Country;

    @ManyToOne(() => Country)
    destination: Country;

    @Column({ type: 'decimal', precision: 10, scale: 5 })
    rate: number; 

    @Column({default: true})
    status: boolean

    @Column({nullable: true})
    type_profit: string

    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;

    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: Date;

}
