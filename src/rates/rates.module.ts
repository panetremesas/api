import { Module } from '@nestjs/common';
import { RatesService } from './rates.service';
import { RatesController } from './rates.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Rate } from './entities/rate.entity';
import { Country } from 'src/countries/entities/country.entity';

@Module({
  controllers: [RatesController],
  providers: [RatesService],
  imports: [TypeOrmModule.forFeature([Rate, Country])]
})
export class RatesModule {}
