import { PartialType } from "@nestjs/mapped-types";
import { CreateAccountDto } from "./create-account.dto";
import { IsBoolean, IsString } from "class-validator";

export class UpdateAccountDto extends PartialType(CreateAccountDto) {
  @IsBoolean()
  status: boolean;
}
