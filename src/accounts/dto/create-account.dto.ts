import {
  IsBoolean,
  IsDecimal,
  IsEmail,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString
} from "class-validator";
import { Bank } from "src/banks/entities/bank.entity";
import { Country } from "src/countries/entities/country.entity";
import { TYPE_ACCOUNT, TYPE_MONEDA } from "src/enums/type_account.enum";
import { User } from "src/users/entities/user.entity";

export class CreateAccountDto {
  @IsString()
  titular: string;

  @IsString()
  number: string;

  @IsOptional()
  @IsString()
  type_account: string;

  @IsOptional()
  @IsString()
  document: string;

  @IsEmail()
  email: string;

  @IsEnum(TYPE_MONEDA)
  type_currency: TYPE_MONEDA;

  @IsBoolean()
  commision: boolean;

  @IsOptional()
  @IsDecimal()
  profit_commission: number;

  @IsNumber()
  user: User;

  @IsOptional()
  @IsNumber()
  countrie: Country;

  @IsOptional()
  @IsNumber()
  bank: Bank;

  @IsString()
  @IsOptional()
  message: string;
}
