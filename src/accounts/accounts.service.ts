import { Injectable } from "@nestjs/common";
import { CreateAccountDto } from "./dto/create-account.dto";
import { UpdateAccountDto } from "./dto/update-account.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { Account } from "./entities/account.entity";
import { Repository } from "typeorm";
import { User } from "src/users/entities/user.entity";
import { Bank } from "src/banks/entities/bank.entity";
import { Country } from "src/countries/entities/country.entity";
import { ResponseUtil } from "src/utils/response.util";

@Injectable()
export class AccountsService {
  constructor(
    @InjectRepository(Account)
    private readonly accountRepository: Repository<Account>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(Bank)
    private readonly bankRepository: Repository<Bank>,
    @InjectRepository(Country)
    private readonly countrieRepository: Repository<Country>
  ) { }
  create(createAccountDto: CreateAccountDto) {
    const account = this.accountRepository.save(createAccountDto);

    return ResponseUtil.success(account, "Cuenta creada con exito");
  }

  async findByUser(id: number) {
    const accounts = await this.userRepository.findOne({
      where: { id },
      relations: ["accounts", "accounts.bank", "accounts.countrie"],
    });

    const data = {
      module: "Cuentas",
      error: false,
      message: "Cuentas obtenidas con exito",
      data: accounts,
    };

    return data;
  }

  async findByBank(id: number) {
    const accounts = await this.bankRepository.findOne({
      where: { id: id },
      relations: [
        "accounts",
        "accounts.bank",
        "accounts.countrie",
        "accounts.user",
      ],
    });

    if (accounts == null) {
      return ResponseUtil.error("Este Banco no existe");
    }

    return ResponseUtil.success(accounts, "Cuentas obtenidas con exito");
  }

  async findByCountrie(id: number) {
    const accounts = await this.countrieRepository.findOne({
      where: { id: id },
      relations: ["accounts", "accounts.bank", "accounts.countrie"],
    });

    return ResponseUtil.success(accounts, "Cuentas obtenidas con exito");
  }

  async findByCountrieReception(id: number) {
    const accounts = await this.countrieRepository.findOne({
      where: { id: id },
      relations: ["accounts", "accounts.bank", "accounts.user"],
    });

    if (accounts) {
      const result = accounts.accounts;
      return ResponseUtil.success(
        accounts,
        "Cuentas de Recepcion obtenidas con exito"
      );
    } else {
      return ResponseUtil.error("Pais o cuentas no existen");
    }
  }

  async findAll(countrie, bank, userId, status, user, tipo, origen) {
    let queryBuilder = await this.accountRepository
      .createQueryBuilder("account")
      .leftJoinAndSelect("account.countrie", "countrie")
      .leftJoinAndSelect("account.bank", "bank");

    if (!status) {
      queryBuilder = queryBuilder.andWhere("account.status = 1")
    }

    if (countrie) {
      queryBuilder = queryBuilder.andWhere("account.countrieId = :countrie", {
        countrie,
      });
    }

    if (bank) {
      queryBuilder = queryBuilder.andWhere("account.bankid = :bank", { bank });
    }

    if (userId) {
      queryBuilder = queryBuilder.andWhere("account.userId = :userId", {
        userId,
      });
    }

    if (origen == 'APP') {
      console.log("Peticion desde la app")
      queryBuilder = queryBuilder.andWhere("account.status = 1")
    }

    const result = await queryBuilder.getMany();
    return ResponseUtil.success(result, "Cuentas obtenidas con exito");
  }

  findOne(id: number) {
    return `This action returns a #${id} account`;
  }

  update(id: number, updateAccountDto: UpdateAccountDto) {
    const account = this.accountRepository.update(id, updateAccountDto);

    return ResponseUtil.success(account, "Cuenta actualizada")
  }

  updateStatus(id: number, status: boolean) {
    const account = this.accountRepository.update(id, { status });

    return ResponseUtil.success(account, "Estado de Cuenta Actualizado")
  }

  updateTipo(id: number, status: boolean) {
    const account = this.accountRepository.update(id, { tipo: status });

    return ResponseUtil.success(account, "Tipo de cuenta Actualizado")
  }

  remove(id: number) {
    return `This action removes a #${id} account`;
  }
}
