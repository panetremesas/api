import { Bank } from "src/banks/entities/bank.entity";
import { Country } from "src/countries/entities/country.entity";
import { TYPE_MONEDA } from "src/enums/type_account.enum";
import { Presupuesto } from "src/presupuesto/entities/presupuesto.entity";
import { TransactionsTemporary } from "src/transactions-temporary/entities/transactions-temporary.entity";
import { Transaction } from "src/transactions/entities/transaction.entity";
import { User } from "src/users/entities/user.entity";
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class Account {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  titular: string;

  @Column({ nullable: true })
  type_account: string;

  @Column()
  number: string;

  @Column({ nullable: true })
  document: string;

  @Column()
  email: string;

  @Column({ type: "enum", enum: TYPE_MONEDA })
  type_currency: TYPE_MONEDA;

  @Column({ default: false })
  commision: boolean;

  @Column("decimal", { precision: 8, scale: 2, nullable: true })
  profit_commission: number;

  @Column({ type: 'text', nullable: true })
  message: string;

  @Column({ default: true })
  status: boolean;

  @ManyToOne(() => User, (user) => user.accounts)
  user: User;

  @ManyToOne(() => Country, (countrie) => countrie.accounts, { nullable: true })
  countrie: Country;

  @ManyToOne(() => Bank, (bank) => bank.accounts, { nullable: true })
  bank: Bank;

  @OneToMany(() => Transaction, transaction => transaction.account_reception)
  transactions: Transaction[];

  @OneToMany(() => TransactionsTemporary, temporal => temporal.account)
  transactions_temporales: TransactionsTemporary[]

  @Column({ type: 'boolean', default: true })
  tipo: boolean

  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt: Date;
}
