import { Module } from "@nestjs/common";
import { AccountsService } from "./accounts.service";
import { AccountsController } from "./accounts.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Account } from "./entities/account.entity";
import { User } from "src/users/entities/user.entity";
import { Bank } from "src/banks/entities/bank.entity";
import { Country } from "src/countries/entities/country.entity";

@Module({
  imports: [TypeOrmModule.forFeature([Account, User, Bank, Country])],
  controllers: [AccountsController],
  providers: [AccountsService],
})
export class AccountsModule {}
