import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  Request,
  UseGuards,
} from "@nestjs/common";
import { AccountsService } from "./accounts.service";
import { CreateAccountDto } from "./dto/create-account.dto";
import { UpdateAccountDto } from "./dto/update-account.dto";
import { AuthGuard } from "src/auth/auth.guard";

@Controller("accounts")
export class AccountsController {
  constructor(private readonly accountsService: AccountsService) { }

  @Post()
  create(@Body() createAccountDto: CreateAccountDto) {
    return this.accountsService.create(createAccountDto);
  }

  @Get()
  @UseGuards(AuthGuard)
  findAll(@Query('countrie') countrie, @Query('bank') bank, @Query('userId') userId, @Query("status") status, @Request() req, @Query("tipo") tipo) {
    return this.accountsService.findAll(countrie, bank, userId, status, req.user.id, tipo, 'WEB');
  }

  @Get("byUser/:id")
  findByUser(@Param("id") id: number) {
    return this.accountsService.findByUser(id);
  }

  @Get("byBank/:id")
  findByBank(@Param("id") id: number) {
    return this.accountsService.findByBank(id);
  }

  @Get("byCountrie/:id")
  findByCountrie(@Param("id") id: number) {
    return this.accountsService.findByCountrie(id);
  }

  @Get(":id")
  findOne(@Param("id") id: string) {
    return this.accountsService.findOne(+id);
  }

  @Patch(":id")
  update(@Param("id") id: string, @Body() updateAccountDto: UpdateAccountDto) {
    return this.accountsService.update(+id, updateAccountDto);
  }

  @Get('update-status/:id/:status')
  updateStatus(@Param("id") id: number, @Param("status") status: boolean) {
    return this.accountsService.updateStatus(id, status)
  }

  @Get('update-tipo/:id/:status')
  updateTipo(@Param("id") id: number, @Param("status") status: boolean) {
    return this.accountsService.updateTipo(id, status)
  }

  @Delete(":id")
  remove(@Param("id") id: string) {
    return this.accountsService.remove(+id);
  }
}
