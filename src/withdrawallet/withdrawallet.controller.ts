import { Controller, Get, Post, Body, Patch, Param, Delete, Query, Request } from '@nestjs/common';
import { WithdrawalletService } from './withdrawallet.service';
import { CreateWithdrawalletDto } from './dto/create-withdrawallet.dto';
import { UpdateWithdrawalletDto } from './dto/update-withdrawallet.dto';

@Controller('withdrawallet')
export class WithdrawalletController {
  constructor(private readonly withdrawalletService: WithdrawalletService) {}

  @Post()
  create(@Body() createWithdrawalletDto, @Request() req) {
    return this.withdrawalletService.create(createWithdrawalletDto, req.user);
  }

  @Get()
  findAll(@Query('userId') userId) {
    return this.withdrawalletService.findAll(userId);
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.withdrawalletService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: number, @Body() updateWithdrawalletDto) {
    return this.withdrawalletService.update(id, updateWithdrawalletDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.withdrawalletService.remove(+id);
  }

  @Post('aprove')
  aprove(@Body() data){
    return this.withdrawalletService.aprove(data)
  }

  @Get('calculo/:id')
  calculo(@Param('id') id: number){
    return this.withdrawalletService.calculo(id)
  }
}
