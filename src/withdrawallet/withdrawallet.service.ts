import { Injectable } from "@nestjs/common";
import { CreateWithdrawalletDto } from "./dto/create-withdrawallet.dto";
import { UpdateWithdrawalletDto } from "./dto/update-withdrawallet.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { Withdrawallet } from "./entities/withdrawallet.entity";
import { Repository } from "typeorm";
import { ResponseUtil } from "src/utils/response.util";
import { WalletUser } from "src/wallet-users/entities/wallet-user.entity";
import {
  STATUS_RETIRO,
  TYPE_ORDER_PAY,
  TYPE_PRESUPUESTO,
  TYPE_WALLET,
} from "src/enums/type_account.enum";
import { User } from "src/users/entities/user.entity";
import { TransWall } from "src/trans-wall/entities/trans-wall.entity";
import { WithdraWalletDetail } from "src/withdra-wallet-detail/entities/withdra-wallet-detail.entity";
import { Transaction } from "src/transactions/entities/transaction.entity";
import { Rate } from "src/rates/entities/rate.entity";

@Injectable()
export class WithdrawalletService {
  constructor(
    @InjectRepository(Withdrawallet)
    private readonly withdrawalletRepository: Repository<Withdrawallet>,
    @InjectRepository(WithdraWalletDetail)
    private readonly withdrawalletDetailRepository: Repository<WithdraWalletDetail>,
    @InjectRepository(WalletUser)
    private readonly walletuserRepository: Repository<WalletUser>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(TransWall)
    private readonly transWallsRepository: Repository<TransWall>,
    @InjectRepository(Transaction)
    private readonly transactionRepository: Repository<Transaction>,
    @InjectRepository(Rate)
    private readonly rateRepository: Repository<Rate>
  ) {}

  async create(createWithdrawalletDto, user) {
    console.log(createWithdrawalletDto);
    const data = await this.withdrawalletRepository.save({
      user: user.id,
      destination: createWithdrawalletDto.countrie,
      instrument: createWithdrawalletDto.instrument
    });

    for (const wallet of createWithdrawalletDto.selectedWallets) {
      if (wallet.status) {
        await this.withdrawalletDetailRepository.save({
          retiro: data,
          wallet: wallet.id,
          amount: wallet.available,
        });

        await this.walletuserRepository.update(wallet.id, { available: 0 });
      }
    }

    return ResponseUtil.success(data, "Retiro registrado con exito");
  }

  async findAll(userId) {
    try {
      let retiros = this.withdrawalletRepository
        .createQueryBuilder("retiro")
        .leftJoinAndSelect("retiro.user", "user")
        .leftJoinAndSelect("retiro.detalles", "detalles")
        .leftJoinAndSelect("detalles.wallet", "wallet")
        .leftJoinAndSelect("wallet.countrie", "countrie");

      if (userId) {
        retiros = retiros.andWhere("retiro.userId = :userId", { userId });
      }

      retiros = retiros.orderBy("retiro.createdAt", "DESC");
      const result = await retiros.getMany();

      return ResponseUtil.success(result, "Retiros obtenidos con éxito");
    } catch (error) {
      console.error("Error al obtener retiros:", error);
      throw error;
    }
  }

  async findOne(id: number) {
    let retiro = await this.withdrawalletRepository.findOne({
      where: { id },
      relations: [
        "user",
        "destination",
        "detalles",
        "instrument",
        "detalles.wallet",
        "detalles.wallet.countrie",
      ],
    });

    return ResponseUtil.success(retiro, "Retiro consultado con exito");
  }

  async update(id: number, updateWithdrawalletDto) {
    const data = await this.withdrawalletRepository.update(id, {
      destination: updateWithdrawalletDto.destination,
      instrument: updateWithdrawalletDto.instrument,
    });

    return ResponseUtil.success(data, "Retiro actualizado con exito");
  }

  remove(id: number) {
    return `This action removes a #${id} withdrawallet`;
  }

  async aprove(data) {
    await this.withdrawalletRepository.update(data.id, {
      status: STATUS_RETIRO.ORDENADA,
      amount_send: data.monto_envio,
      amount_discount: data.descuento,
      observation_discount: data.observation_discount,
    });

    // Recorrer los detalles
    for (const detalle of data.detalles) {
      await this.withdrawalletDetailRepository.update(detalle.id, {
        amount_transform: detalle.montoTransformado,
      });
    }

    let data2 = await this.withdrawalletRepository.findOne({
      where: { id: data.id },
      relations: ["destination", "instrument", "instrument.client"],
    });

    const imageUrl = "NO PROCEDE";
    // consulta la tsas

    // se genera la referencia
    const lastTransaction = await this.transactionRepository
      .createQueryBuilder("transaction")
      .orderBy("transaction.id", "DESC")
      .getOne();

    let nextId = 1;

    if (lastTransaction) {
      nextId = lastTransaction.id + 1;
    }

    let number = Math.floor(Math.random() * 9000000) + 1000000;
    const referencia = "PT-" + number + "-" + nextId;

    const rate = await this.rateRepository.findOne({ where: { id: 1 } });

    const save2 = await this.transactionRepository.save({
      amountSend: 0, // si
      currencySend: data2.destination.currency,
      amountReceived: data.monto_envio,
      currencyReceived: data2.destination.currency,
      dateProofOfSend: new Date(),
      numberProofOfSend: "1111111111",
      client: data2.instrument.client,
      instrument: data2.instrument,
      rate: rate,
      creator: data.creator,
      proofOfSend: imageUrl,
      reference: referencia, // si
      puller_profit: 0,
      account_profit: 0,
      home_profit: 0,
      account: null,
      owner: data.creator,
      origin: data2.destination,
      destination: data2.destination,
      wallet_reception: null,
      account_reception: null,
      observation: "PAGO DE COMISIONES ID: " + data.id,
    });

    return ResponseUtil.success(save2.id, "Detalle del registro!");
  }

  async calculo(id) {
    let retiro = this.withdrawalletRepository
      .createQueryBuilder("retiro")
      .where("retiro.id = :id", { id })
      .leftJoinAndSelect("retiro.detalles", "detalles")
      .leftJoinAndSelect("detalles.wallet", "wallet")
      .leftJoinAndSelect("wallet.countrie", "countrie")
      .leftJoinAndSelect("retiro.destination", "destination");
    let result = await retiro.getOne();

    const venta = result.destination.rate_sales;
    const results = [];

    const detalles = result.detalles;
    let total = 0;
    detalles.forEach((detalle) => {
      const compra = detalle.wallet.countrie.rate_purchase;
      let porcentaje = 0.02;
      let mayor = venta / compra;
      let descuento = 0;
      let result2 = 0;
      let trs = 0;
      // si es de venezuela a otros paises
      if (detalle.wallet.countrie.id == 1) {
        mayor = compra / venta;
        descuento = mayor * porcentaje;
        result2 = mayor + descuento;
        trs = detalle.amount / result2;
      }
      // si es colombia a venezuela
      if (detalle.wallet.countrie.id == 1 && result.destination.id == 2) {
        mayor = venta / compra;
        descuento = mayor * porcentaje;
        result2 = mayor + descuento;
        trs = result2 / detalle.amount;
      }

      if (detalle.wallet.countrie.id != 1) {
        mayor = venta / compra;
        descuento = mayor * porcentaje;
        result2 = mayor - descuento;
        trs = result2 * detalle.amount;
      }

      total = total + trs;
      results.push({
        mayor,
        countrie: detalle.wallet.countrie.name,
        amount: detalle.amount,
        transform: trs,
      });
    });

    let data = {
      paisRecepcion: result.destination.name,
      total,
      results,
    };

    return ResponseUtil.success(data, "Detalle del registro!");
  }
}
