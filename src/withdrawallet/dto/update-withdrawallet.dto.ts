import { PartialType } from '@nestjs/mapped-types';
import { CreateWithdrawalletDto } from './create-withdrawallet.dto';

export class UpdateWithdrawalletDto extends PartialType(CreateWithdrawalletDto) {}
