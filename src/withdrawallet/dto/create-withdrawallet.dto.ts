import { IsNotEmpty, IsNumber, IsNumberString, IsOptional, IsString } from "class-validator";
import { ClientInstrument } from "src/client-instruments/entities/client-instrument.entity";
import { Country } from "src/countries/entities/country.entity";
import { User } from "src/users/entities/user.entity";
import { WalletUser } from "src/wallet-users/entities/wallet-user.entity";

export class CreateWithdrawalletDto {
  @IsNotEmpty()
  wallet: WalletUser;

  @IsNumber()
  amount: number;

  @IsOptional()
  description: string;

  @IsNotEmpty()
  destination: Country

  @IsNotEmpty()
  instrument: ClientInstrument

  @IsNumberString()
  user: User
}
