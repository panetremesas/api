import { Module } from '@nestjs/common';
import { WithdrawalletService } from './withdrawallet.service';
import { WithdrawalletController } from './withdrawallet.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Withdrawallet } from './entities/withdrawallet.entity';
import { WalletUser } from 'src/wallet-users/entities/wallet-user.entity';
import { User } from 'src/users/entities/user.entity';
import { TransWall } from 'src/trans-wall/entities/trans-wall.entity';
import { WithdraWalletDetail } from 'src/withdra-wallet-detail/entities/withdra-wallet-detail.entity';
import { Transaction } from 'src/transactions/entities/transaction.entity';
import { Rate } from 'src/rates/entities/rate.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Withdrawallet, WalletUser, User, TransWall, WithdraWalletDetail, Transaction, Rate])],
  controllers: [WithdrawalletController],
  providers: [WithdrawalletService],
})
export class WithdrawalletModule {}
