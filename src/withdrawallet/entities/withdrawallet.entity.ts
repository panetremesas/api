import { ClientInstrument } from "src/client-instruments/entities/client-instrument.entity";
import { Country } from "src/countries/entities/country.entity";
import { STATUS_RETIRO } from "src/enums/type_account.enum";
import { User } from "src/users/entities/user.entity";
import { WalletUser } from "src/wallet-users/entities/wallet-user.entity";
import { WithdraWalletDetail } from "src/withdra-wallet-detail/entities/withdra-wallet-detail.entity";
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class Withdrawallet {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  amount_send: number;

  @Column({ nullable: true })
  amount_discount: number;

  @Column({ nullable: true })
  observation_discount: string

  @ManyToOne(() => Country)
  destination: Country;

  @ManyToOne(() => ClientInstrument)
  instrument: ClientInstrument

  @ManyToOne(() => User)
  user: User;

  @Column({ type: "enum", enum: STATUS_RETIRO, default: STATUS_RETIRO.CREADA })
  status: STATUS_RETIRO;

  @OneToMany(() => WithdraWalletDetail, (detalle) => detalle.retiro)
  detalles: WithdraWalletDetail[]

  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt: Date;
}
