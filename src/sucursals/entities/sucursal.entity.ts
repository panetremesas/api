import { User } from "src/users/entities/user.entity";
import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Sucursal {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    code: string

    @Column()
    name: string

    @Column()
    address: string

    @Column()
    phone: string

    @Column({ default: false })
    remittances: boolean

    @Column({ default: false })
    shipments: boolean

    @Column({ nullable: true })
    userSurAmerica: string

    @Column({ nullable: true })
    passwordSurAmerica: string

    @Column({ default: true })
    status: boolean

    @OneToMany(() => User, user => user.sucursal)
    users: User[];

    @CreateDateColumn({ type: 'timestamp' })
    createdAt: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt: Date;
}
