import { Module } from '@nestjs/common';
import { SucursalsService } from './sucursals.service';
import { SucursalsController } from './sucursals.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Sucursal } from './entities/sucursal.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Sucursal])],
  controllers: [SucursalsController],
  providers: [SucursalsService],
})
export class SucursalsModule { }
