import { Injectable } from '@nestjs/common';
import { CreateSucursalDto } from './dto/create-sucursal.dto';
import { UpdateSucursalDto } from './dto/update-sucursal.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Sucursal } from './entities/sucursal.entity';
import { Repository } from 'typeorm';

@Injectable()
export class SucursalsService {

  constructor(
    @InjectRepository(Sucursal)
    private readonly sucursalRepository: Repository<Sucursal>
  ) { }

  async create(createSucursalDto: CreateSucursalDto) {
    const sucursal = await this.sucursalRepository.save(createSucursalDto)

    const data = {
      module: 'Sucursales',
      error: false,
      message: 'Sucursal creada con exito',
      data: sucursal
    }
    return data
  }

  async findAll() {
    const data = {
      module: 'Sucursales',
      error: false,
      message: 'Listado Obtenido con exito',
      data: await this.sucursalRepository.find()
    }

    return data
  }

  async findOne(id: any) {
    const sucursal = await this.sucursalRepository.findOne({ where: { id }, relations: ['users'] })
    if (sucursal == null) {
      const data = {
        module: 'Sucursales',
        error: true,
        message: 'Sucursal no existe',
        data: null
      }
      return data
    }
    const data = {
      module: 'Sucursales',
      error: false,
      message: 'Listado Obtenido con exito',
      data: sucursal
    }
    return data
  }

  async update(id: any, updateSucursalDto: UpdateSucursalDto) {
    const sucursal = this.sucursalRepository.update(id, updateSucursalDto)

    const data = {
      module: 'Sucursales',
      error: false,
      message: 'Sucursal Actualizada con exito',
      data: sucursal
    }
    return data
  }

}
