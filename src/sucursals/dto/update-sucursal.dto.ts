import { PartialType } from '@nestjs/mapped-types';
import { CreateSucursalDto } from './create-sucursal.dto';
import { IsBoolean } from 'class-validator';

export class UpdateSucursalDto extends PartialType(CreateSucursalDto) {
    @IsBoolean()
    status: boolean
}
