import { IsBoolean, IsNumber, IsOptional, IsString, MinLength } from "class-validator"

export class CreateSucursalDto {
    @IsString()
    @MinLength(3)
    code: string

    @IsString()
    @MinLength(3)
    name: string

    @IsString()
    @MinLength(5)
    address: string

    @IsOptional()
    phone: string

    @IsBoolean()
    @IsOptional()
    remittances: boolean

    @IsBoolean()
    @IsOptional()
    shipments: boolean

    @IsOptional()
    userSurAmerica: string

    @IsOptional()
    passwordSurAmerica: string
}
