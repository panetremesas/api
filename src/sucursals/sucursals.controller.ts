import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { SucursalsService } from './sucursals.service';
import { CreateSucursalDto } from './dto/create-sucursal.dto';
import { UpdateSucursalDto } from './dto/update-sucursal.dto';
import { AuthGuard } from 'src/auth/auth.guard';

@UseGuards(AuthGuard)
@Controller('sucursals')
export class SucursalsController {
  constructor(private readonly sucursalsService: SucursalsService) { }

  @Post()
  create(@Body() createSucursalDto: CreateSucursalDto) {
    return this.sucursalsService.create(createSucursalDto);
  }

  @Get()
  findAll() {
    return this.sucursalsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: any) {
    return this.sucursalsService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: any, @Body() updateSucursalDto: UpdateSucursalDto) {
    return this.sucursalsService.update(id, updateSucursalDto);
  }
}
