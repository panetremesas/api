import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ClientInstrumentsService } from './client-instruments.service';
import { CreateClientInstrumentDto } from './dto/create-client-instrument.dto';
import { UpdateClientInstrumentDto } from './dto/update-client-instrument.dto';

@Controller('client-instruments')
export class ClientInstrumentsController {
  constructor(private readonly clientInstrumentsService: ClientInstrumentsService) {}

  @Post()
  create(@Body() createClientInstrumentDto: CreateClientInstrumentDto) {
    return this.clientInstrumentsService.create(createClientInstrumentDto);
  }

  @Get()
  findAll() {
    return this.clientInstrumentsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.clientInstrumentsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateClientInstrumentDto: UpdateClientInstrumentDto) {
    return this.clientInstrumentsService.update(+id, updateClientInstrumentDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.clientInstrumentsService.remove(+id);
  }
}
