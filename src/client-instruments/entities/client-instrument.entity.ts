import { Bank } from "src/banks/entities/bank.entity";
import { Client } from "src/clients/entities/client.entity";
import { Transaction } from "src/transactions/entities/transaction.entity";
import { Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class ClientInstrument {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    type_document: string

    @Column()
    document: string

    @Column()
    full_name: string

    @Column({nullable: true})
    email: string

    @Column({nullable: true})
    type_method: string

    @Column()
    id_method: string

    @ManyToOne(() => Bank)
    bank_method: Bank

    @ManyToOne(() => Client, client => client.instruments)
    client: Client;

    @OneToMany(() => Transaction, transaction => transaction.instrument)
    transactions: Transaction[];

    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;

    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: Date;
    
}
