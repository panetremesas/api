import { Injectable } from '@nestjs/common';
import { CreateClientInstrumentDto } from './dto/create-client-instrument.dto';
import { UpdateClientInstrumentDto } from './dto/update-client-instrument.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { ClientInstrument } from './entities/client-instrument.entity';
import { Repository } from 'typeorm';
import { ResponseUtil } from 'src/utils/response.util';

@Injectable()
export class ClientInstrumentsService {

  constructor(
    @InjectRepository(ClientInstrument)
    private readonly clientInstrumentRepository: Repository<ClientInstrument>
  ){}

  async create(createClientInstrumentDto: CreateClientInstrumentDto) {
    const data = await this.clientInstrumentRepository.save(createClientInstrumentDto)

    return ResponseUtil.success(data, 'Instrumento Creado con exito')
  }

  findAll() {
    return `This action returns all clientInstruments`;
  }

  findOne(id: number) {
    return `This action returns a #${id} clientInstrument`;
  }

  async update(id: number, updateClientInstrumentDto: UpdateClientInstrumentDto) {
    const data = await this.clientInstrumentRepository.update(id, updateClientInstrumentDto)

    return ResponseUtil.success(data, "Wallet Vaciado con exito");
  }

  remove(id: number) {
    return `This action removes a #${id} clientInstrument`;
  }
}
