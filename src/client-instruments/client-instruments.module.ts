import { Module } from '@nestjs/common';
import { ClientInstrumentsService } from './client-instruments.service';
import { ClientInstrumentsController } from './client-instruments.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientInstrument } from './entities/client-instrument.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ClientInstrument])],
  controllers: [ClientInstrumentsController],
  providers: [ClientInstrumentsService],
})
export class ClientInstrumentsModule {}
