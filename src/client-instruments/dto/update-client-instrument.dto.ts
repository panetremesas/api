import { PartialType } from '@nestjs/mapped-types';
import { CreateClientInstrumentDto } from './create-client-instrument.dto';

export class UpdateClientInstrumentDto extends PartialType(CreateClientInstrumentDto) {}
