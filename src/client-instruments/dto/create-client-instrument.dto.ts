import { IsEmail, IsNumber, IsOptional, IsString, IsNotEmpty } from "class-validator"
import { Bank } from "src/banks/entities/bank.entity"
import { Client } from "src/clients/entities/client.entity"

export class CreateClientInstrumentDto {
    @IsString()
    @IsNotEmpty()
    type_document: string

    @IsNotEmpty()
    document: string

    @IsString()
    @IsNotEmpty()
    full_name: string

    @IsEmail()
    @IsOptional()
    email: string

    @IsString()
    @IsNotEmpty()
    type_method: string

    @IsString()
    @IsNotEmpty()
    id_method: string

    @IsNumber()
    @IsOptional()
    bank_method: Bank

    @IsNumber()
    @IsNotEmpty()
    client: Client;
}
