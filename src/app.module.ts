import { Module, OnApplicationBootstrap } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AccountsModule } from "./accounts/accounts.module";
import { AuthModule } from "./auth/auth.module";
import { BanksModule } from "./banks/banks.module";
import { CountriesModule } from "./countries/countries.module";
import { SeederService } from "./seeder/seeder.service";
import { SucursalsModule } from "./sucursals/sucursals.module";
import { User } from "./users/entities/user.entity";
import { UsersModule } from "./users/users.module";
import { PresupuestoModule } from "./presupuesto/presupuesto.module";
import { AppsModule } from "./apps/apps.module";
import { ClientsModule } from "./clients/clients.module";
import { ClientInstrumentsModule } from "./client-instruments/client-instruments.module";
import { RatesModule } from "./rates/rates.module";
import { TransactionsModule } from "./transactions/transactions.module";
import { WalletUsersModule } from "./wallet-users/wallet-users.module";
import { MulterModule } from "@nestjs/platform-express";
import { diskStorage } from "multer";
import { TransactionsTrackingModule } from "./transactions-tracking/transactions-tracking.module";
import { TransWallModule } from './trans-wall/trans-wall.module';
import { WalletsRefillsModule } from './wallets-refills/wallets-refills.module';
import { ConfigModule } from "@nestjs/config";
import { WithdrawalletModule } from './withdrawallet/withdrawallet.module';
import { DocumentsModule } from './documents/documents.module';
import { DocumentsExpedientModule } from './documents-expedient/documents-expedient.module';
import { ExpedientModule } from './expedient/expedient.module';
import { QueuesModule } from './queues/queues.module';
import { TransactionsTemporaryModule } from './transactions-temporary/transactions-temporary.module';
import { WithdraWalletDetailModule } from './withdra-wallet-detail/withdra-wallet-detail.module';
import { BullModule } from "@nestjs/bull";
import { NotificationsModule } from "./notifications/notifications.module";
import { BackupModule } from './backup/backup.module';
import { DolarModule } from './dolar/dolar.module';
import { BitacoraModule } from './bitacora/bitacora.module';
import { PublicidadModule } from './publicidad/publicidad.module';
import { RechargeModule } from './recharge/recharge.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: [
        '.prod.env',
        '.dev.env',
        '.env'
      ]
    }),
    MulterModule.register({
      storage: diskStorage({
        destination: "./uploads",
      }),
    }),
    BullModule.forRoot({
      redis: {
        host: 'localhost',
        port: 6379,
      },
    }),
    TypeOrmModule.forRoot({
      type: "mysql",
      host: process.env.DB_HOST || "desarrollo.paneteirl.store",
      port: parseInt(process.env.DB_PORT, 10) || 3306,
      username: process.env.DB_USERNAME || "acampos",
      password: process.env.DB_PASSWORD || "1256Eli*a1",
      database: process.env.DB_DATABASE || "panet",
      autoLoadEntities: process.env.DB_AUTOLOAD_ENTITIES === "true" || true,
      synchronize: process.env.DB_SYNCHRONIZE === "true" || true,
    }),
    UsersModule,
    SucursalsModule,
    AccountsModule,
    CountriesModule,
    BanksModule,
    AuthModule,
    TypeOrmModule.forFeature([User]),
    PresupuestoModule,
    AppsModule,
    ClientsModule,
    ClientInstrumentsModule,
    RatesModule,
    TransactionsModule,
    WalletUsersModule,
    TransactionsTrackingModule,
    TransWallModule,
    WalletsRefillsModule,
    WithdrawalletModule,
    DocumentsModule,
    DocumentsExpedientModule,
    ExpedientModule,
    QueuesModule,
    TransactionsTemporaryModule,
    WithdraWalletDetailModule,
    NotificationsModule,
    BackupModule,
    DolarModule,
    BitacoraModule,
    PublicidadModule,
    RechargeModule,
  ],
  controllers: [],
  providers: [SeederService],
})
export class AppModule implements OnApplicationBootstrap {
  constructor(private readonly seederService: SeederService) {}

  async onApplicationBootstrap() {
    await this.seederService.seed();
  }
}
