import { ClientInstrument } from "src/client-instruments/entities/client-instrument.entity";
import { Transaction } from "src/transactions/entities/transaction.entity";
import { User } from "src/users/entities/user.entity";
import { Collection, Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Client {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    type_document: string

    @Column()
    document: string

    @Column()
    full_name: string

    @Column()
    phone: string

    @Column()
    email: string

    @ManyToOne(() => User, user => user.clients, { nullable: true })
    jalador: User;

    @OneToMany(() => ClientInstrument, instrument => instrument.client)
    instruments: ClientInstrument[]

    @OneToMany(() => Transaction, transaction => transaction.client)
    transactions: Transaction[];

    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;

    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: Date;
}