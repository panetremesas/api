import { IsEmail, IsNotEmpty, IsNumber, IsNumberString, IsOptional, IsString } from "class-validator"
import { User } from "src/users/entities/user.entity"

export class CreateClientDto {
    @IsString()
    @IsNotEmpty()
    type_document: string

    @IsNotEmpty()
    document: string
    
    @IsString()
    @IsNotEmpty()
    full_name: string

    @IsString()
    @IsNotEmpty()
    phone: string

    @IsEmail()
    @IsNotEmpty()
    email: string

    @IsNumber()
    @IsOptional()
    jalador: User;
}
