import { Injectable } from "@nestjs/common";
import { CreateClientDto } from "./dto/create-client.dto";
import { UpdateClientDto } from "./dto/update-client.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { Client } from "./entities/client.entity";
import { Repository } from "typeorm";
import { ResponseUtil } from "src/utils/response.util";

@Injectable()
export class ClientsService {
  constructor(
    @InjectRepository(Client)
    private readonly clientRepository: Repository<Client>
  ) {}
  async create(createClientDto: CreateClientDto) {
    const vClient = await this.clientRepository.findOneBy({
      document: createClientDto.document,
    });

    if (vClient !== null) {
      return ResponseUtil.error(
        "Este cliente no se puede registrar por que ya existe"
      );
    }

    const data = await this.clientRepository.save(createClientDto);

    return ResponseUtil.success(data, "Cliente creado con exito");
  }

  async findAll(jaladorId) {
    const queryBuilder = this.clientRepository.createQueryBuilder("client");

    if (jaladorId) {
      queryBuilder.andWhere("client.jaladorId = :jaladorId", {
        jaladorId,
      });
    }

    const result = await queryBuilder.getMany();

    if (!result) {
      return ResponseUtil.error(
        "No se encontro ningun Cliente con este criterio de busqueda"
      );
    }

    return ResponseUtil.success(result, "Clientes obtenidos con éxito.");
  }

  async findOne(document: string) {
    const vClient = await this.clientRepository.findOne({
      where: { document },
      relations: ["transactions", "instruments", "instruments.bank_method"],
    });

    if (vClient == null) {
      return ResponseUtil.error("Cliente no existe");
    }

    return ResponseUtil.success(vClient, "Cliente Obtenido con exito");
  }

  async findById(idCliente: string, document) {
    let vClient = null;
    if (!document) {
      vClient = await this.clientRepository.findOne({
        where: { id: parseInt(idCliente) },
        relations: ["instruments", "transactions"],
      });
    } else {
      vClient = await this.clientRepository.findOne({
        where: { document: idCliente },
        relations: ["instruments", "transactions"],
      });
    }

    if (vClient == null) {
      return ResponseUtil.error("Cliente no existe");
    }

    return ResponseUtil.success(vClient, "Cliente Obtenido con exito");
  }

  async update(id: number, updateClientDto: UpdateClientDto) {
    const data = await this.clientRepository.update(id, updateClientDto);

    return ResponseUtil.success(data, "CLiente editado con exito");
  }

  remove(id: number) {
    return `This action removes a #${id} client`;
  }
}
