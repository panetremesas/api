import { Module } from '@nestjs/common';
import { PublicidadService } from './publicidad.service';
import { PublicidadController } from './publicidad.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Publicidad } from './entities/publicidad.entity';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname, join } from 'path';
import { Client } from 'src/clients/entities/client.entity';
import { Notification } from 'src/notifications/entities/notification.entity';
const uploadDir = join(process.cwd(), 'uploads');

@Module({
  imports: [
    MulterModule.register({
      storage: diskStorage({
        destination: (req, file, cb) => {
          cb(null, uploadDir);
        },
        filename: (req, file, cb) => {
          const ext = extname(file.originalname);
          const filename = `${Date.now()}${ext}`;
          cb(null, filename);
        },
      }),
      fileFilter: (req, file, cb) => {
        if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
          cb(null, true);
        } else {
          cb(new Error('Only images are allowed...'), false);
        }
      },
    }),
    TypeOrmModule.forFeature([Publicidad, Client, Notification])],
  controllers: [PublicidadController],
  providers: [PublicidadService],
})
export class PublicidadModule { }
