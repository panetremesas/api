import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Publicidad {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    cliente: string

    @Column()
    numero: string

    @Column({type: 'text'})
    mensage: string

    @Column({type: 'text'})
    imagen: string
}
