import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, UploadedFile } from '@nestjs/common';
import { PublicidadService } from './publicidad.service';
import { CreatePublicidadDto } from './dto/create-publicidad.dto';
import { UpdatePublicidadDto } from './dto/update-publicidad.dto';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('publicidad')
export class PublicidadController {
  constructor(private readonly publicidadService: PublicidadService) {}

  @Post()
  @UseInterceptors(FileInterceptor("image"))
  create(@Body() createPublicidadDto, @UploadedFile() file) {
    return this.publicidadService.create(createPublicidadDto, file.filename);
  }

  @Get()
  findAll() {
    return this.publicidadService.procesar();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.publicidadService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updatePublicidadDto: UpdatePublicidadDto) {
    return this.publicidadService.update(+id, updatePublicidadDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.publicidadService.remove(+id);
  }
}
