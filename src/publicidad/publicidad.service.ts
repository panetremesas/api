import { Injectable } from '@nestjs/common';
import { CreatePublicidadDto } from './dto/create-publicidad.dto';
import { UpdatePublicidadDto } from './dto/update-publicidad.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Client } from 'src/clients/entities/client.entity';
import { Repository } from 'typeorm';
import { Publicidad } from './entities/publicidad.entity';
import { ResponseUtil } from 'src/utils/response.util';
import { Notification } from 'src/notifications/entities/notification.entity';

@Injectable()
export class PublicidadService {

  constructor(
    @InjectRepository(Publicidad)
    private readonly publicidadRepository: Repository<Publicidad>,
    @InjectRepository(Client)
    private readonly clientsRepository: Repository<Client>,
    @InjectRepository(Notification)
    private readonly notificationRepository: Repository<Notification>,
  ) { }

  async create(createPublicidadDto, file) {
    const imageUrl = process.env.URL_IMAGENES + file;
    await this.publicidadRepository.clear();

    const clients = await this.clientsRepository.find();

    for (const client of clients) {
      const cleanedNumber = client.phone.replace(/\D/g, '');
      await this.publicidadRepository.save({
        cliente: client.full_name,
        numero: cleanedNumber,
        mensage: createPublicidadDto.message,
        imagen: imageUrl
      });
    }

    return ResponseUtil.success(null, "Publicidad creada con éxito");
  }

  async procesar() {
    let data = await this.publicidadRepository.find({ take: 20 })

    for (let notificacion of data) {
      await this.notificationRepository.save({
        phone: notificacion.numero,
        message: notificacion.mensage,
        image: notificacion.imagen
      })

      await this.publicidadRepository.delete(notificacion.id)
    }

    return ResponseUtil.success(data, "Agregados a cola de notificaciones correctamente!")
  }


  findAll() {
    return `This action returns all publicidad`;
  }

  findOne(id: number) {
    return `This action returns a #${id} publicidad`;
  }

  update(id: number, updatePublicidadDto: UpdatePublicidadDto) {
    return `This action updates a #${id} publicidad`;
  }

  remove(id: number) {
    return `This action removes a #${id} publicidad`;
  }
}
