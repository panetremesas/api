import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger, ValidationPipe } from '@nestjs/common';
import { TimezoneMiddleware } from './middlewares/times-zones';
import { join } from 'path';
import { existsSync, mkdirSync } from 'fs';
import * as express from 'express';
import Bugsnag from '@bugsnag/js';
import bugsnagPluginExpress from '@bugsnag/plugin-express';

Bugsnag.start({
  apiKey: process.env.BUGSNAG_API_KEY,
  plugins: [bugsnagPluginExpress],
  appVersion: process.env.APP_VERSION,
});


async function bootstrap() {
  const logger = new Logger("MainProvider");
  const app = await NestFactory.create(AppModule);
  const bugsnagMiddleware = Bugsnag.getPlugin('express');
  app.use(bugsnagMiddleware.requestHandler);
  app.use(bugsnagMiddleware.errorHandler);
  const uploadDir = join(process.cwd(), 'uploads');
  if (!existsSync(uploadDir)) {
    mkdirSync(uploadDir);
  }
  app.enableCors();
  app.setGlobalPrefix("api/v1");
  app.use('/uploads', express.static(join(process.cwd(), 'uploads')));
  app.use(new TimezoneMiddleware().use);
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidNonWhitelisted: true,
      transform: true,
    })
  );
  await app.listen(process.env.PORT_SERVER);
  logger.log("Api corriendo en el puerto: ", process.env.PORT_SERVER)
}
bootstrap();
