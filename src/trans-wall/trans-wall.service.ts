import { Injectable } from "@nestjs/common";
import { CreateTransWallDto } from "./dto/create-trans-wall.dto";
import { UpdateTransWallDto } from "./dto/update-trans-wall.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { TransWall } from "./entities/trans-wall.entity";
import { Repository } from "typeorm";
import { ResponseUtil } from "src/utils/response.util";
import { Country } from "src/countries/entities/country.entity";
import { WalletUser } from "src/wallet-users/entities/wallet-user.entity";
import { TYPE_PRESUPUESTO, TYPE_WALLET } from "src/enums/type_account.enum";
import { PushNotificationUtil } from "src/utils/push.utils";

@Injectable()
export class TransWallService {
  constructor(
    @InjectRepository(TransWall)
    private readonly transRepository: Repository<TransWall>,
    @InjectRepository(Country)
    private readonly countrieRepository: Repository<Country>,
    @InjectRepository(WalletUser)
    private readonly walletUserRepository: Repository<WalletUser>,
    @InjectRepository(TransWall)
    private readonly transWallRepository: Repository<TransWall>
  ) {}
  create(createTransWallDto) {
    const vWallet = this.walletUserRepository
      .createQueryBuilder("wallet")
      .leftJoinAndSelect("wallet.user", "user")
      .where("wallet.id = :id", { id: createTransWallDto.wallet })
      .getOne();

    try {
      // const data = this.transRepository.save(createTransWallDto);
      // PushNotificationUtil(vWallet.user.token_owner, "Se ha registrado un nuevo ingreso en tu billetera de gestion PANET Revisa ")
      return ResponseUtil.success(vWallet, "Transaccion registrada con exito");
    } catch (error) {}
  }

  async generateExpenses(data) {
    const vWallet = await this.walletUserRepository.findOne({
      where: { id: data.walletId },
    });

    if (!vWallet) {
      return ResponseUtil.error(
        "Este wallet no se encuentra, intente nuevamente."
      );
    }

    let newAmount = vWallet.available - parseFloat(data.amount);

    await this.transWallRepository.save({
      concept: data.concept,
      old_amount: vWallet.available,
      new_amount: newAmount,
      amount_transaction: data.amount,
      type: TYPE_PRESUPUESTO.EGRESO,
      wallet: vWallet,
    });

    let expense = await this.walletUserRepository.update(vWallet.id, {
      available: newAmount,
    });

    return ResponseUtil.success(expense, "Gasto registrado con éxito");
  }

  async findAll(walletId, type) {
    let queryBuilder = this.transRepository.createQueryBuilder("trans");
    queryBuilder.leftJoinAndSelect("trans.wallet", "wallet");
    queryBuilder.leftJoinAndSelect("wallet.user", "user");
    queryBuilder.leftJoinAndSelect("wallet.countrie", "countrie");

    // if (type == "INGRESOS") {
    //   queryBuilder.andWhere("trans.type = :type1", {
    //     type1: TYPE_PRESUPUESTO.INGRESO,
    //   });

    //   // Filtro para que los wallets sean de tipo USER
    //   queryBuilder.andWhere("wallet.type_wallet = :walletType", {
    //     walletType: "USER",
    //   });
    // }

    if (walletId) {
      queryBuilder.andWhere("trans.walletId = :walletId", { walletId });
    }

    const result = await queryBuilder.getMany();

    return ResponseUtil.success(result, "Transacciones obtenidas con exito");
  }

  async getOwnerAccountReport(userId, date) {
    // consultar paises
    const countries = await this.countrieRepository.find();
    const wallets = [];

    countries.map((countrie) => {
      // consulta que existan wallet con el id del usuario con el pais y con el tipo
      // const wallet = await this.
    });
  }

  findOne(id: number) {
    return `This action returns a #${id} transWall`;
  }

  update(id: number, updateTransWallDto: UpdateTransWallDto) {
    return `This action updates a #${id} transWall`;
  }

  remove(id: number) {
    return `This action removes a #${id} transWall`;
  }

  async generateReporstProfitsOwner(userId, date) {
    let dateCondition = "";
    if (date) {
      const formattedDate = date.split("T")[0];
      dateCondition = `AND DATE(trans_walls.updatedAt) = '${formattedDate}'`;
    }

    const wallets = await this.walletUserRepository
      .createQueryBuilder("wallet")
      .leftJoinAndSelect("wallet.countrie", "countrie")
      .leftJoin(
        "wallet.trans_walls",
        "trans_walls",
        `trans_walls.concept = :concept ${dateCondition}`,
        { concept: "INGRESO POR COMISION" }
      )
      .addSelect("SUM(trans_walls.amount_transaction)", "profits_obtains")
      .where("wallet.userId = :userId AND wallet.type_wallet = :type_wallet", {
        userId: userId,
        type_wallet: TYPE_WALLET.PROFITS,
      })
      .groupBy("wallet.id")
      .getRawMany();

    return ResponseUtil.success(
      wallets,
      "Estos son los wallets de comisiones disponibles"
    );
  }
}
