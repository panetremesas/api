import { PartialType } from '@nestjs/mapped-types';
import { CreateTransWallDto } from './create-trans-wall.dto';

export class UpdateTransWallDto extends PartialType(CreateTransWallDto) {}
