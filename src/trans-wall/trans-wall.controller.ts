import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UseGuards,
} from "@nestjs/common";
import { TransWallService } from "./trans-wall.service";
import { CreateTransWallDto } from "./dto/create-trans-wall.dto";
import { UpdateTransWallDto } from "./dto/update-trans-wall.dto";
import { WalletUsersService } from "src/wallet-users/wallet-users.service";
import { WalletUser } from "src/wallet-users/entities/wallet-user.entity";
import { ResponseUtil } from "src/utils/response.util";
import { AuthGuard } from "src/auth/auth.guard";

@Controller("trans-wall")
export class TransWallController {
  constructor(
    private readonly transWallService: TransWallService,
    private readonly walletService: WalletUsersService
  ) {}

  @UseGuards(AuthGuard)
  @Post()
  create(@Body() createTransWallDto: CreateTransWallDto) {
    return this.transWallService.create(createTransWallDto);
  }

  @UseGuards(AuthGuard)
  @Get()
  async findAll(
    @Query("userId") userId,
    @Query("countrieId") countrieId,
    @Query("type_wallet") type_wallet,
    @Query("date") date
  ) {
    const wallet = await this.walletService.findOneR(
      null,
      type_wallet,
      userId,
      countrieId,
      date
    );
    if (wallet == null) {
      return ResponseUtil.error("No existe wallet con ese criterio");
    }
    const transactions = await this.transWallService.findAll(
      wallet.id,
      type_wallet
    );
    return transactions;
  }

  @Get("find-filters")
  findFilters(@Query("type_wallet") type_wallet) {
    return this.transWallService.findAll(null, type_wallet);
  }

  @UseGuards(AuthGuard)
  @Get(":id")
  findOne(@Param("id") id: string) {
    return this.transWallService.findOne(+id);
  }

  @UseGuards(AuthGuard)
  @Patch(":id")
  update(
    @Param("id") id: string,
    @Body() updateTransWallDto: UpdateTransWallDto
  ) {
    return this.transWallService.update(+id, updateTransWallDto);
  }

  @UseGuards(AuthGuard)
  @Delete(":id")
  remove(@Param("id") id: string) {
    return this.transWallService.remove(+id);
  }

  @UseGuards(AuthGuard)
  @Get("generate-reports-profits-owner")
  generateReporstProfitsOwner(@Query("userId") userId, @Query("date") date) {
    return this.transWallService.generateReporstProfitsOwner(userId, date);
  }

  @UseGuards(AuthGuard)
  @Post("generate-expenses")
  generateExpenses(@Body() data) {
    return this.transWallService.generateExpenses(data);
  }
}
