import { TYPE_PRESUPUESTO } from "src/enums/type_account.enum";
import { WalletUser } from "src/wallet-users/entities/wallet-user.entity";
import {
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  Entity,
  OneToOne,
  JoinColumn,
} from "typeorm";

@Entity()
export class TransWall {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  concept: string;

  @Column("decimal", { precision: 15, scale: 4, default: 0.0 })
  old_amount: number;

  @Column("decimal", { precision: 15, scale: 4, default: 0.0 })
  new_amount: number;

  @Column("decimal", { precision: 15, scale: 4, default: 0.0 })
  amount_transaction: number;

  @Column({ type: "enum", enum: TYPE_PRESUPUESTO })
  type: TYPE_PRESUPUESTO;

  @ManyToOne(() => WalletUser, (wallet) => wallet.trans_walls)
  wallet: WalletUser;

  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt: Date;
}
