import { Module } from '@nestjs/common';
import { TransWallService } from './trans-wall.service';
import { TransWallController } from './trans-wall.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TransWall } from './entities/trans-wall.entity';
import { WalletUsersService } from 'src/wallet-users/wallet-users.service';
import { WalletUser } from 'src/wallet-users/entities/wallet-user.entity';
import { Country } from 'src/countries/entities/country.entity';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([TransWall, WalletUser, Country, User])],
  controllers: [TransWallController],
  providers: [TransWallService, WalletUsersService],
  exports: [TypeOrmModule],
})
export class TransWallModule {}
