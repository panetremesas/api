import { DocumentsExpedient } from "src/documents-expedient/entities/documents-expedient.entity";
import { TYPE_EXPEDIENT } from "src/enums/type_account.enum";
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class Expedient {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({type: 'enum', enum: TYPE_EXPEDIENT})
  type_expedient: TYPE_EXPEDIENT;

  @Column()
  id_trans: number //esto puede ser: una transaccion o una recarga

  @OneToMany(() => DocumentsExpedient, (document) => document.expedient)
  documents: DocumentsExpedient[]

  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt: Date;
}
