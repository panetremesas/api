import { Injectable } from '@nestjs/common';
import { CreateExpedientDto } from './dto/create-expedient.dto';
import { UpdateExpedientDto } from './dto/update-expedient.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Expedient } from './entities/expedient.entity';
import { Repository } from 'typeorm';
import { ResponseUtil } from 'src/utils/response.util';

@Injectable()
export class ExpedientService {
  constructor(
    @InjectRepository(Expedient)
    private readonly expedientRepository: Repository<Expedient>
  ){}
  create(createExpedientDto: CreateExpedientDto) {
    return 'This action adds a new expedient';
  }

  findAll() {
    return `This action returns all expedient`;
  }

  async findOne(id: string) {
    const data = await this.expedientRepository.findOne({where: {id: id}, relations: ['documents', 'documents.document']})

    return ResponseUtil.success(data, "Consulta realizada con exito")
  }

  update(id: number, updateExpedientDto: UpdateExpedientDto) {
    return `This action updates a #${id} expedient`;
  }

  remove(id: number) {
    return `This action removes a #${id} expedient`;
  }
}
