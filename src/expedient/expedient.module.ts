import { Module } from '@nestjs/common';
import { ExpedientService } from './expedient.service';
import { ExpedientController } from './expedient.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Expedient } from './entities/expedient.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Expedient])],
  controllers: [ExpedientController],
  providers: [ExpedientService],
})
export class ExpedientModule {}
