import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ExpedientService } from './expedient.service';
import { CreateExpedientDto } from './dto/create-expedient.dto';
import { UpdateExpedientDto } from './dto/update-expedient.dto';

@Controller('expedient')
export class ExpedientController {
  constructor(private readonly expedientService: ExpedientService) {}

  @Post()
  create(@Body() createExpedientDto: CreateExpedientDto) {
    return this.expedientService.create(createExpedientDto);
  }

  @Get()
  findAll() {
    return this.expedientService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.expedientService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateExpedientDto: UpdateExpedientDto) {
    return this.expedientService.update(+id, updateExpedientDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.expedientService.remove(+id);
  }
}
