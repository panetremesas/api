import { Injectable } from '@nestjs/common';
import { createConnection } from 'mysql2/promise';

@Injectable()
export class DatabaseDolarService {
  private connection: any;

  async connect() {
    this.connection = await createConnection({
      host: 'produccion.dolardaily.com',
      user: 'acampos',
      password: '1256Eli*a1',
      database: 'dolar_produccion',
    });
  }

  private async ensureConnection() {
    if (!this.connection || this.connection.connection._closing) {
      await this.connect();
    }
  }

  async query(sql: string, params: any[] = []) {
    try {
      await this.ensureConnection();
      const [results] = await this.connection.execute(sql, params);
      return results;
    } catch (error) {
      if (error.message.includes('Can\'t add new command when connection is in closed state')) {
        await this.connect();
        const [results] = await this.connection.execute(sql, params);
        return results;
      } else {
        throw error;
      }
    }
  }

  async disconnect() {
    if (this.connection) {
      await this.connection.end();
    }
  }
}
