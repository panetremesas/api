import { IsNumber } from "class-validator";
import { User } from "src/users/entities/user.entity";
import { Transaction } from "typeorm";

export class CreateQueueDto {

    @IsNumber()
    transaction: Transaction

    @IsNumber()
    dispatcher: User
}
