import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { QueuesService } from './queues.service';
import { CreateQueueDto } from './dto/create-queue.dto';
import { UpdateQueueDto } from './dto/update-queue.dto';

@Controller('queues')
export class QueuesController {
  constructor(private readonly queuesService: QueuesService) { }

  @Get()
  findAll() {
    return this.queuesService.findAll()
  }

  @Patch()
  transfer(@Body() data) {
    return this.queuesService.transfer(data)
  }

  @Get('test')
  test() {
    return this.queuesService.test()
  }

  @Get("terminar/:id")
  terminar(@Param("id") id: string) {
    return this.queuesService.terminar(id)
  }

  @Get("colocar/:id")
  colocar(@Param("id") id: string) {
    return this.queuesService.colocar(id)
  }

}
