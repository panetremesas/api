import { STATUS_QUEUE } from "src/enums/type_account.enum";
import { TYPE_DESPACHO_ACCOUNT, TYPES_QUEUES } from "src/enums/types-queues.enum";
import { Transaction } from "src/transactions/entities/transaction.entity";
import { User } from "src/users/entities/user.entity";
import { WalletsRefill } from "src/wallets-refills/entities/wallets-refill.entity";
import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
@Entity()
@Index(['transaction'], { unique: false })
export class Queue {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @ManyToOne(() => Transaction, {nullable: true})
  transaction: Transaction;

  @ManyToOne(() => WalletsRefill, {nullable: true})
  wallet_refill: WalletsRefill;

  @ManyToOne(() => User)
  dispatcher: User;

  @Column({ type: "enum", enum: STATUS_QUEUE, default: STATUS_QUEUE.CREADA })
  status: STATUS_QUEUE;

  @Column({ type: 'enum', enum: TYPES_QUEUES, default: TYPES_QUEUES.DESPACHO })
  type: TYPES_QUEUES;

  @Column({ type: 'enum', enum: TYPE_DESPACHO_ACCOUNT, default: TYPE_DESPACHO_ACCOUNT.NOPROCEDE})
  type_despacho: TYPE_DESPACHO_ACCOUNT

  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt: Date;
}
