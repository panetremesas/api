import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Queue } from "./entities/queue.entity";
import { Repository } from "typeorm";
import { ResponseUtil } from "src/utils/response.util";
import { STATUS_QUEUE } from "src/enums/type_account.enum";
import { createClient } from "@supabase/supabase-js";
import { TYPES_QUEUES } from "src/enums/types-queues.enum";

@Injectable()
export class QueuesService {
  private supabase;
  constructor(
    @InjectRepository(Queue)
    private readonly queueRepository: Repository<Queue>
  ) {
    this.supabase = createClient(
      "https://xxbfqggjfjcbmxtlobrh.supabase.co",
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Inh4YmZxZ2dqZmpjYm14dGxvYnJoIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTY1NjY1NTEsImV4cCI6MjAzMjE0MjU1MX0.M4Y0rMHaPb-ynXYM_VFPyVDBTJ4GNctgwE4Nv-taOgU"
    );
  }

  async getOperations(home, user, status, type?) {
    console.log(type);
    type = type || TYPES_QUEUES.DESPACHO;

    let operations = await this.queueRepository
      .createQueryBuilder("operation")
      .leftJoinAndSelect("operation.transaction", "transaction")
      .leftJoinAndSelect("operation.wallet_refill", "wallet_refill")
      .leftJoinAndSelect("transaction.destination", "destination")
      .leftJoinAndSelect("transaction.origin", "origin")
      .leftJoinAndSelect("wallet_refill.countrie", "countrie")
      .where("operation.dispatcherId = :dispatcherId", {
        dispatcherId: user.id,
      })
      .andWhere("operation.type = :type", { type });

    if (status) {
      operations = operations.andWhere("operation.status = :status", {
        status: status,
      });
    }

    if (home) {
      operations = operations.take(5);
    }

    const result = await operations.getMany();

    return ResponseUtil.success(result, "Operaciones obtenidas con éxito");
  }

  async getOne(id) {
    let operation = await this.queueRepository
      .createQueryBuilder("operation")
      .leftJoinAndSelect("operation.transaction", "transaction")
      .leftJoinAndSelect("operation.wallet_refill", "wallet_refill")
      .leftJoinAndSelect("transaction.destination", "destination")
      .leftJoinAndSelect("transaction.instrument", "instrument")
      .leftJoinAndSelect("instrument.bank_method", "bank_method")
      .where("operation.id = :id", { id })
      .getOne();

    return ResponseUtil.success(operation, "Operaciones Obtenidas con exito");
  }

  async findAll() {
    let operations = await this.queueRepository
      .createQueryBuilder("queue")
      .leftJoinAndSelect("queue.dispatcher", "dispatcher")
      .leftJoinAndSelect("queue.wallet_refill", "wallet_refill")
      .leftJoinAndSelect("wallet_refill.countrie", "countrie")
      .leftJoinAndSelect("queue.transaction", "transaction")
      .leftJoinAndSelect("transaction.destination", "destination")
      .where("queue.status IN (:statuses)", {
        statuses: [STATUS_QUEUE.CREADA, STATUS_QUEUE.OBSERVADA],
      })
      .orderBy("queue.transactionId", "DESC")
      .getMany();

    return ResponseUtil.success(
      operations,
      "Listado de Operaciones pendientes"
    );
  }

  async transfer(data) {
    try {
      const update = this.queueRepository.update(data.idQueue, {
        dispatcher: data.idDispatcher,
      });
      return ResponseUtil.success(update, "Transaccion transferida con exito");
    } catch (error) {
      return ResponseUtil.error("Transaccion no transferida");
    }
  }

  async test() {
    const { data, error } = await this.supabase
      .from("transactions")
      .insert([{ transactionId: 123 }])
      .select();

    return ResponseUtil.success(data, "Prueba correcta");
  }

  async terminar(id) {
    const data = await this.queueRepository.update(id, {
      status: STATUS_QUEUE.COMPLEATADA,
    });
    return ResponseUtil.success(data, "Datos obtenidos con exito");
  }

  async colocar(id) {
    let data = await this.queueRepository
      .createQueryBuilder("transaccion")
      .where("transaccion.transactionId = :id", { id })
      .getOne();
    if (!data) {
      ResponseUtil.error("Transaccion fallida contacta a soporte!");
    }

    await this.queueRepository.update(data.id, { status: STATUS_QUEUE.CREADA });
    return ResponseUtil.success(
      data,
      "Transaccion agregada a cola correctamente"
    );
  }
}
