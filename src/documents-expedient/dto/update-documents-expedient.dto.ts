import { PartialType } from '@nestjs/mapped-types';
import { CreateDocumentsExpedientDto } from './create-documents-expedient.dto';

export class UpdateDocumentsExpedientDto extends PartialType(CreateDocumentsExpedientDto) {}
