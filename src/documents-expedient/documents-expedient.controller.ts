import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { DocumentsExpedientService } from './documents-expedient.service';
import { CreateDocumentsExpedientDto } from './dto/create-documents-expedient.dto';
import { UpdateDocumentsExpedientDto } from './dto/update-documents-expedient.dto';

@Controller('documents-expedient')
export class DocumentsExpedientController {
  constructor(private readonly documentsExpedientService: DocumentsExpedientService) {}

  @Post()
  create(@Body() createDocumentsExpedientDto: CreateDocumentsExpedientDto) {
    return this.documentsExpedientService.create(createDocumentsExpedientDto);
  }

  @Get()
  findAll() {
    return this.documentsExpedientService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.documentsExpedientService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDocumentsExpedientDto: UpdateDocumentsExpedientDto) {
    return this.documentsExpedientService.update(+id, updateDocumentsExpedientDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.documentsExpedientService.remove(+id);
  }
}
