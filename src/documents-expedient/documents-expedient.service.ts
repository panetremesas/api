import { Injectable } from '@nestjs/common';
import { CreateDocumentsExpedientDto } from './dto/create-documents-expedient.dto';
import { UpdateDocumentsExpedientDto } from './dto/update-documents-expedient.dto';

@Injectable()
export class DocumentsExpedientService {
  create(createDocumentsExpedientDto: CreateDocumentsExpedientDto) {
    return 'This action adds a new documentsExpedient';
  }

  findAll() {
    return `This action returns all documentsExpedient`;
  }

  findOne(id: number) {
    return `This action returns a #${id} documentsExpedient`;
  }

  update(id: number, updateDocumentsExpedientDto: UpdateDocumentsExpedientDto) {
    return `This action updates a #${id} documentsExpedient`;
  }

  remove(id: number) {
    return `This action removes a #${id} documentsExpedient`;
  }
}
