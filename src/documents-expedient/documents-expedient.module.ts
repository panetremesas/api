import { Module } from '@nestjs/common';
import { DocumentsExpedientService } from './documents-expedient.service';
import { DocumentsExpedientController } from './documents-expedient.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DocumentsExpedient } from './entities/documents-expedient.entity';

@Module({
  imports: [TypeOrmModule.forFeature([DocumentsExpedient])],
  controllers: [DocumentsExpedientController],
  providers: [DocumentsExpedientService],
})
export class DocumentsExpedientModule {}
