import { Document } from "src/documents/entities/document.entity";
import { Expedient } from "src/expedient/entities/expedient.entity";
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class DocumentsExpedient {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @ManyToOne(() => Expedient, (expedient) => expedient.documents)
  expedient: Expedient;

  @ManyToOne(() => Document)
  document: Document;

  @Column({ nullable: true })
  response: string;

  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt: Date;
}
