import { Logger, Module, forwardRef } from '@nestjs/common';
import { WalletUsersService } from './wallet-users.service';
import { WalletUsersController } from './wallet-users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WalletUser } from './entities/wallet-user.entity';
import { TransWallModule } from 'src/trans-wall/trans-wall.module';
import { TransWallService } from 'src/trans-wall/trans-wall.service';
import { TransWall } from 'src/trans-wall/entities/trans-wall.entity';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([WalletUser, TransWall, User]),
    TransWallModule
  ],
  controllers: [WalletUsersController],
  providers: [WalletUsersService, TransWallService]
})
export class WalletUsersModule {}
