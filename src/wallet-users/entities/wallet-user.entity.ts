import { Country } from "src/countries/entities/country.entity";
import { TYPE_WALLET } from "src/enums/type_account.enum";
import { TransWall } from "src/trans-wall/entities/trans-wall.entity";
import { User } from "src/users/entities/user.entity";
import { WithdraWalletDetail } from "src/withdra-wallet-detail/entities/withdra-wallet-detail.entity";

import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class WalletUser {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Country)
  countrie: Country;

  @ManyToOne(() => User, (user) => user.wallets)
  user: User;

  @Column("decimal", { precision: 15, scale: 4, default: 0.0 })
  available: number;

  @Column({ type: "enum", enum: TYPE_WALLET })
  type_wallet: TYPE_WALLET;

  @Column({ type: 'boolean', default: true })
  status: boolean;

  @OneToMany(() => TransWall, (transaction) => transaction.wallet)
  trans_walls: TransWall[];

  @OneToMany(() => WithdraWalletDetail, (retiro) => retiro.wallet)
  retiros: WithdraWalletDetail[]
  
  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt: Date;
}
