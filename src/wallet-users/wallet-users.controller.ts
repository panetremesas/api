import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UseGuards,
  Request,
} from "@nestjs/common";
import { WalletUsersService } from "./wallet-users.service";
import { CreateWalletUserDto } from "./dto/create-wallet-user.dto";
import { UpdateWalletUserDto } from "./dto/update-wallet-user.dto";
import { AuthGuard } from "src/auth/auth.guard";
import { TransWallService } from "src/trans-wall/trans-wall.service";

@UseGuards(AuthGuard)
@Controller("wallet-users")
export class WalletUsersController {
  constructor(
    private readonly walletUsersService: WalletUsersService,
    private readonly transService: TransWallService
  ) {}

  @Post()
  async create(@Body() createWalletUserDto: CreateWalletUserDto) {
    return await this.walletUsersService.create(createWalletUserDto);
  }

  @Get()
  findAll(
    @Query("type") type_wallet,
    @Query("userId") userId,
    @Query("countrieId") countrieId
  ) {
    return this.walletUsersService.findAll(type_wallet, userId, countrieId);
  }

  @Get("filtrar")
  findOne(
    @Query("id") id,
    @Query("type_wallet") type_wallet,
    @Query("userId") userId,
    @Query("countrieId") countrieId
  ) {
    return this.walletUsersService.findOne(id, type_wallet, userId, countrieId);
  }

  @Patch(":id")
  async update(
    @Param("id") id: number,
    @Body() updateWalletUserDto: UpdateWalletUserDto
  ) {
    return await this.walletUsersService.update(id, updateWalletUserDto);
  }

  @Get("update-status/:id/:status")
  updateStatus(@Param("id") id, @Param("status") status){
    return this.walletUsersService.updateStatus(id, status)
  }
}
