import { IsNumber, IsString } from "class-validator";
import { TYPE_PRESUPUESTO } from "src/enums/type_account.enum";

export class UpdateWalletUserDto  {
    @IsNumber()
    amount: number

    @IsString()
    type: TYPE_PRESUPUESTO

    @IsString()
    concept: string
}
