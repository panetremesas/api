import { IsNumber, IsString } from "class-validator";
import { Country } from "src/countries/entities/country.entity";
import { TYPE_PRESUPUESTO, TYPE_WALLET } from "src/enums/type_account.enum";
import { User } from "src/users/entities/user.entity";

export class CreateWalletUserDto {
    @IsNumber()
    user: User

    @IsNumber()
    countrie: Country
    
    @IsNumber()
    available: number

    @IsString()
    type_wallet: TYPE_WALLET

    @IsString()
    type: TYPE_PRESUPUESTO

    @IsString()
    concept: string
}
