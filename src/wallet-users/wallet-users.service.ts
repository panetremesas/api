import { Injectable, Query } from "@nestjs/common";
import { CreateWalletUserDto } from "./dto/create-wallet-user.dto";
import { UpdateWalletUserDto } from "./dto/update-wallet-user.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { WalletUser } from "./entities/wallet-user.entity";
import { Repository } from "typeorm";
import { ResponseUtil } from "src/utils/response.util";
import { TYPE_PRESUPUESTO, TYPE_WALLET } from "src/enums/type_account.enum";
import { TransWall } from "src/trans-wall/entities/trans-wall.entity";
import { User } from "src/users/entities/user.entity";
import { PushNotificationUtil } from "src/utils/push.utils";

@Injectable()
export class WalletUsersService {
  constructor(
    @InjectRepository(WalletUser)
    private readonly walletUserRepository: Repository<WalletUser>,
    @InjectRepository(TransWall)
    private readonly transWallsRepository: Repository<TransWall>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>
  ) { }

  async create(createWalletUserDto: CreateWalletUserDto) {
    let vValidate = await this.walletUserRepository.createQueryBuilder("wallet")
      .where("userId = :userId AND type_wallet = 'USER' AND countrieId = :countrieId", {
        userId: createWalletUserDto.user,
        countrieId: createWalletUserDto.countrie
      }).getOne()

    if (vValidate) {
      let newAmount = +vValidate.available + +createWalletUserDto.available
      console.log(newAmount)
      let concept = createWalletUserDto.concept
      await this.transWallsRepository.save({
        concept: concept,
        old_amount: vValidate.available,
        new_amount: newAmount,
        amount_transaction: createWalletUserDto.available,
        type: TYPE_PRESUPUESTO.INGRESO,
        wallet: vValidate
      });

      try {
        const updateResult = await this.walletUserRepository.update(vValidate.id, {
          available: newAmount,
        });
        return ResponseUtil.success(updateResult, "Ingreso registrado con exito")
      } catch (error) {
        console.error('Error updating wallet user:', error);
      }
    }
    const wallet = await this.walletUserRepository.save(createWalletUserDto);

    const vTrans = await this.transWallsRepository.save({
      concept: createWalletUserDto.concept,
      old_amount: createWalletUserDto.available,
      new_amount: createWalletUserDto.available,
      amount_transaction: createWalletUserDto.available,
      type: createWalletUserDto.type,
      wallet: wallet,
    });

    const vUser = await this.userRepository
      .createQueryBuilder("user")
      .where("id = :id", { id: createWalletUserDto.user })
      .getOne();

    // PushNotificationUtil.sendPush(
    //   vUser.token_owner,
    //   "Se ha Creado un nuevo wallet para ti, al mismo tiempo se realizo una recarga por concepto: " +
    //   createWalletUserDto.concept
    // );

    return ResponseUtil.success(
      vTrans,
      "Wallet y Transaccion registradas con exito"
    );
  }

  async findAll(type_wallet: TYPE_WALLET, userId, countrieId) {
    const query = this.walletUserRepository
      .createQueryBuilder("wallet")
      .leftJoinAndSelect("wallet.countrie", "countrie")
      .leftJoinAndSelect("wallet.user", "user");

    if (type_wallet) {
      query.andWhere("wallet.type_wallet = :type", {
        type: type_wallet,
      });
    }

    if (userId) {
      query.andWhere("wallet.userId = :userId", { userId });
    }

    if (countrieId) {
      query.andWhere("wallet.countrieId = :countrieId", { countrieId });
    }

    const result = await query.getMany();

    return ResponseUtil.success(result, "Wallets Ontenidos con exito");
  }

  async findOne(
    id: number | null,
    type_wallet: TYPE_WALLET | null,
    userId: number | null,
    countrieId: number | null
  ) {
    const queryBuilder = this.walletUserRepository
      .createQueryBuilder("wallet")
      .leftJoinAndSelect("wallet.countrie", "countrie");

    if (!id && !type_wallet && !userId && !countrieId)
      return ResponseUtil.error("No has Proporcionado criterio de busqueda");

    if (id) {
      queryBuilder.andWhere("wallet.id = :id", { id });
    }

    if (userId) {
      queryBuilder.andWhere("wallet.userId = :userId", { userId });
    }

    if (type_wallet) {
      queryBuilder.andWhere("wallet.type_wallet = :type_wallet", {
        type_wallet,
      });
    }

    if (countrieId) {
      queryBuilder.andWhere("wallet.countrieId = :countrieId", { countrieId });
    }

    const result = await queryBuilder.getOne();

    if (!result) {
      return ResponseUtil.error(
        "No se encontró ninguna billetera que coincida con los criterios de búsqueda."
      );
    }

    return ResponseUtil.success(result, "Billetera obtenida con éxito.");
  }

  async findOneR(
    id,
    type_wallet,
    userId,
    countrieId,
    date
  ): Promise<WalletUser> {
    const queryBuilder = this.walletUserRepository
      .createQueryBuilder("wallet")
      .leftJoinAndSelect("wallet.countrie", "countrie");

    if (id) {
      queryBuilder.andWhere("wallet.id = :id", { id });
    }

    if (userId) {
      queryBuilder.andWhere("wallet.userId = :userId", { userId });
    }

    if (type_wallet) {
      queryBuilder.andWhere("wallet.type_wallet = :type_wallet", {
        type_wallet,
      });
    }

    if (date) {

    }

    if (countrieId) {
      queryBuilder.andWhere("wallet.countrieId = :countrieId", { countrieId });
    }

    const result = await queryBuilder.orderBy("wallet.id", "DESC").getOne();

    return result;
  }

  async update(id: number, updateWalletUserDto: UpdateWalletUserDto) {
    const vWallet = await this.walletUserRepository.findOne({
      where: { id: id },
      relations: ["user", "countrie"],
    });
    // const userId = vWallet.user.id;

    if (vWallet == null)
      return ResponseUtil.error("El wallet que intentas actualizar no existe");

    let newAmount = +vWallet.available + +updateWalletUserDto.amount;

    if (updateWalletUserDto.amount !== 0) {
      await this.walletUserRepository.update(id, {
        available: newAmount,
      });
    }
    else {
      await this.walletUserRepository.update(id, {
        available: 0,
      });
      newAmount = 0
      updateWalletUserDto.concept = "Vaciado del allet por parte de administracion!"
    }

    await this.transWallsRepository.save({
      concept: updateWalletUserDto.concept,
      old_amount: vWallet.available,
      new_amount: newAmount,
      amount_transaction: updateWalletUserDto.amount,
      type: updateWalletUserDto.type,
      wallet: vWallet,
    });

    // const vUser = await this.userRepository
    //   .createQueryBuilder("user")
    //   .where("id = :id", { id: userId })
    //   .getOne();

    // PushNotificationUtil.sendPush(
    //   vUser.token_owner,
    //   "Se ha agregado salgo por un ingreso a tiu wallet de: PANET " +
    //   vWallet.countrie.name +
    //   " por concepto de: " +
    //   updateWalletUserDto.concept
    // );

    return ResponseUtil.success(vWallet, "Actualizacion de wallet Realizada con exito");
  }

  remove(id: number) {
    return `This action removes a #${id} walletUser`;
  }

  async updateStatus(id, status) {
    let value = false
    if (status === 'true') {
      value = true
    }
    await this.walletUserRepository.update(id, { status: value })

    return ResponseUtil.success(id, "Wallet Actualizado con exito")
  }

  async walletsTransactions(user) {
    let data = await this.walletUserRepository.createQueryBuilder("wallet")
      .where("wallet.type_wallet = :type_wallet", { type_wallet: TYPE_WALLET.TRANSACTIONS })
      .andWhere("wallet.userId = :userId", { userId: user.id })
      .leftJoinAndSelect("wallet.countrie", "countrie")
      .getMany()

    return { data, message: 'Wallets Obtenidos con exito' }
  }

}
