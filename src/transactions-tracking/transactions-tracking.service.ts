import { Injectable } from '@nestjs/common';
import { CreateTransactionsTrackingDto } from './dto/create-transactions-tracking.dto';
import { UpdateTransactionsTrackingDto } from './dto/update-transactions-tracking.dto';

@Injectable()
export class TransactionsTrackingService {
  create(createTransactionsTrackingDto: CreateTransactionsTrackingDto) {
    return 'This action adds a new transactionsTracking';
  }

  findAll() {
    return `This action returns all transactionsTracking`;
  }

  findOne(id: number) {
    return `This action returns a #${id} transactionsTracking`;
  }

  update(id: number, updateTransactionsTrackingDto: UpdateTransactionsTrackingDto) {
    return `This action updates a #${id} transactionsTracking`;
  }

  remove(id: number) {
    return `This action removes a #${id} transactionsTracking`;
  }
}
