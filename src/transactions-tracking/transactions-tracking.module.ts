import { Module } from '@nestjs/common';
import { TransactionsTrackingService } from './transactions-tracking.service';
import { TransactionsTrackingController } from './transactions-tracking.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TransactionsTracking } from './entities/transactions-tracking.entity';

@Module({
  imports: [TypeOrmModule.forFeature([TransactionsTracking])],
  controllers: [TransactionsTrackingController],
  providers: [TransactionsTrackingService],
})
export class TransactionsTrackingModule {}
