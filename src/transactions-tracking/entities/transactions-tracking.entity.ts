import { Transaction } from "src/transactions/entities/transaction.entity";
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class TransactionsTracking {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    status: string

    @Column({type: 'date'})
    fecha: Date

    @ManyToOne(() => Transaction)
    transaction: Transaction;

    @CreateDateColumn({ type: 'timestamp' })
    createdAt: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt: Date;
}
