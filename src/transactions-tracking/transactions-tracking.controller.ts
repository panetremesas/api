import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { TransactionsTrackingService } from './transactions-tracking.service';
import { CreateTransactionsTrackingDto } from './dto/create-transactions-tracking.dto';
import { UpdateTransactionsTrackingDto } from './dto/update-transactions-tracking.dto';

@Controller('transactions-tracking')
export class TransactionsTrackingController {
  constructor(private readonly transactionsTrackingService: TransactionsTrackingService) {}

  @Post()
  create(@Body() createTransactionsTrackingDto: CreateTransactionsTrackingDto) {
    return this.transactionsTrackingService.create(createTransactionsTrackingDto);
  }

  @Get()
  findAll() {
    return this.transactionsTrackingService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.transactionsTrackingService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTransactionsTrackingDto: UpdateTransactionsTrackingDto) {
    return this.transactionsTrackingService.update(+id, updateTransactionsTrackingDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.transactionsTrackingService.remove(+id);
  }
}
