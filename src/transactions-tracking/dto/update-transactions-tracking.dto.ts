import { PartialType } from '@nestjs/mapped-types';
import { CreateTransactionsTrackingDto } from './create-transactions-tracking.dto';

export class UpdateTransactionsTrackingDto extends PartialType(CreateTransactionsTrackingDto) {}
