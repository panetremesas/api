import { Account } from "src/accounts/entities/account.entity";
import { Bank } from "src/banks/entities/bank.entity";
import { Country } from "src/countries/entities/country.entity";
import { TYPE_PRESUPUESTO } from "src/enums/type_account.enum";
import { User } from "src/users/entities/user.entity";
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class Presupuesto {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: "enum", enum: TYPE_PRESUPUESTO })
  type: TYPE_PRESUPUESTO;

  @Column({ type: "date" })
  date: Date;

  @Column("decimal", { precision: 20, scale: 3 })
  amount: number;

  @Column({ type: "text" })
  concept: string;

  @ManyToOne(() => User, (user) => user.presupuestos)
  userCreate: User;

  @ManyToOne(() => User, (user) => user.presupuestos_wallet)
  userWallet: User;

  @ManyToOne(() => Country, (countrie) => countrie.presupuestos)
  countrie: Country;

  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt: Date;
}
