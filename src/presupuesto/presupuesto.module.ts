import { Module } from "@nestjs/common";
import { PresupuestoService } from "./presupuesto.service";
import { PresupuestoController } from "./presupuesto.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Presupuesto } from "./entities/presupuesto.entity";
import { Account } from "src/accounts/entities/account.entity";

@Module({
  imports: [TypeOrmModule.forFeature([Presupuesto, Account])],
  controllers: [PresupuestoController],
  providers: [PresupuestoService],
})
export class PresupuestoModule { }
