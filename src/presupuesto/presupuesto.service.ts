import { Injectable } from "@nestjs/common";
import { CreatePresupuestoDto } from "./dto/create-presupuesto.dto";
import { UpdatePresupuestoDto } from "./dto/update-presupuesto.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { Bank } from "src/banks/entities/bank.entity";
import { Repository } from "typeorm";
import { Account } from "src/accounts/entities/account.entity";
import { Presupuesto } from "./entities/presupuesto.entity";

@Injectable()
export class PresupuestoService {
  constructor(
    @InjectRepository(Account)
    private readonly accountRepository: Repository<Account>,
    @InjectRepository(Presupuesto)
    private readonly presupuestoRepository: Repository<Presupuesto>
  ) {}
  async create(createPresupuestoDto: CreatePresupuestoDto) {
    
  }

  async findAll(countrie) {
    
    const queryBuilder = this.presupuestoRepository.createQueryBuilder(
      "presupuesto"
    );

    if(countrie){
      queryBuilder.andWhere("presupuesto.countrieId = :countrie", {
        countrie,
      });
    }
  }

  findOne(id: number) {
    return `This action returns a #${id} presupuesto`;
  }

  update(id: number, updatePresupuestoDto: UpdatePresupuestoDto) {
    return `This action updates a #${id} presupuesto`;
  }

  remove(id: number) {
    return `This action removes a #${id} presupuesto`;
  }
}
