import { IsDate, IsDateString, IsDecimal, IsEnum, IsNumber, IsString } from "class-validator";
import { Account } from "src/accounts/entities/account.entity";
import { Country } from "src/countries/entities/country.entity";
import { TYPE_PRESUPUESTO } from "src/enums/type_account.enum";
import { User } from "src/users/entities/user.entity";

export class CreatePresupuestoDto {
  @IsEnum(TYPE_PRESUPUESTO)
  type: TYPE_PRESUPUESTO;

  @IsDateString()
  date: string;

  @IsNumber()
  amount: number;

  @IsString()
  concept: string;

  @IsNumber()
  countrie: Country;

  @IsNumber()
  user: User;

  @IsNumber()
  account: Account;

  @IsNumber()
  accountFind: number;
}
