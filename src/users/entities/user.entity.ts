import { Account } from "src/accounts/entities/account.entity";
import { Bitacora } from "src/bitacora/entities/bitacora.entity";
import { Client } from "src/clients/entities/client.entity";
import { Presupuesto } from "src/presupuesto/entities/presupuesto.entity";
import { Queue } from "src/queues/entities/queue.entity";
import { Sucursal } from "src/sucursals/entities/sucursal.entity";
import { Transaction } from "src/transactions/entities/transaction.entity";
import { WalletUser } from "src/wallet-users/entities/wallet-user.entity";
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ unique: true })
  user: string;

  @Column()
  password: string;

  @Column({ nullable: true })
  typeUser: string;

  @Column({ default: false })
  puller: boolean; //jalador

  @Column({ default: false })
  account_owner: boolean; //dueño de cuenta

  @Column({ default: false })
  dispatcher: boolean; //despachador

  @Column({ default: false })
  remittances: boolean; //transferencias

  @Column({ default: false })
  shipments: boolean; //envios

  @Column("decimal", { precision: 15, scale: 2, nullable: true })
  profit_commission: number;

  @ManyToOne(() => Sucursal, (sucursal) => sucursal.users, { nullable: true })
  sucursal: Sucursal;

  @OneToMany(() => Account, (account) => account.user)
  accounts: Account[];

  @OneToMany(() => Client, (client) => client.jalador)
  clients: Client[];

  @OneToMany(() => Presupuesto, presupuesto => presupuesto.userCreate)
  presupuestos: Presupuesto[];

  @OneToMany(() => Presupuesto, presupuesto => presupuesto.userWallet)
  presupuestos_wallet: Presupuesto[];

  @OneToMany(() => Transaction, transaction => transaction.instrument)
  transactions: Transaction[];

  @OneToMany(() => Bitacora, (bitacora) => bitacora.user)
  bitacoras: Bitacora[]

  @OneToMany(() => WalletUser, wallet => wallet.user)
  wallets: WalletUser[];

  @OneToMany(() => Queue, wallet => wallet.dispatcher)
  cola: Queue[];

  @Column({ type: 'boolean', default: false })
  online: boolean

  @Column({ type: 'boolean', default: true })
  status: boolean

  @Column({ type: 'boolean', default: true })
  tipoCuentas: boolean

  @Column({ nullable: true })
  token_puler: string

  @Column({ nullable: true })
  token_owner: string

  @Column({ nullable: true })
  token_dispatcher: string

  @Column({ nullable: true })
  referer: string

  @Column({ nullable: true })
  phone: string

  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt: Date;
}
