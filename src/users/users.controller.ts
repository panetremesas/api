import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Req,
  UseGuards,
  Query,
} from "@nestjs/common";
import { UsersService } from "./users.service";
import { CreateUserDto } from "./dto/create-user.dto";
import { UpdateUserDto } from "./dto/update-user.dto";
import { AuthGuard } from "src/auth/auth.guard";

@Controller("users")
@UseGuards(AuthGuard)
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  @Get()
  findAll(@Req() request: Request, @Query('type_user') type_user, @Query('owners') owners) {
    const user = request["user"];
    const sucursalId = user.sucursalId?.id || null;
    const typeUser = user.typeUser;
    return this.usersService.findAll(typeUser, sucursalId, type_user);
  }

  @Patch(":id")
  update(@Param("id") id: number, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update(id, updateUserDto);
  }

  @Get('dispatcher-with-wallets')
  getDistpatcherWiethWallets(){
    return this.usersService.getDistpatcherWiethWallets()
  }

  @Post('update-password')
  updatePassword(@Body() data){
    return this.usersService.updatePassword(data)
  }

  @Get('update/accounts/:user/:status')
  updateAccounts(@Param("user") user, @Param("status") status){
    return this.usersService.updateAccounts(user, status)
  }
}
