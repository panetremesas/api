import { IsBoolean, IsNumber, IsOptional, IsString } from "class-validator";
import { Sucursal } from "src/sucursals/entities/sucursal.entity";

export class CreateUserDto {
  @IsString()
  name: string;

  @IsString()
  user: string;

  @IsString()
  password: string;

  @IsString()
  typeUser: string;

  @IsBoolean()
  puller: boolean;

  @IsBoolean()
  account_owner: boolean;

  @IsBoolean()
  dispatcher: boolean;

  @IsBoolean()
  remittances: boolean;

  @IsBoolean()
  shipments: boolean;

  @IsString()
  profit_commission: number;

  @IsOptional()
  @IsNumber()
  sucursal: Sucursal;

  @IsOptional()
  online: boolean;

  @IsOptional()
  token_puler: string;

  @IsOptional()
  token_owner: string;

  @IsOptional()
  token_dispatcher: string;

  @IsOptional()
  phone: string;
}

