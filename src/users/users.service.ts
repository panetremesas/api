import { Injectable } from "@nestjs/common";
import { CreateUserDto } from "./dto/create-user.dto";
import { UpdateUserDto } from "./dto/update-user.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { User } from "./entities/user.entity";
import { Repository } from "typeorm";
import { hash } from "bcrypt";
import { ResponseUtil } from "src/utils/response.util";

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>
  ) { }

  async create(createUserDto: CreateUserDto) {
    const hashed = await hash(createUserDto.password, 10);
    const user = { ...createUserDto, password: hashed };
    try {
      const usuario = await this.userRepository.save(user);
      return ResponseUtil.success(usuario, "Usuario creado con exito!");
    } catch (error) {
      if (error.code == 'ER_DUP_ENTRY') {
        return ResponseUtil.error(
          "Usuario duplicado, intente con otro nombre de usuario."
        );
      } else {
        return ResponseUtil.error("Error undefinied: " + error);
      }
    }
  }

  async findAll(typeUser, sucursal, type_user) {
    const queryBuilder = this.userRepository.createQueryBuilder("user");

    if (typeUser == "POPERARIO" || typeUser == "PREVISION") {
      return ResponseUtil.error("No Autorizado para obtener lista de usuarios");
    }
    if (typeUser == "PADMIN") {
      queryBuilder.andWhere("user.sucursalId = :sucursal", {
        sucursal: sucursal,
      });
    }

    if (type_user && type_user == "account_owner") {
      queryBuilder.where("user.account_owner = :account_owner", {
        account_owner: true,
      });
    }

    if (type_user && type_user == "puller") {
      queryBuilder.where("user.puller = :puller", {
        puller: true,
      });
    }

    if (type_user && type_user == "dispatcher") {
      queryBuilder.where("user.dispatcher = :dispatcher", {
        dispatcher: true,
      });
    }
    const result = await queryBuilder.getMany();

    return ResponseUtil.success(
      result,
      "Listado de Usuarios Obtenidos con exito"
    );
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const update = this.userRepository.update(id, updateUserDto);

    return ResponseUtil.success(update, "Usuario actualizado con exito");
  }

  async getDistpatcherWiethWallets() {
    const dispatchers = await this.userRepository.createQueryBuilder("user")
      .where("user.dispatcher = :dispatcher", { dispatcher: true })
      .leftJoinAndSelect("user.wallets", "wallets")
      .leftJoinAndSelect("wallets.countrie", "countrie")
      .andWhere("wallets.type_wallet = :typeWallet", { typeWallet: 'USER' })
      .getMany();

    return ResponseUtil.success(dispatchers, "Listado de Despachadores obtenidos con exito")
  }

  async updatePassword(data) {
    let passwordHashed = await hash(data.newPassword, 10)
    let result = await this.userRepository.update(data.userId, { password: passwordHashed })

    return ResponseUtil.success(result, "Clave actualizada con exito")
  }

  async updateAccounts(user, status) {
    let value = false
    if (status == "true") {
      value = true
    }
    console.log(user)
    console.log(value)

    let data = await this.userRepository.update(user, { tipoCuentas: value })

    return { data, message: 'Actualizacion correcta' }
  }
}
