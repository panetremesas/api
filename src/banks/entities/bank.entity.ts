import { Account } from "src/accounts/entities/account.entity";
import { Country } from "src/countries/entities/country.entity";
import { Presupuesto } from "src/presupuesto/entities/presupuesto.entity";
import { Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Bank {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string

    @Column()
    code: string

    @Column({ default: true })
    status: boolean

    @ManyToOne(() => Country, countrie => countrie.banks, { nullable: true })
    countrie: Country;

    @OneToMany(() => Account, account => account.countrie)
    accounts: Account[];

    @CreateDateColumn({ type: 'timestamp' })
    createdAt: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt: Date;
}
