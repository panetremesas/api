import { Injectable } from "@nestjs/common";
import { CreateBankDto } from "./dto/create-bank.dto";
import { UpdateBankDto } from "./dto/update-bank.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { Bank } from "./entities/bank.entity";
import { Repository } from "typeorm";
import { ResponseUtil } from "src/utils/response.util";

@Injectable()
export class BanksService {
  constructor(
    @InjectRepository(Bank)
    private readonly bankRepository: Repository<Bank>
  ) {}

  async create(createBankDto: CreateBankDto) {
    const bank = await this.bankRepository.save(createBankDto);

    const data = {
      module: "Bancos",
      error: false,
      message: "Banco creado con exito",
      data: bank,
    };
    return data;
  }

  async findAll(countrie) {
    let banks = this.bankRepository.createQueryBuilder("bank");

    if (countrie) {
      banks = banks.andWhere("bank.countrie = :countrie", { countrie });
    }

    const result = await banks.getMany();
    return ResponseUtil.success(result, "Bancos obtenidos con exito");
  }

  findOne(id: number) {
    return `This action returns a #${id} bank`;
  }

  async update(id: number, updateBankDto: UpdateBankDto) {
    const bank = await this.bankRepository.update(id, updateBankDto);

    const data = {
      module: "Bancos",
      error: false,
      message: "Banco editado con exito",
      data: bank,
    };
    return data;
  }

  remove(id: number) {
    return `This action removes a #${id} bank`;
  }
}
