import { PartialType } from '@nestjs/mapped-types';
import { CreateBankDto } from './create-bank.dto';
import { IsBoolean } from 'class-validator';

export class UpdateBankDto extends PartialType(CreateBankDto) {

    @IsBoolean()
    status: boolean
}
