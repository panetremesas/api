import { IsString } from "class-validator";
import { Country } from "src/countries/entities/country.entity";

export class CreateBankDto {
    @IsString()
    name: string

    @IsString()
    code: string

    @IsString()
    countrie: Country;
}
