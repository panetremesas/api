import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { hash } from 'bcrypt';
import { User } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';


@Injectable()
export class SeederService {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
    ) { }

    async seed() {
        // registra al usuario admin
        const findUser = await this.userRepository.findOne({ where: { user: 'sadmin' } })
        if (!findUser) {
            const plain = 'sadmin'
            const plainToHash = await hash(plain, 10)
            const usersToSeed = [
                { name: 'Super Admin', user: 'sadmin', password: plainToHash, typeUser: 'PSADMIN' },
            ];

            await Promise.all(
                usersToSeed.map(async (userData) => {
                    const user = this.userRepository.create(userData);
                    await this.userRepository.save(user);
                }),
            );
        }

    }
}
